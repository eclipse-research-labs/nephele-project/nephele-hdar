#!/bin/bash

components=("components/hdar-ctl" "components/hdar-sdk" "components/hdar-api" "components/hdav-listeners")
for component in "${components[@]}"; do
  if [ -d "$component" ]; then
    cd "$component" || continue
    go fmt $(go list ./... | grep -v /vendor/)
    go vet $(go list ./... | grep -v /vendor/)
    go test $(go list ./... | grep -v /vendor/) -v
    go mod tidy
    cd - > /dev/null # Go back to the original component dir silently
  else
    echo "component $component does not exist"
  fi
done
cd assets/xNF-Registry-Verification
bash tools/go-clean.sh
cd - > /dev/null # Go back to the original component dir silently