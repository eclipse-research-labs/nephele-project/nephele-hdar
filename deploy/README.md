
# Deploy in k8s

0. Configure the K8s cluster. 
- Either request the kubeconfig file to guillermo.gomezchavez@eviden.com or create your own, build your own images and push to a registry
- Adapt the values.yaml to update the images and secrets to pull them
- Create the required NS and secrets

```bash
kubectl create ns hdar
kubectl create secret docker-registry nexussecretnephele --docker-server=https://registry.atosresearch.eu:18498 --docker-username=REPLACE --docker-password=REPLACE -n hdar
kubectl create secret docker-registry nexussecretsns --docker-server=https://registry.atosresearch.eu:18467 --docker-username=REPLACE --docker-password=REPLACE -n hdar
kubectl create secret docker-registry nexussecretetsn --docker-server=https://registry.atosresearch.eu:18488 --docker-username=REPLACE --docker-password=REPLACE -n hdar
```

1. Install the solution

Install the OCI Registry if not available 
```bash
# adapt registry/values.yaml
# Assumes hdav installed in the same ns -> else change xnfvapi svc 
helm install registry OCI://registry.atosresearch.eu:18467/xnfregistry --version 1.0.0 -n hdar -f k8s/registry/values.yaml
```
Install the HDAR Verification Engine
```bash
# adapt hdav/values.yaml
helm install hdav OCI://registry.atosresearch.eu:18467/xnfv --version 1.0.0 -n hdar -f k8s/hdav/values.yaml
```
Install the HDAR API
```bash
# adapt hdarapi/values.yaml
helm install hdarapi OCI://registry.atosresearch.eu:18498/hdarapi --version 1.0.0 -n hdar -f k8s/hdarapi/values.yaml
```

Install the dev docs
```bash
# adapt dev-env-docs/values.yaml
helm install devdocs OCI://registry.atosresearch.eu:18488/nginx --version 1.0.0 -n hdar -f k8s/dev-env-docs/values.yaml
```

Install the gateway
```bash
# adapt gateway/values.yaml
helm install gateway  OCI://registry.atosresearch.eu:18488/gateway --version 1.3.0 -n hdar -f k8s/gateway/values.yaml
```

Note:
inside the ARI testbed you have to manually add this to the cert-manager deployment and restart it
```bash
kubectl edit deployments.apps -n cert-manager cert-manager
```
```yaml
# inside the container spec
      hostAliases:
      - hostnames:
        - registry.nephele.ari-imet.eu
        - documentation.nephele.ari-imet.eu
        - hdavapi.nephele.ari-imet.eu
        - hdarapi.nephele.ari-imet.eu
        - auth.nephele.ari-imet.eu
        ip: 192.168.137.35
```
## Monitoring stack

Metrics available under the Nephele environment https://monitoring.nephele-hdar.netmode.ece.ntua.gr 
and dashboard is localy available under [DevK8s ATOS local Cluster](192.168.137.24/31578)
Monitoring stack based on the kube-prometheus-stack as described in the gateway asset

Gateway metrics

[gateway dashboard](./k8s/gateway/grafana-dashboard.png)

Resource metrics

[promestack dashboard](./k8s/gateway/promestack-resource-dashboard.png)
