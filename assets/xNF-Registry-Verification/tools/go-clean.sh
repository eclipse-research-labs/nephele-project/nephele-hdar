#!/bin/bash

components=("components/xnfr-ctl" "components/xnfrv-sdk" "components/xnfv-api" "components/xnfv-listeners")
for component in "${components[@]}"; do
  if [ -d "$component" ]; then
    cd "$component" || continue
    go fmt $(go list ./... | grep -v /vendor/)
    go vet $(go list ./... | grep -v /vendor/)
    go test $(go list ./... | grep -v /vendor/) -v
    go mod tidy
    cd - > /dev/null # Go back to the original component dir silently
  else
    echo "component $component does not exist"
  fi
done