#!/bin/bash

components=("components/go-colors" "components/k8s" "components/utils")
for component in "${components[@]}"; do
  if [ -d "$component" ]; then
    cd "$component" || continue
    go fmt $(go list ./... | grep -v /vendor/)
    go vet $(go list ./... | grep -v /vendor/)
    go test $(go list ./... | grep -v /vendor/) -v
    go mod tidy
    cd - > /dev/null # Go back to the original component dir silently
  else
    echo "component $component does not exist"
  fi
done