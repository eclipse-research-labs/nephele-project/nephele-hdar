# Go Common

## Description

Set of modules for go development that can be reused accross projects

- components: contain generic modules to be imported by other projects and assets. Since it contains the ETSN logger, it should be used by all go developments
- tools/dockerfile_tools: scripts used in the dockerfile for the CICD

### Prerequisites

#### Software

All libraries are fixed to Golang 1.22.

## Usage

Based on a repo that uses the ETSN structure:

```bash
cd assets
git submodule add https://github.gsissc.myatos.net/GLB-BDS-ETSN-COMMON/go-common.git
cd ..
cd components/YOUR_COMPONENT
echo "replace go-common/components/utils => ../../assets/go-common/components/utils" >> go.mod
# First time
git submodule update init --recursive --remote
go mod tidy
# Next time you need to upgrade (e.g., after change in .gitmodules file )
git submodule update --recursive --remote
```

Use a specific branch or commit in your .gitmodules
```text
[submodule "assets/go-common"]
	path = assets/go-common
	url = https://github.gsissc.myatos.net/GLB-BDS-ETSN-COMMON/go-common.git
	branch = develop
```

```go
// check components/utils/README.md for a better example 
import "go-common/components/utils/logger"

func main(){
  logger.Log.Info("My message")
}
```

## Contribution

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or PR created from Github.
4. After the first commit, open a DRAFT PR to develop.
3. Commit and push frequently.
4. When ready, set PR as ready, request review and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```