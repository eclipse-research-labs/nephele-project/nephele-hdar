package k8s

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"

	workv1 "open-cluster-management.io/api/work/v1"
)

// This function creates a new ManifestWork from a given YAML string
func CreateManifestWork(namespace string, workflowName string, manifest string) (string, error) {
	decoder := serializer.NewCodecFactory(scheme.Scheme).UniversalDecoder()
	manifestWork := &workv1.ManifestWork{}
	fmt.Println("Namespace:", namespace, "WorkflowName:", workflowName)
	err := runtime.DecodeInto(decoder, []byte(manifest), manifestWork)
	if err != nil {
		log.Warning("Decode into yaml failed: ", err)
	}
	//fmt.Println(manifestWork)
	_, err3 := clientsetWorkOper.WorkV1().
		ManifestWorks(namespace).Create(context.TODO(),
		manifestWork, metav1.CreateOptions{})
	if err3 != nil {
		log.Error("ERROR CREATING: ", err3)
		// panic(err)
		return "Errors", err3
	}
	return "All good", nil
}

func DeleteManifestWork(namespace string, manifestName string) {
	manifestlist, _ := clientsetWorkOper.WorkV1().ManifestWorks(namespace).List(context.TODO(), metav1.ListOptions{})
	if len(manifestlist.Items) > 0 {
		for _, mw := range manifestlist.Items {
			if mw.Name == manifestName {
				fmt.Println(clientsetWorkOper.WorkV1().ManifestWorks(namespace).Delete(context.TODO(), manifestName, metav1.DeleteOptions{GracePeriodSeconds: new(int64)}))
			}
		}

	}
}
