package k8s

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

func ManifestWorksInfo(managedcluster ManagedCluster) []ManifestWork {
	//This allows to gather all manifestworks inside each managedcluster
	//fmt.Println(managedcluster)
	manifestlist, err := clientsetWorkOper.WorkV1().ManifestWorks(managedcluster.Name).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Warning("\nError obtaining ManifestWorkList: ", err)
	}

	manifestWorkSet := make([]ManifestWork, len(manifestlist.Items))
	if len(manifestlist.Items) > 0 {
		for i, mw := range manifestlist.Items {

			// Create list for all resources deployed in this manifestWork
			resourceList := make([]Resource, len(mw.Status.ResourceStatus.Manifests))

			for i, manifest := range mw.Status.ResourceStatus.Manifests {
				resourceList[i] = Resource{
					Name:    manifest.ResourceMeta.Name,
					Kind:    manifest.ResourceMeta.Kind,
					Status:  manifest.Conditions[1].Reason,
					Message: manifest.Conditions[0].Message,
				}
				if manifest.StatusFeedbacks.Values != nil {
					// if string(mw.Status.Conditions[0].Status) == "True" {

					conditions := make(map[string]string)
					conditions[manifest.StatusFeedbacks.Values[0].Name] = *manifest.StatusFeedbacks.Values[0].Value.String
					conditions[manifest.StatusFeedbacks.Values[1].Name] = *manifest.StatusFeedbacks.Values[1].Value.String

					// ### NEW CODE TO AVOID ASSIGNING STATUS FEEDBACKS IF IT IS NOT A POD ###
					if manifest.ResourceMeta.Kind == "Pod" {
						resourceList[i].StatusFeedbacks = StatusFeedbacks{
							Sync:       manifest.Conditions[2].Reason,
							Conditions: conditions,
						}
					}
					// ########################################################################
				}
			}

			mwStatus := MWStatus{
				Status:             mw.Status.Conditions[0].Reason,
				Reason:             mw.Status.Conditions[1].Message,
				LastTransitionTime: mw.Status.Conditions[0].LastTransitionTime.String(),
			}

			manifestWorkSet[i] = ManifestWork{
				Name:      mw.Name,
				Status:    mwStatus,
				Resources: resourceList,
			}

		}
	} else {
		fmt.Printf("Managed cluster: %v has no ManifestWorks applied", managedcluster.Name)
		return nil
	}
	return manifestWorkSet
}

func printManifestInfo(manifestList []ManifestWork) {

	for _, mw := range manifestList {
		fmt.Println("Name: ", mw.Name)
		fmt.Println("Status: ", mw.Status)
		fmt.Println("List of resources:")
		for _, rs := range mw.Resources {
			fmt.Println("\tName: ", rs.Name)
			fmt.Println("\tKind: ", rs.Kind)
			fmt.Println("\tStatus: ", rs.Status)
			fmt.Println("\tMessage: ", rs.Message)
			fmt.Println("\tStatusFeedbacks: ")
			fmt.Println("\t\tSync: ", rs.StatusFeedbacks.Sync)
			fmt.Println("\t\tConditions: ", rs.StatusFeedbacks.Conditions)
		}
	}

}

func ManagedClusterInfo() []ManagedCluster {
	// Get the list of all managedclusters using OCM API
	listManagedCluster, err := clientOCM.ManagedClusters().List(context.TODO(), metav1.ListOptions{})
	// ManagedClusterSet variable to store each ManagedClusters' info
	if err != nil {
		fmt.Println("Error obtaining list of managed clusters: ", err)
	}
	managedClusterSet := make([]ManagedCluster, len(listManagedCluster.Items))

	// Iterate through the list of ManagedClusters to access the attributes of interest and store it in our Set
	for item, managedCluster := range listManagedCluster.Items {
		managedClusterSpec := managedCluster.Spec
		currentYearDay := time.Now().YearDay()
		createdYearDay := managedCluster.ObjectMeta.CreationTimestamp.Time.YearDay()

		Name := managedCluster.Name
		HubAccepted := managedClusterSpec.HubAcceptsClient
		Age := currentYearDay - createdYearDay

		URL := "Unknown"
		if managedClusterSpec.ManagedClusterClientConfigs != nil {
			URL = managedClusterSpec.ManagedClusterClientConfigs[0].URL
		}

		// Store the information in the Set
		managedClusterSet[item] = ManagedCluster{
			Name:        Name,
			URL:         URL,
			HubAccepted: HubAccepted,
			Age:         Age,
		}
		managedClusterSet[item].SpokeAvailable = false
		managedClusterSet[item].SpokeJoined = false

		conditions := managedCluster.Status.Conditions
		if conditions != nil {
			if string(conditions[1].Status) == "True" {
				managedClusterSet[item].SpokeAvailable = true
			}
			if string(conditions[2].Status) == "True" {
				managedClusterSet[item].SpokeJoined = true
			}
		} else {
			managedClusterSet[item].SpokeAvailable = false
			managedClusterSet[item].SpokeJoined = false
		}

	}
	return managedClusterSet
}

func GetNodes() *rest.Request {
	return clientsetWorkOper.WorkV1alpha1().RESTClient().Get().Resource("nodes")
}
