package k8s

import (
	"flag"
	"fmt"
	"path/filepath"

	"os"

	log "github.com/sirupsen/logrus"

	restgo "k8s.io/client-go/rest"
	clientcmd "k8s.io/client-go/tools/clientcmd"
	ocm "open-cluster-management.io/api/client/cluster/clientset/versioned/typed/cluster/v1"
	workclient "open-cluster-management.io/api/client/work/clientset/versioned"
)

// var clientset *kubernetes.Clientset
var clientsetWorkOper *workclient.Clientset
var clientOCM *ocm.ClusterV1Client
var k8s_config KubeConfig

func SetupConfig(homedirectory string) {
	log.Info("\nCreating K8S client...")
	// log.Info("\nIs this the last version of the image?...")

	// Creates the in-cluster config
	config, err := restgo.InClusterConfig()

	// Outside of the cluster for development
	if err != nil {
		log.Warn("\nError in cluster config, whats going on? -> ", err)
		config = local_config(homedirectory)
		k8s_config.Server = config.Host
	}

	// KEY PARAMETERS TO ALLOW ROUTINES
	config.QPS = 100
	config.Burst = 100

	// Create a first OCM client used to get the ManifestWorks
	clientsetWorkOper, err = workclient.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	// Create a second OCM client used to get the ManagedClusters
	clientOCM, err = ocm.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
}

func APIEndpoint() string {
	fmt.Println("API Server")
	return k8s_config.Server
}

func local_config(homedirectory string) *restgo.Config {
	log.Warn("\nTrying local config, there was something wrong with the InClusterConfig")
	//panic(err.Error())
	var kubeconfig *string

	localkubeconfig := os.Getenv("localkubeconfig")
	log.Info("\nThe local kubeconfig to use is: ", localkubeconfig)
	if home := homedirectory; home != "" {
		kubeconfig = flag.String("kubeconfig",
			filepath.Join(home, ".kube/", localkubeconfig),
			"(optional) absolute path to the kubeconfig file")
		// log.Debug("\nKubeconfig to use is: ", *kubeconfig)
	} else {
		kubeconfig = flag.String("kubeconfig",
			"",
			"absolute path to the kubeconfig file")
		// log.Debug("\nKubeconfig to use is: ", *kubeconfig)
	}

	err := flag.CommandLine.Parse(os.Args[1:])
	if err != nil {
		panic(err.Error())
	}

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	return config
}
