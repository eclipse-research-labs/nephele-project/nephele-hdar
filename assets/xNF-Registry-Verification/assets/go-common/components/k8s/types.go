package k8s

// object to store the attributes of kubectl get managedclusters command
type ManagedCluster struct {
	Name           string
	URL            string
	Age            int
	HubAccepted    bool
	SpokeJoined    bool
	SpokeAvailable bool
	Color          string
}

// type ManifestWork struct { //change to bool some of the attributes instead of string?
// 	Message            string //Apply manifest complete. Reason directive in kubectl describe manifestwork
// 	Available          string //Resource Available or not
// 	Sync               string //Status Feedback Synced
// 	LastTransitionTime string //Last date of transition
// 	Name               string
// 	Resources          map[string]string //list of resources inside the manifestwork
// }

type ManifestWork struct {
	Name      string
	Status    MWStatus
	Resources []Resource
}

type MWStatus struct {
	Status             string
	Reason             string
	LastTransitionTime string
}

type Resource struct {
	Name            string
	Kind            string
	Status          string
	Message         string
	StatusFeedbacks StatusFeedbacks
}

type StatusFeedbacks struct {
	Sync       string
	Conditions map[string]string //[key: condition, value: state]. E.g. PodPhase: Running , PodReady: True
}

type KubeConfig struct {
	Name      string // Name of the cluster
	Server    string // https://{ip}:{port}
	Namespace string // "core"
	User      string // service account
	Token     string // pass
}
