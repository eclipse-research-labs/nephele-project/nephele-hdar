# Provided libraries

- "github.com/muesli/gamut"
- "github.com/muesli/gamut/palette"
- "github.com/muesli/gamut/theme"

Just for convenience there’s a hex-value parser:

```
var color color.Color
color = gamut.Hex("#333")
color = gamut.Hex("#ABCDEF")
```

Conversely you can retrieve the hex encoding of any color.Color value:

```
hex = gamut.ToHex(color)
```

The Darker and Lighter functions darken and lighten respectively a given color value by a specified percentage, without changing the color's hue:

returns a 10% darker version of color

```
color = gamut.Darker(color, 0.1)
```

returns a 30% lighter version of color

```
color = gamut.Lighter(color, 0.3)
```

Complementary returns the complementary color for a given color:

```
color = gamut.Complementary(color)
```

// Contrast returns the color with the highest contrast to a given color, either black or white:

```
color = gamut.Contrast(color)
```

To retrieve a color with the same lightness and saturation, but a different angle on the color wheel, you can use the HueOffset function:

```
color = gamut.HueOffset(color, 90)
```

# Shades, Tints & Tones

Monochromatic returns colors of the same hue, but with a different saturation/lightness:

```
colors = gamut.Monochromatic(color, 8)
```

### Shades returns colors blended from the given color to black:

```
colors = gamut.Shades(color, 8)
```
