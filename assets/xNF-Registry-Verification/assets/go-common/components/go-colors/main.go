package main

import (
	"bytes"
	"fmt"
	"os"

	colorful "github.com/lucasb-eyer/go-colorful"
	"github.com/muesli/gamut"
	"github.com/muesli/gamut/examples/htmlgen"
)

func main() {
	// Write HTML header
	buffer := bytes.NewBuffer([]byte{})
	// buffer.Write([]byte(header))

	happycolors, err := gamut.Generate(5, gamut.HappyGenerator{})
	if err != nil {
		panic(err)
	}

	for _, color := range happycolors {
		col, ok := colorful.MakeColor(color)
		if !ok {
			panic(fmt.Sprintf("invalid RGB color: %s", color))
		}
		htmlgen.Table(buffer,
			"",
			gamut.Monochromatic(col, 10))

		fmt.Println("Color as Hex:", col.Hex())
		// buffer.Write([]byte(fmt.Sprintf(cell, col.Hex())))
	}

	// Write HTML footer and generate palette.html
	// buffer.Write([]byte(footer))
	err = os.WriteFile("palette.html", buffer.Bytes(), 0644)
	if err != nil {
		fmt.Println("Error creating palette.html file")
	}
}
