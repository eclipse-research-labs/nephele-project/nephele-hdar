package logger_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	// imported as a consumer
	"go-common/components/utils/logger"
)

// MockEnv mocks environment variable values for testing.
func MockEnv(vars map[string]string) logger.EnvFunc {
	return func(key string) string {
		return vars[key]
	}
}

func TestConfigureLogger_DefaultSettings(t *testing.T) {
	var buf bytes.Buffer
	mockEnv := MockEnv(map[string]string{})
	logger := logger.ConfigureLogger("test", &buf, mockEnv)

	logger.Info("Test message")
	output := buf.String()

	assert.Contains(t, output, "Test message", "Log should contain the test message")
	assert.Contains(t, output, "INF", "Log should contain the log level INFO")
}

func TestConfigureLogger_DiscardLogs(t *testing.T) {
	var buf bytes.Buffer
	mockEnv := MockEnv(map[string]string{
		"LOGS_DISCARD": "True",
	})
	logger := logger.ConfigureLogger("test", &buf, mockEnv)

	logger.Info("Test message")
	output := buf.String()

	assert.Empty(t, output, "Log should be discarded and output should be empty")
}

func TestConfigureLogger_CustomFormat(t *testing.T) {
	var buf bytes.Buffer
	mockEnv := MockEnv(map[string]string{
		"LOGS_FORMAT": "%{message}",
	})
	logger := logger.ConfigureLogger("test", &buf, mockEnv)

	logger.Info("Test message")
	output := buf.String()

	assert.Equal(t, "Test message\n", output, "Log should use the custom format")
}

func TestConfigureLogger_InvalidLogLevel(t *testing.T) {
	var buf bytes.Buffer
	mockEnv := MockEnv(map[string]string{
		"LOG_LEVEL": "INVALID",
	})
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("Expected panic but none occurred")
		}
	}()
	logger.ConfigureLogger("test", &buf, mockEnv)
}

func TestPrintPanic_Recovery(t *testing.T) {
	defer logger.PrintPanic()
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("Expected panic but none occurred")
		}
	}()
	panic("Test panic")
}
