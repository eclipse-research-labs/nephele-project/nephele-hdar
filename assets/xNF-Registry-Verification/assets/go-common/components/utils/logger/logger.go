package logger

import (
	"fmt"
	"io"
	"os"

	logging "github.com/op/go-logging"
)

var (
	Log = ConfigureLogger("common", os.Stdout, getEnv)
)

type EnvFunc func(key string) string

// getEnv retrieves the value of the environment variable key.
func getEnv(key string) string {
	return os.Getenv(key)
}

// ConfigureLogger configures a logger based on environment variables.
func ConfigureLogger(name string, writer io.Writer, env EnvFunc) *logging.Logger {
	format := logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortpkg}/%{shortfile} %{level:.3s} %{id:03x}%{color:reset} ▶ %{message}`,
	)
	if env("LOGS_FORMAT") != "" {
		format = logging.MustStringFormatter(env("LOGS_FORMAT"))
	}
	logging.SetFormatter(format)
	log := logging.MustGetLogger(name)

	backend := logging.NewLogBackend(writer, "", 0)
	if env("LOGS_DISCARD") == "True" {
		backend = logging.NewLogBackend(io.Discard, "", 0)
	}
	backendLeveled := logging.AddModuleLevel(backend)

	if env("LOG_LEVEL") != "" {
		level, err := logging.LogLevel(env("LOG_LEVEL"))
		if err != nil {
			log.Panicf("Invalid log level: %s", err.Error())
			return log
		} else {
			backendLeveled.SetLevel(level, name)
		}
	}
	logging.SetBackend(backendLeveled)
	return log
}

// Useful as a deferred function to make sure we see panic errors when LOGS_DISCARD == True
func PrintPanic() {
	if r := recover(); r != nil {
		fmt.Println("Internal Error due to: ", r)
	}
}
