**Latest Tag**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-COMMON/go-common/wiki/gocommon-coverage-latesttag.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-COMMON/go-common/wiki/gocommon-version-latesttag.svg)

**Develop**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-COMMON/go-common/wiki/gocommon-coverage-develop.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-COMMON/go-common/wiki/gocommon-version-develop.svg)

# Filesystem

Common functions to work with the local filesystem.

## Env vars

Easy way to load env variables from a "." file (e.g., .env)

### Usage

```go
import "go-common/components/utils/filesystem"

func main(){
  filesystem.LoadEnvFile(".env") // relative to CWD
  varFromFile := os.Getenv("MY_LOCAL_ENV_FILE_VAR")
}
```

# Logger

Better approach for logging than fmt.Print.

Supports customization via env variables:

```text
# .env (automatically read)
LOGS_FORMAT= # default: `%{color}%{time:15:04:05.000} %{shortpkg}/%{shortfile} %{level:.3s} %{id:03x}%{color:reset} ▶ %{message}`
LOGS_DISCARD=True # default: False
LOG_LEVEL=INFO # default: DEBUG. Choose from ("CRITICAL",	"ERROR",	"WARNING",	"NOTICE",	"INFO",	DEBUG") 
```

## Usage

```go
import "go-common/components/utils/logger"

// Option 1: To separate loggers in the same component
var (
  Log = logger.ConfigureLogger("mymodule")
)
func main(){
  defer logger.PrintPanic() // make sure we see errors when LOGS_DISCARD=True
  // Option 1
  Log.Info("My message")
  
  // Option 2: Simpler usage
  
  logger.Log.Info("My message")
}
```

