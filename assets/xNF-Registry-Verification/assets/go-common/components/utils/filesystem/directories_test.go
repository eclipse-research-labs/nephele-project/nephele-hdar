package filesystem

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Test DeleteDirIfEmpty function
func TestDeleteDirIfEmpty_EmptyDir(t *testing.T) {
	// Create a temporary directory
	dir := t.TempDir()

	// Test the function with an empty directory
	err := DeleteDirIfEmpty(dir)
	assert.NoError(t, err, "DeleteDirIfEmpty should not return an error for empty dir")

	// Check that the directory is deleted
	_, err = os.Stat(dir)
	assert.True(t, os.IsNotExist(err), "Directory should be deleted if empty")
}

func TestDeleteDirIfEmpty_NonEmptyDir(t *testing.T) {
	// Create a temporary directory
	dir := t.TempDir()

	// Create a file inside the directory
	tmpfile, err := ioutil.TempFile(dir, "testfile")
	assert.NoError(t, err, "Should not fail creating a temp file")
	tmpfile.Close()

	// Test the function with a non-empty directory
	err = DeleteDirIfEmpty(dir)
	assert.NoError(t, err, "DeleteDirIfEmpty should not return an error for non-empty dir")

	// Check that the directory still exists
	_, err = os.Stat(dir)
	assert.NoError(t, err, "Directory should not be deleted if not empty")
}

func TestDeleteDirIfEmpty_DirectoryNotExist(t *testing.T) {
	// Test with a directory that doesn't exist
	err := DeleteDirIfEmpty("nonexistent_dir")
	assert.Error(t, err, "DeleteDirIfEmpty should return an error when the directory does not exist")
}

// Test CreateDirIfNotExist function
func TestCreateDirIfNotExist_NewDir(t *testing.T) {
	// Create a temporary directory
	dir := t.TempDir()

	// Define a new subdirectory path
	newDir := dir + "/new_subdir"

	// Test the function for creating a new directory
	err := CreateDirIfNotExist(newDir)
	assert.NoError(t, err, "CreateDirIfNotExist should not return an error when creating a new directory")

	// Check that the directory was created
	_, err = os.Stat(newDir)
	assert.NoError(t, err, "Directory should be created if it does not exist")
}

func TestCreateDirIfNotExist_DirAlreadyExists(t *testing.T) {
	// Create a temporary directory
	dir := t.TempDir()

	// Test the function for a directory that already exists
	err := CreateDirIfNotExist(dir)
	assert.NoError(t, err, "CreateDirIfNotExist should not return an error if the directory already exists")

	// Check that the directory still exists
	_, err = os.Stat(dir)
	assert.NoError(t, err, "Directory should still exist if it already existed")
}
