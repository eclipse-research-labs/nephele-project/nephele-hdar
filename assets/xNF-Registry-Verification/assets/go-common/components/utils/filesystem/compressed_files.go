package filesystem

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"go-common/components/utils/logger"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// pending to bring more from xnfrv/components/xnfrv-sdk/pkg/models (they are too specific to the asset)
func WriteToTar(out *tar.Writer, name string, body []byte) error {
	h := &tar.Header{
		Name:    filepath.ToSlash(name),
		Mode:    0644,
		Size:    int64(len(body)),
		ModTime: time.Now(),
	}
	if err := out.WriteHeader(h); err != nil {
		return err
	}
	_, err := out.Write(body)
	return err
}

func WriteGZFile(name, origin, destination string) error {
	// Prepare gz+tar writer to file
	if destination != "." {
		os.MkdirAll(destination, 0777)
	}
	tmpF, err := os.Create(filepath.Join(destination, name))
	if err != nil {
		tmpF.Close()
		os.Remove(filepath.Join(destination, name))
		DeleteDirIfEmpty(destination)
		return err
	}
	zipper := gzip.NewWriter(tmpF)
	tw := tar.NewWriter(zipper)
	defer func() {
		tw.Close()
		zipper.Close()
		tmpF.Close()
	}()
	// Save contents in tarball
	f, err := os.Stat(origin)
	if err != nil {
		tmpF.Close()
		os.Remove(filepath.Join(destination, name))
		DeleteDirIfEmpty(destination)
		return err
	} else if !f.IsDir() {
		tmpF.Close()
		os.Remove(filepath.Join(destination, name))
		DeleteDirIfEmpty(destination)
		return errors.New("Cannot tar a file without a parent directory.")
	} else {
		err = filepath.Walk(origin,
			func(fullPath string, info os.FileInfo, err error) error {
				if info, _ := os.Stat(fullPath); info.IsDir() {
					// ignore the folders
					return nil
				}
				if err != nil {
					return err
				}
				raw, err := ioutil.ReadFile(fullPath)
				if err != nil && err != io.EOF {
					return fmt.Errorf("File '%s' cannot be read: %s", fullPath, err)
				}
				unwantedParentDir, base := filepath.Split(origin)
				fullParent, _ := filepath.Split(fullPath)
				wantedParentDir := base
				wantedParentDir = strings.TrimPrefix(fullParent, unwantedParentDir)
				wantedParentDir = strings.TrimPrefix(wantedParentDir, "/")
				innerPath := filepath.Join(wantedParentDir, info.Name())
				logger.Log.Debugf("Writing %s\n", innerPath)
				return WriteToTar(tw, innerPath, raw)
			})
		if err != nil {
			tmpF.Close()
			os.Remove(filepath.Join(destination, name))
			DeleteDirIfEmpty(destination)
			return err
		}
	}
	return nil
}
