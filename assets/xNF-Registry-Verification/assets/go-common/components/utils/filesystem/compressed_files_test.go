package filesystem

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// Mock file info to simulate os.FileInfo in tests
type MockFileInfo struct {
	name  string
	isDir bool
}

func (mfi MockFileInfo) Name() string       { return mfi.name }
func (mfi MockFileInfo) Size() int64        { return 0 }
func (mfi MockFileInfo) Mode() os.FileMode  { return 0 }
func (mfi MockFileInfo) ModTime() time.Time { return time.Now() }
func (mfi MockFileInfo) IsDir() bool        { return mfi.isDir }
func (mfi MockFileInfo) Sys() interface{}   { return nil }

// Test WriteToTar function
func TestWriteToTar(t *testing.T) {
	var buf bytes.Buffer
	tarWriter := tar.NewWriter(&buf)
	defer tarWriter.Close()

	content := []byte("sample content")
	err := WriteToTar(tarWriter, "testfile.txt", content)

	assert.NoError(t, err, "WriteToTar should not return an error")
	// Now check if the tar entry is correctly written
	tr := tar.NewReader(&buf)
	header, err := tr.Next()

	assert.NoError(t, err, "Should be able to read tar header")
	assert.Equal(t, "testfile.txt", header.Name, "Tar file should have correct name")
	assert.Equal(t, int64(len(content)), header.Size, "Tar file should have correct size")

	tarContent, err := ioutil.ReadAll(tr)
	assert.NoError(t, err, "Should be able to read tar content")
	assert.Equal(t, content, tarContent, "Tar content should match the input content")
}

// Test WriteGZFile function
func TestWriteGZFile(t *testing.T) {
	// Mock data to simulate file content and directory structure
	dir := t.TempDir() // Temporary directory for the test
	mockFileName := "testfile.tar.gz"
	mockFilePath := filepath.Join(dir, mockFileName)
	originDir := filepath.Join(dir, "origin")
	os.Mkdir(originDir, 0755)

	// Write a file inside the origin directory
	mockFileContent := []byte("test content")
	mockFile := filepath.Join(originDir, "file.txt")
	err := ioutil.WriteFile(mockFile, mockFileContent, 0644)
	assert.NoError(t, err, "Should not fail writing the mock file")

	// Test WriteGZFile function
	err = WriteGZFile(mockFileName, originDir, dir)
	assert.NoError(t, err, "WriteGZFile should not return an error")

	// Validate the output file exists
	outputFile, err := os.Open(mockFilePath)
	defer outputFile.Close()
	assert.NoError(t, err, "Output .tar.gz file should be created")

	// Read and verify the gzip + tar content
	gzReader, err := gzip.NewReader(outputFile)
	assert.NoError(t, err, "Should be able to open gzip file")

	tarReader := tar.NewReader(gzReader)
	header, err := tarReader.Next()
	assert.NoError(t, err, "Should be able to read the tar header")
	assert.Equal(t, "origin/file.txt", header.Name, "Tarball should contain the correct file")

	tarContent, err := ioutil.ReadAll(tarReader)
	assert.NoError(t, err, "Should be able to read the file from the tarball")
	assert.Equal(t, mockFileContent, tarContent, "Tarball content should match the original file content")
}

// Test handling of error cases
func TestWriteGZFile_FileNotFound(t *testing.T) {
	dir := t.TempDir()
	err := WriteGZFile("nonexistent.tar.gz", "invalid/path", dir)
	assert.Error(t, err, "Should return an error when origin directory does not exist")
}

func TestWriteGZFile_CannotTarFileWithoutParent(t *testing.T) {
	dir := t.TempDir()
	mockFilePath := filepath.Join(dir, "file.txt")
	err := ioutil.WriteFile(mockFilePath, []byte("content"), 0644)
	assert.NoError(t, err, "Should not fail writing the mock file")

	err = WriteGZFile("file.tar.gz", mockFilePath, dir)
	assert.EqualError(t, err, "Cannot tar a file without a parent directory.", "Should return error for single file")
}
