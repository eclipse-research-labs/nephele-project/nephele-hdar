package filesystem

import (
	"go-common/components/utils/logger"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Wrapper function to duplicate a file with a different name/location and possibly delete the original one
func RenameFile(src string, dst string, del bool) error {
	input, err := ioutil.ReadFile(src)
	if err != nil {
		logger.Log.Infof("Unable to read file %s\n", err.Error())
		return err
	}
	baseDir := filepath.Dir(dst)
	err = CreateDirIfNotExist(baseDir)
	if err != nil {
		logger.Log.Infof("Error creating destination dir %s\n", err.Error())
		return err
	}
	err = ioutil.WriteFile(dst, input, 0644)
	if err != nil {
		logger.Log.Infof("Error creating destination file %s\n", err.Error())
		return err
	}
	if del {
		logger.Log.Infof("Deleting %s\n", src)
		if err := os.Remove(src); err != nil {
			logger.Log.Infof("Error deleting original file %s\n", err.Error())
			return err
		}
	}
	return nil
}
