module go-common/components/utils

go 1.22.7

require (
	github.com/joho/godotenv v1.5.1
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/stretchr/testify v1.9.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace go-common/components/utils/ => ./
