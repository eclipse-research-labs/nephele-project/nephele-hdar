package main

import (
	"go-common/components/utils/logger"
	"xnfrv/components/xnfv-listeners/pkg/redis"
)

func main() {
	logger.Log.Info("Starting xNF Verification Engine")
	// Run listeners as go-routines in the background.
	go redis.CreateAndRunListener()
	go redis.CreateAndRunListener()
	go redis.CreateAndRunListener()
	// Block with the last one
	redis.CreateAndRunListener()
	logger.Log.Info("Finished")
}
