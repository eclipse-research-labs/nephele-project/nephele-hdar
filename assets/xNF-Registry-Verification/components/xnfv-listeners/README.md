# xNF Verification Engine Listeners

**Latest Tag Release**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfvlisteners-coverage-latesttag.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfvlisteners-version-latesttag.svg)

**Develop**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfvlisteners-coverage-develop.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfvlisteners-version-develop.svg)


## Description

Provides the Redis Workers pool to cary out the verification pipeline of the artifacts received in the OCI Registry.

## Getting Started

- listeners: Baseline implementation of the xnfrv-sdk/interfaces/listeners which can be extended by other projects by correctly configuring their listener_selector method.
- redis: Pub/sub implementation using the Redis cache from the OCI Registry

### Prerequisites

Full configuration via ENV Vars:

- Check $PWD/install/.env-test for local deployment and move it to $PWD/.env

#### Software

Project built using Golang 1.22.

#### Hardware

No major requirements found. 1vCPU, 1GiB RAM

### Installation

Project developed using WSL Golang installation

1. Go auto formatting and linting. Execute before pushing to origin. It is automated in CI pipeline and will fail if not ok

```bash
go fmt $(go list ./... | grep -v /vendor/)
go vet $(go list ./... | grep -v /vendor/)
```

2. Build the program

```bash
GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o xnfvlisteners . 
```

3. Make sure tests pass

```bash
go test $(go list ./... | grep -v /vendor/) -v
```

## Usage

### Locally

A) Manually

If you want to run it locally, you need to replicate the local environment from the registry.atosresearch.eu:18467/xnfv_base image -> install/baseline.dockerfile. Then, just run with:

```bash
# adapt env vars
cp install/.env-tests .env
export $(grep -v '^#' .env | xargs)
./xnfvlisteners
```

B) Create docker image



```bash
# we need access to the full repo
cd ../../
docker build -t registry.atosresearch.eu:18467/xnfvlisteners:latest -f components/xnfv-listeners/install/dockerfile --progress plain  .
# adapt env vars from install/.env-tests
cd components/xnfv-listeners/
docker run registry.atosresearch.eu:18467/xnfvlisteners:latest --env-file install/.env-tests
```

### Production

Rely on the ../../deploy/README.md

## Testing

Golang tests are provided as part of the source code.


## Contribution

Tech:

- Guillermo Gomez guillermo.gomezchavez@eviden.com

Asset Owner:

- Sonia Castro sonia.castro@eviden.com

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or PR created from GitHub as draft.
3. Commit and push frequently.
4. When ready, set PR as ready, tag team and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```
