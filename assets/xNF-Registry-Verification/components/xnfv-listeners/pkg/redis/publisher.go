package redis

import (
	"encoding/json"

	"github.com/hibiken/asynq"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

// Client Publisher
type RedisPublisher struct {
	r *RedisConnection
}

func NewPublisher() *RedisPublisher {
	r, e := NewRedisConnection()
	if e != nil {
		logger.Log.Error(e.Error())
		return nil
	}
	return &RedisPublisher{
		r: r,
	}
}

func (p *RedisPublisher) PublishPushEvent(e models.OCIEvent) error {
	// Connect & Disconnect
	p.r.InitPublisher()
	defer p.r.Publisher.Close()

	// Generate JSON
	payload, err := json.Marshal(e)
	if err != nil {
		logger.Log.Errorf(err.Error())
		return err
	}
	// TODO: Check if same image already with action.
	// If request user agent is xnfv discard Cancel and rerun
	task := asynq.NewTask(PushEvent, payload)

	// Request to process the task immediately in critical queue.
	_, err = p.r.Publisher.Enqueue(
		task,                    // task payload
		asynq.Queue("critical"), // set queue for task
	)
	if err != nil {
		logger.Log.Errorf(err.Error())
		return err
	}
	return nil
}

func (p *RedisPublisher) PublishConfigureEvent() error {
	// Connect & Disconnect
	p.r.InitPublisher()
	defer p.r.Publisher.Close()

	task := asynq.NewTask(ConfigureEvent, []byte{})

	// Request to process the task immediately in critical queue.
	_, err := p.r.Publisher.Enqueue(
		task,                    // task payload
		asynq.Queue("critical"), // set queue for task
	)
	if err != nil {
		logger.Log.Errorf(err.Error())
		return err
	}
	return nil
}
