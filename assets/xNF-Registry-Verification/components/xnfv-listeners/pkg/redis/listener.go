package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/hibiken/asynq"
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	"gopkg.in/yaml.v2"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfv-listeners/pkg/listeners"
)

type RedisListener struct {
	r *RedisConnection
}

func NewListener() (*RedisListener, error) {
	r, e := NewRedisConnection()
	if e != nil {
		logger.Log.Error(e.Error())
		return nil, e
	}
	l := &RedisListener{
		r: r,
	}
	l.configureListener()
	return l, nil
}

// Worker listener
func (l *RedisListener) configureListener() {
	// Define a task handler per topic
	l.r.Mux.HandleFunc(
		PushEvent,         // task type
		l.HandlePushEvent, // handler function
	)
	l.r.Mux.HandleFunc(
		ConfigureEvent,    // task type
		l.HandleConfigure, // handler function
	)
}

func (l *RedisListener) HandleConfigure(ctx context.Context, t *asynq.Task) error {
	// Create login record with admin privileges
	logger.Log.Info("Requested to configure")
	logger.Log.Info("Saving credentials to OCI Registry")
	regURL := strings.TrimSpace(os.Getenv("REGISTRY_URL"))
	regPass := strings.TrimSpace(os.Getenv("REGISTRY_ADMIN_PASSWORD"))
	regUser := strings.TrimSpace(os.Getenv("REGISTRY_ADMIN_USER"))
	logger.Log.Debugf("Login info '%s', '%s', '%s'", regURL, regUser, regPass)
	if regURL == "" || regPass == "" || regUser == "" {
		logger.Log.Fatal("Login info not available")
		return fmt.Errorf("Login info '%s', '%s', '%s' not valid", regURL, regUser, regPass)
	}
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "login", regURL, "-u", regUser, "-p", regPass)
	stdout, err := cmd.Output()
	if err != nil {
		logger.Log.Errorf("Cannot log to registry: %s\n", err.Error())
		return err
	}
	logger.Log.Infof("login info: %s\n", stdout)
	return nil
}

func (l *RedisListener) HandlePushEvent(ctx context.Context, t *asynq.Task) error {
	logger.Log.Info("Received an event")
	var event models.OCIEvent
	err := json.Unmarshal(t.Payload(), &event)
	if err != nil {
		logger.Log.Error("Event unmarsharl error %s\n", err.Error())
		return err
	}
	report := models.NewVerificationReport(&event)
	err = os.MkdirAll(report.Id, 0777)
	defer os.RemoveAll(report.Id)
	if err != nil {
		logger.Log.Infof("Unable to create the desired destination path: %s\n", err.Error())
		return err
	}
	// Find out what type of artifact it is

	//Second: Based on the config mediaType
	// All events are for targe.MediaType="application/vnd.oci.image.manifest.v1+json"
	// Pull the artifact's manifests
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "manifest", report.Artifact.ArtifactFullRepo)
	output, err := cmd.CombinedOutput()
	if err != nil {
		logger.Log.Errorf("Cannot pull manifests: %s -> %s\n", err.Error(), string(output))
		return err
	}
	logger.Log.Debug(string(output))
	// Read the image manifest content (name convention from xNFRV CLI repo)
	manifestId := strings.ReplaceAll(event.Target.Repository, "/", "-")
	imageManifest := fmt.Sprintf("./manifest-image-%s.json", manifestId)
	configManifest := fmt.Sprintf("./manifest-config-%s.json", manifestId)
	content, err := os.ReadFile(imageManifest)
	defer os.Remove(imageManifest)
	defer os.Remove(configManifest)
	if err != nil {
		logger.Log.Errorf("Error when opening file: %s\n", err.Error())
		return err // Should upload report anyway??
	}

	logger.Log.Debug(string(content))
	// Now let's unmarshall the data into `payload`
	var payload v1.Manifest
	err = json.Unmarshal(content, &payload)
	if err != nil {
		logger.Log.Errorf("Error when opening file: %s\n", err.Error())
		return err // Should upload report anyway??
	}
	logger.Log.Debugf("report before: %+v\n", report)

	// Select the right listener implementation and execute the pipeline
	configMediaType := payload.Config.MediaType
	if configMediaType == "application/vnd.eviden.verification.config.v1+json" {
		logger.Log.Info("Received a verification report. Ignoring it")
		return nil
	} else if configMediaType == "application/vnd.oci.image.config.v1+json" && payload.Layers[0].MediaType == "application/vnd.dev.cosign.simplesigning.v1+json" {
		logger.Log.Info("Received a Signature. Ignoring it")
		return nil
	}
	listener := listeners.ListenerConfigInstance.SelectListenerFunction(payload)
	if listener == nil {
		logger.Log.Errorf("unable to obtain the right Listener implementation for %s\n", configMediaType)
		return err
	}
	logger.Log.Debugf("listener: %+v\n", listener)
	listener.ExecutePipeline(report)
	logger.Log.Debugf("report after: %+v\n", report)
	// transfer report to a local file, package and push
	logger.Log.Debug("Creating report artifact")
	data, err := yaml.Marshal(report)
	if err != nil {
		logger.Log.Infof("Unable to load report: %s\n", err.Error())
		return err
	}
	err = filesystem.CreateDirIfNotExist(report.Id)
	if err != nil {
		logger.Log.Infof("Unable to create base dir for report: %s\n", err.Error())
		return err
	}
	defer os.RemoveAll(report.Id)
	defer os.Remove(report.Scan.ResultsFile)
	defer os.Remove(report.Lint.ResultsFile)
	if err := os.WriteFile(filepath.Join(report.Id, "report.yaml"), data, 0644); err != nil {
		logger.Log.Infof("Unable Move files to location: %s\n", err.Error())
		return err
	}
	if report.Scan.ResultsFile != "" {
		if err := os.Rename(report.Scan.ResultsFile, filepath.Join(report.Id, report.Scan.ResultsFile)); err != nil {
			logger.Log.Infof("Unable Move files to location: %s\n", err.Error())
			return err

		}
	}
	if report.Lint.ResultsFile != "" {
		if err := os.Rename(report.Lint.ResultsFile, filepath.Join(report.Id, report.Lint.ResultsFile)); err != nil {
			logger.Log.Infof("Unable Move files to location: %s\n", err.Error())
			return err
		}
	}

	cmd = exec.Command(os.Getenv("DEFAULT_CLI"), "package", "tar", report.Id)
	output, err = cmd.CombinedOutput()
	if err != nil {
		logger.Log.Infof("Unable to combine cli output: %s\n", err.Error())
		return err
	}
	logger.Log.Debug(output)
	// Push to the same base repository as the artifact
	reportFullRepoName := report.Artifact.ArtifactFullRepo
	logger.Log.Debugf("Geting registry base url from %s\n", reportFullRepoName)
	lastIndex := strings.LastIndex(reportFullRepoName, "/")
	if lastIndex != -1 {
		logger.Log.Debugf("index is %d\n", lastIndex)
		reportFullRepoName = reportFullRepoName[:lastIndex]
	}
	reportArtifactName := fmt.Sprintf("%s-%s.tar.gz", report.Id, report.Artifact.ArtifactVersion)
	defer os.Remove(reportArtifactName)
	logger.Log.Debugf("reportArtifactName for push %s\n", reportArtifactName)
	logger.Log.Debugf("reportFullRepoName for push %s\n", reportFullRepoName)
	// Subject is automatically generated by the artifact_inspector
	cmd = exec.Command(os.Getenv("DEFAULT_CLI"), "push", reportArtifactName, reportFullRepoName)
	output, err = cmd.CombinedOutput()
	if err != nil {
		logger.Log.Errorf("Cannot push report: %s -> %s\n", err.Error(), string(output))
		return err
	}
	logger.Log.Debug(string(output))

	return nil
}

func (l *RedisListener) listenAndBlock() {
	// Run worker server.
	if err := l.r.Listener.Run(l.r.Mux); err != nil {
		logger.Log.Fatal(err)
	}
}

func CreateAndRunListener() {
	listener, err := NewListener()
	if err != nil {
		logger.Log.Fatal("Unable to start the Redis listeners")
	}
	listener.listenAndBlock()
}
