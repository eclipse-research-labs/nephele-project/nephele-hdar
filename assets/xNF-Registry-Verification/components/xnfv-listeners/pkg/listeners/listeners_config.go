package listeners

import (
	lib "xnfrv/components/xnfrv-sdk/pkg/listeners"
)

// Baseline configuration of the CMD. This is overwritten by projects extending the xnfrctl thanks to the methods in inspector_selector.go
var ListenerConfigInstance = &lib.SelectListener{
	SelectListenerFunction: AddCorrectListener,
}
