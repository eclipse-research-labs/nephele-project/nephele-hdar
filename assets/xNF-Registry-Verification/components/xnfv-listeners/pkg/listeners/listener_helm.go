package listeners

import (
	"go-common/components/utils/logger"
	"os"
	"os/exec"
	"strings"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/utils"
)

// Implements ListenerInterface
type ListenerHelm struct {
}

func (v *ListenerHelm) ExecutePipeline(report *models.VerificationReport) {
	logger.Log.Debug("Detected helm event")
	// helm charts pushed with the helm cli are based on ORAS v1 which is not compatible
	// with our implementation
	// Fix helm pull with --plain-http
	helmRepoURL := strings.TrimPrefix(report.Artifact.ArtifactFullRepo, "http://")
	helmArg := ""
	if helmRepoURL != report.Artifact.ArtifactFullRepo {
		helmArg = "--plain-http"
	}
	helmRepoURL = strings.TrimPrefix(helmRepoURL, "https://")
	// Helm command does not allow tag in the repoURL
	repoParts := strings.Split(helmRepoURL, ":")
	logger.Log.Debugf("OCI://" + repoParts[0] + " " + helmArg)
	hcmd := []string{"pull", "OCI://" + repoParts[0], "--version", repoParts[1], "--debug"}
	if helmArg != "" {
		hcmd = append(hcmd, helmArg)
	}
	cmd := exec.Command("helm", hcmd...)
	output, err := cmd.CombinedOutput()
	logger.Log.Debug(string(output))
	aName := report.Artifact.ArtifactID + "-" + report.Artifact.ArtifactVersion + ".tgz"
	defer os.Remove(aName)
	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, "cannot pull the artifact")
		logger.Log.Errorf("Cannot pull artifact with %s: %s -> %s\n", "helm", err.Error(), "cannot pull the artifact")
		return
	}
	logger.Log.Debugf("aName for pipeline: %s\n", aName)
	v.StageLint(report, aName)
	v.StageScan(report, aName)
	v.StageIntegrity(report, "")
}

func (v *ListenerHelm) StageIntegrity(report *models.VerificationReport, artifactName string) {
	// Sign artifact with cosign
	CosignIntegrity(report)
}

func (v *ListenerHelm) StageScan(report *models.VerificationReport, artifactName string) {
	// Begin scan
	nMap, err := utils.GetFileFormat(artifactName)
	if err != nil {
		report.Scan.Report.Success = false
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, err.Error())
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, "Unable to infer name and tag from artifact filename")
		logger.Log.Errorf("Unable to infer name and tag from artifact filename: %s", err.Error())
	}
	scanFile := "scan-" + nMap["name"] + "-" + nMap["version"] + ".json"
	// defer os.Remove(scanFile)
	cmd := exec.Command("trivy", "conf", artifactName, "-f", "json", "-o", scanFile)
	output, err := cmd.CombinedOutput()
	report.Scan.Report.Tool = "trivy conf"
	if err != nil {
		report.Scan.Report.Success = false
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, err.Error())
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, string(output))
		logger.Log.Errorf("Cannot scan artifact with trivy: %s -> %s\n", err.Error(), string(output))
	} else {
		report.Scan.Report.Success = true // TODO set it to false if any Critical CVE is found
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, string(output))
		logger.Log.Debug(string(output))
		report.Scan.ResultsFile = scanFile
	}

}

func (v *ListenerHelm) StageLint(report *models.VerificationReport, artifactName string) {
	logger.Log.Debug("Detected Helm image event")
	cmd := exec.Command("helm", "lint", artifactName)
	output, err := cmd.CombinedOutput()
	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, string(output))
		logger.Log.Errorf("Cannot lint artifact with helm: %s -> %s\n", err.Error(), string(output))
	} else {
		report.Lint.Report.Success = true
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, string(output))
		logger.Log.Debug(string(output))
	}

}
