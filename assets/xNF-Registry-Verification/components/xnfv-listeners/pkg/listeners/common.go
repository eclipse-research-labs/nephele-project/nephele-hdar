package listeners

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

func CosignIntegrity(report *models.VerificationReport) {
	// cosign sign --key cosign.key registry.nephele-hdar.netmode.ece.ntua.gr/chart/nginx:latest -y --tlog-upload=false --record-creation-timestamp
	// In order to inyect the passphrase we need the env var COSIGN_PASSWORD
	// TODO: change to digest signature:
	// uses a tag, not a digest, to identify the image to sign.
	//   This can lead you to sign a different image than the intended one. Please use a
	//   digest (example.com/ubuntu@sha256:abc123...) rather than tag
	//   (example.com/ubuntu:latest) for the input to cosign. The ability to refer to
	//   images by tag will be removed in a future release.
	report.Integrity.Report.Tool = "cosign"
	cmdParams := []string{"sign", "--key", os.Getenv("KEY_PATH"), report.Artifact.ArtifactFullRepo, "-y", "--tlog-upload=false", "--record-creation-timestamp"}
	cmd := exec.Command("cosign", cmdParams...)
	output, err := cmd.CombinedOutput()
	logger.Log.Debug("cosign " + string(output))
	if err != nil {
		report.Integrity.Report.Success = false
		report.Integrity.Report.Logs = append(report.Integrity.Report.Logs, err.Error())
		report.Integrity.Report.Logs = append(report.Integrity.Report.Logs, string(output))
		report.Integrity.Report.Logs = append(report.Integrity.Report.Logs, "cosign failed")
		logger.Log.Errorf("Cosign failed with params '%s': %s -> %s\n", strings.Join(cmdParams, " "), err.Error(), string(output))
		return
	}
	report.Integrity.Report.Success = true
	report.Integrity.Report.Logs = append(report.Integrity.Report.Logs, string(output))
	report.Integrity.SignatureImage = "FROM stdout?" // todo fix this
}

func PullArtifact(report *models.VerificationReport) (artifactName string, err error) {
	// --untar deletes the original .tar.gz /.tgz. We want both here
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "pull", report.Artifact.ArtifactFullRepo)
	output, err := cmd.CombinedOutput()
	if err != nil {
		logger.Log.Errorf("Cannot pull artifact: %s -> %s\n", err.Error(), string(output))
		return "", err // Should upload report anyway??
	}
	logger.Log.Debug(string(output))
	// It seems to exit before the artifact is actually downloaded...
	// Name convention from OCI artifacts name:tag -> name-tag.tgz or name-tag.tar.gz
	aName := fmt.Sprintf("%s-%s.tar.gz", report.Artifact.ArtifactID, report.Artifact.ArtifactVersion)
	_, err = os.Stat(aName)
	if err != nil {
		logger.Log.Errorf("Unable to find the local artifact after pull: %s -> %s\n", aName, err.Error())
		aName = fmt.Sprintf("%s-%s.tgz", report.Artifact.ArtifactID, report.Artifact.ArtifactVersion)
		_, err = os.Stat(aName)
		if err != nil {
			logger.Log.Errorf("Unable to find the local artifact after pull: %s -> %s\n", aName, err.Error())
			return "", err // Should upload report anyway??
		}
	}
	return aName, err
}
