package listeners

import (
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	"xnfrv/components/xnfrv-sdk/pkg/interfaces"
)

// xnfv library implementations of listeners, other projects shall try to infer the required
// listener type first and only if not found, use this ones
func AddCorrectListener(manifest v1.Manifest) interfaces.ListenerInterface {
	switch manifest.Config.MediaType {
	case "application/vnd.etsi.nfv.config.v1+json":
		return &ListenerOSM{}
	case "application/vnd.cncf.helm.config.v1+json":
		return &ListenerHelm{}
	case "application/vnd.docker.container.image.v1+json":
		return &ListenerDocker{}
	}
	return nil
}
