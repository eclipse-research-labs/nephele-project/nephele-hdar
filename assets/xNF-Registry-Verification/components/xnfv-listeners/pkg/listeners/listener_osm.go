package listeners

import (
	"go-common/components/utils/logger"
	"os"
	"os/exec"
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

// Implements ListenerInterface
type ListenerOSM struct {
}

func (v *ListenerOSM) ExecutePipeline(report *models.VerificationReport) {
	logger.Log.Debug("Detected NFV image event")
	aName, err := PullArtifact(report)
	defer os.Remove(aName)
	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, "cannot pull the artifact")
		logger.Log.Errorf("Cannot pull artifact with %s: %s -> %s\n", os.Getenv("DEFAULT_CLI"), err.Error(), "cannot pull the artifact")
		return
	}
	logger.Log.Debugf("aName for pipeline: %s\n", aName)
	v.StageLint(report, aName)
	v.StageIntegrity(report, "")

}

func (v *ListenerOSM) StageIntegrity(report *models.VerificationReport, artifactName string) {
	// Sign artifact with cosign
	CosignIntegrity(report)
}

func (v *ListenerOSM) StageScan(report *models.VerificationReport, artifactName string) {
	// ToDO: Implement NFV CVE or missconfiguration scan!
}

func (v *ListenerOSM) StageLint(report *models.VerificationReport, artifactName string) {
	// TODO: Replace with OSM cli
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "lint", artifactName)
	output, err := cmd.CombinedOutput()
	report.Lint.Report.Tool = os.Getenv("DEFAULT_CLI")

	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, string(output))
		logger.Log.Errorf("Cannot lint artifact with %s: %s -> %s\n", os.Getenv("DEFAULT_CLI"), err.Error(), string(output))
	} else {
		report.Lint.Report.Success = true
		logger.Log.Debug(string(output))
	}
	logger.Log.Debugf("Lint report: %+v\n", report)
}
