package listeners

import (
	"go-common/components/utils/logger"
	"os/exec"
	"strings"
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

// Implements ListenerInterface
type ListenerDocker struct {
}

func (v *ListenerDocker) ExecutePipeline(report *models.VerificationReport) {
	logger.Log.Debug("Detected Docker image event")
	v.StageScan(report, "")
	v.StageIntegrity(report, "")
}

func (v *ListenerDocker) StageIntegrity(report *models.VerificationReport, artifactName string) {
	// Sign artifact with cosign
	CosignIntegrity(report)
}

func (v *ListenerDocker) StageScan(report *models.VerificationReport, artifactName string) {
	// Fix trivy pull with plain-http
	dockerRepoURL := strings.TrimPrefix(report.Artifact.ArtifactFullRepo, "http://")
	dockerRepoURL = strings.TrimPrefix(dockerRepoURL, "https://")
	scanFile := "scan-" + report.Artifact.ArtifactID + "-" + report.Artifact.ArtifactVersion + ".json"
	// defer os.Remove(scanFile)
	logger.Log.Debugf("About to scan %s\n", dockerRepoURL)
	cmd := &exec.Cmd{}
	if dockerRepoURL != report.Artifact.ArtifactFullRepo {
		cmd = exec.Command("trivy", "image", "-f", "json", "-o", scanFile, "--insecure", dockerRepoURL)
	} else {
		cmd = exec.Command("trivy", "image", "-f", "json", "-o", scanFile, dockerRepoURL)
	}
	// Begin scan
	output, err := cmd.CombinedOutput()
	report.Scan.Report.Tool = "trivy image"
	if err != nil {
		report.Scan.Report.Success = false
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, err.Error())
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, string(output))
		logger.Log.Errorf("Cannot scan artifact with trivy: %s -> %s\n", err.Error(), string(output))
	} else {
		report.Scan.Report.Success = true // Should probably set it to false if any Critical CVE is found
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, string(output))
		logger.Log.Debug(string(output))
		report.Scan.ResultsFile = scanFile
	}

}

func (v *ListenerDocker) StageLint(report *models.VerificationReport, artifactName string) {

}
