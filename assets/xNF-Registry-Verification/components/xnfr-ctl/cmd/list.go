package cmd

import (
	"context"
	"fmt"
	"os"
	"text/tabwriter"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/registry"

	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "ls [REGISTRY] [flags]",
	Short: "List all artifacts in a repository",
	Long:  `Shows all artifacts along with their mediaType`,
	Args:  cobra.ExactArgs(1),
	RunE:  ListCmdRun,
}

func ConfigureListCmd(c *cobra.Command) {
	// Add option registry here??
	c.PersistentFlags().StringP("repository", "r", "", "Path to the repository. Empty for full root")
}

func ListCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	repoPath, _ := cmd.Flags().GetString("repository")
	var contentMap map[string]string

	reg, err := registry.NewRegWithExistingDockerCredentials(args[0])
	if err != nil {
		logger.Log.Infof("Unable to create registry %s\n", err.Error())
		return err
	}
	orderedRepos, err := registry.List(reg)
	w := tabwriter.NewWriter(os.Stdout, 1, 1, 1, ' ', tabwriter.Debug|tabwriter.DiscardEmptyColumns)
	fmt.Fprintln(w, "Artifact\tmediaType\t")
	for _, k := range orderedRepos {
		if k == repoPath {
			logger.Log.Debugf("Ignoring %s\n", k)
		}
		fmt.Fprintf(w, "%s\t%s\t\n", k, contentMap[k])
	}

	w.Flush()
	ctx := context.WithValue(cmd.Context(), CmdFailed, false)
	cmd.SetContext(ctx)
	return nil
}
