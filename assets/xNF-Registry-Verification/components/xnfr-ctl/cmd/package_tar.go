package cmd

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
)

var tarCmd = &cobra.Command{
	Use:   "tar [PATH]",
	Short: "Package PATH as a tar.gz file.",
	Long:  `Packages the directory pointed by PATH as a tar.gz file.`,
	Args:  cobra.ExactArgs(1),
	RunE:  TarCmdRun,
}

func init() {
	packageCmd.AddCommand(tarCmd)
}

func TarCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	path, dPath, err := ValidatePackageCmdInput(args, cmd.Flags())
	if err != nil {
		err = fmt.Errorf(err.Error())
		return err
	}
	tmpA, err := artifact.NewTmpArtifactFromFolder(path)
	defer os.Remove("tmp.tar.gz")
	if err != nil {
		return err
	} else if found, err := ConfigCmdInstance.AddCorrectInspectorFunction(tmpA, false); !found {
		err = fmt.Errorf("Unable to add correct inspector implementation during tar\n")
		return err
	} else if found && err != nil {
		err = fmt.Errorf("Verification failed: %s", err.Error())
		return err
	}
	aName, err := artifact.TmpArtifactToLocalArtifact(tmpA, dPath)
	if err != nil {
		return err
	}
	// Verify the new artifact
	a, err := artifact.NewLocalArtifact(aName)
	if err != nil {
		return err
		// verification is automatically
	} else if found, err := ConfigCmdInstance.AddCorrectInspectorFunction(a, true); err != nil {
		err = fmt.Errorf("Error in Artifact: %s", err.Error())
		return err
	} else if !found {
		return errors.New("unable to find the right inspector")
	}
	fmt.Printf("Package created and validated %s\n", aName)
	return nil
}
