package cmd

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"

	"github.com/spf13/cobra"
)

var lintCmd = &cobra.Command{
	Use:   "lint [PATH]",
	Short: "Verifies syntax",
	Long:  `Verifies syntax of a descriptor in folder or artifact and the structure`,
	Args:  cobra.ExactArgs(1),
	RunE:  LintCmdRun,
}

func LintCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	// Performs syntax and semantic verification of descriptor
	// Read the artifact
	arg_dir := strings.TrimSuffix(args[0], "/")
	fInfo, err := os.Stat(arg_dir)
	if err != nil {
		return err
	}
	var aName string
	if fInfo.IsDir() {
		// Create a Local Artifact
		tmpA, err := artifact.NewTmpArtifactFromFolder(arg_dir)
		defer os.Remove("tmp.tar.gz")
		if err != nil {
			return err
		} else if found, err := ConfigCmdInstance.AddCorrectInspectorFunction(tmpA, false); !found {
			logger.Log.Info("Not found")
			err = fmt.Errorf("Unable to add correct inspector implementation during lint\n")
			return err
		} else if found && err != nil {
			logger.Log.Info("found but error")
			err = fmt.Errorf("Verification failed: %s", err.Error())
			return err
		}
		logger.Log.Info("No error")
		aName, err = artifact.TmpArtifactToLocalArtifact(tmpA, arg_dir)
		defer os.Remove(aName)
		if err != nil {
			return err
		}
	} else {
		aName = arg_dir
	}
	a, err := artifact.NewLocalArtifact(aName)
	if err != nil {
		return err
		// verification is automatically
	} else if found, err := ConfigCmdInstance.AddCorrectInspectorFunction(a, true); err != nil {
		err = fmt.Errorf("Error in Artifact: %s", err.Error())
		return err
	} else if !found {
		return errors.New("unable to find the right inspector")
	}
	fmt.Println("Package verified")
	return nil
}
