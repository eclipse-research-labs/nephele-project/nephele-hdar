package cmd

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

var manifestCmd = &cobra.Command{
	Use:   "manifest [REGISTRY/REPOSITORY/NAME:TAG] [FLAGS]",
	Short: "Pulls an OCI manifest. ",
	Long:  `Downloads the the OCI manifests for image and config.`,
	RunE:  ManifestCmdRun,
}

func ConfigureManifestCmd(cmd *cobra.Command) {
	cmd.PersistentFlags().StringP("destination", "d", ".", "Base directory where the manifest will be downloaded")
	return
}

func ManifestCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	// Check input info
	if len(args) != 1 {
		err := fmt.Errorf("Only accepts 2 arguments [REGISTRY/REPOSITORY/NAME:TAG]\n")
		return err
	}
	// Prepare destination
	fBase, err := cmd.Flags().GetString("destination")
	if err != nil {
		err = fmt.Errorf("Unable to read destination flag\n")
		err = fmt.Errorf(err.Error())
		return err
	}
	err = filesystem.CreateDirIfNotExist(fBase)
	if err != nil {
		err = fmt.Errorf("Unable to create destination dir\n")
		err = fmt.Errorf(err.Error())
		return err
	}
	// 1. Prepare connection to remote registry
	// Check that args is correct (repo.Reference gives the info about reg, rep and image)
	repo, err := registry.NewRepoWithExistingDockerCredentials(args[0])
	if err != nil {
		err = fmt.Errorf("Unable to create repo\n")
		err = fmt.Errorf(err.Error())
		return err
	}
	logger.Log.Debug("Repo created ok")
	// 2. Pull the info
	manifests, err := registry.FetchManifest(repo)

	if err != nil {
		logger.Log.Errorf("Unable to fetch manifests %s\n", err.Error())
		return err
	}

	err = fmt.Errorf("Pull successful\n")
	// 3. Save as local files
	fi, _ := os.Create(fmt.Sprintf(filepath.Join(fBase, "manifest-image-%s.json"), strings.ReplaceAll(repo.Reference.Repository, "/", "-")))
	fc, _ := os.Create(fmt.Sprintf(filepath.Join(fBase, "manifest-config-%s.json"), strings.ReplaceAll(repo.Reference.Repository, "/", "-")))
	defer fi.Close()
	defer fc.Close()
	fi.Write(manifests.ImageManifestContent)
	fc.Write(manifests.ConfigManifestContent)
	ctx := context.WithValue(cmd.Context(), CmdFailed, false)
	cmd.SetContext(ctx)
	return nil

}
