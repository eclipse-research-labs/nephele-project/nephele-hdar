package cmd

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"golang.org/x/term"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

var loginCmd = &cobra.Command{
	Use:   "login [REGISTRY] [flags]",
	Short: "Log in to a remote registry",
	Long:  `Log in to a remote registry. Registry might be a url or ip:port. No repository is needed.`,
	Args:  cobra.ExactArgs(1),
	RunE:  LoginCmdRun,
}

func ConfigureLoginCmd(c *cobra.Command) {
	c.PersistentFlags().StringP("username", "u", "", "username in the registry")
	c.PersistentFlags().StringP("password", "p", "", "Password or Personal Access Token. More secure option for password 'echo $PASSWORD |' and then your login command with only the -u option")
}

func LoginCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	// We store credentials in the standard docker store for the local environment
	p, err := cmd.Flags().GetString("password")
	if err != nil {
		err = fmt.Errorf("Unable to process input %s\n", err.Error())
		return err
	}
	u, err := cmd.Flags().GetString("username")
	if err != nil {
		err = fmt.Errorf("Unable to process input %s\n", err.Error())
		return err
	}
	if u == "" {
		// prompt for username
		u, err = readLine("Username: ", false)
		if err != nil {
			err = fmt.Errorf("Unable to process input %s\n", err.Error())
			return err
		}
		u = strings.TrimSpace(u)
	}
	if p == "" {
		// prompt for token
		if p, err = readLine("Password: ", true); err != nil {
			err = fmt.Errorf("Unable to process input %s\n", err.Error())
			return err
		}
		p = strings.TrimSpace(p)
	}
	if p == "" {
		err = fmt.Errorf("Password not provided %s\n", err.Error())
		return err
	}

	_, err = registry.NewRegWithStaticCredentials(args[0], u, p)
	if err != nil {
		err = fmt.Errorf("Unable to access reg %+v with credentials given:  %s\n", args[0], err.Error())
		return err
	}

	err = fmt.Errorf("Login Succeeded\n")
	ctx := context.WithValue(cmd.Context(), CmdFailed, false)
	cmd.SetContext(ctx)
	return nil
}

// Can be replaced with 'echo $PASS |' then the password is automatically taken from stdin
func readLine(prompt string, silent bool) (string, error) {
	fmt.Print(prompt)
	fd := int(os.Stdin.Fd())
	var bytes []byte
	var err error
	// not working
	if silent && term.IsTerminal(fd) {
		if bytes, err = term.ReadPassword(fd); err == nil {
			_, err = fmt.Println()
		}
	} else {
		bytes, err = ReadLine(os.Stdin)
	}
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

// ReadLine reads a line from the reader with trailing \r dropped.
func ReadLine(reader io.Reader) ([]byte, error) {
	var line []byte
	var buffer [1]byte
	for {
		n, err := reader.Read(buffer[:])
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if n == 0 {
			continue
		}
		c := buffer[0]
		if c == '\n' {
			break
		}
		line = append(line, c)
	}
	return bytes.TrimSuffix(line, []byte{'\r'}), nil
}
