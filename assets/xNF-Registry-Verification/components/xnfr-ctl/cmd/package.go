package cmd

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/pflag"

	"github.com/spf13/cobra"
)

var packageCmd = &cobra.Command{
	Use:   "package tar/untar [flags]",
	Short: "Parent command to tar and untar commands",
	Long:  `Parent command to work with local artifacts`,
	Args:  cobra.ExactArgs(1),
	RunE:  PackageCmdRun,
}

func ConfigurePackageCmd(cmd *cobra.Command) {
	// Common to tar and untar subcommands
	cmd.PersistentFlags().StringP("destination", "d", ".", "Path to where the results shall be shaved.")
}

func PackageCmdRun(cmd *cobra.Command, args []string) error {
	err := fmt.Errorf("You need to specify the action too: 'tar', 'untar'\n")
	return err
}

func ValidatePackageCmdInput(args []string, flags *pflag.FlagSet) (string, string, error) {
	// Check input info
	if len(args) != 1 {
		return "", "", errors.New("Only accepts 1 argument [PATH] ")
	}
	// Check that it exists
	_, err := os.Stat(args[0])
	if os.IsNotExist(err) {
		return "", "", errors.New("Path does not exist.")
	} else if err != nil {
		return "", "", err
	}

	// Check flags
	dPath, err := flags.GetString("destination")
	if err != nil {
		return "", "", errors.New("Unable to read destination flag")
	}
	// if PATH == PATH/ -> we need to remove / because it was added during bash autocompletion
	path := strings.TrimSuffix(args[0], "/")
	return path, dPath, nil
}
