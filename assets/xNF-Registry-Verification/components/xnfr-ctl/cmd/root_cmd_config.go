package cmd

import (
	"maps"
	"xnfrv/components/xnfrv-sdk/pkg/inspectors"
)

// Baseline configuration of the CMD. This is overwritten by projects extending the xnfrctl thanks to the methods in inspector_selector.go
var ConfigCmdInstance = &inspectors.ConfigCmd{
	AddCorrectInspectorFunction: inspectors.AddCorrectInspector,
	GetTemplateMapFunction:      AggregatedGetTemplateMaps,
}

func AggregatedGetTemplateMaps() map[string]string {
	templMap := inspectors.NewOSMInspector().GetTemplateMap()
	maps.Copy(templMap, inspectors.NewVReportInspector().GetTemplateMap())
	return templMap
}
