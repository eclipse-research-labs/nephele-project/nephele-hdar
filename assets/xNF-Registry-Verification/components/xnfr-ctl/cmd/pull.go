package cmd

import (
	"errors"
	"fmt"

	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"oras.land/oras-go/v2/content/file"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

var pullCmd = &cobra.Command{
	Use:   "pull [REGISTRY/REPOSITORY/NAME:TAG]",
	Short: "Pulls an OCI artifact from a remote registry",
	Long:  `Downloads the files associated to an OCI artifact and optionally un-zips the content.`,
	RunE:  PullCmdRun,
}

func ConfigurePullCmd(cmd *cobra.Command) {
	cmd.PersistentFlags().StringP("destination", "d", ".", "Base directory where the file will be downloaded")
	cmd.PersistentFlags().Bool("untar", false, "Indicates if downloaded artifact shall be kept as a GZ tarball (false) or extracted (true).")
	return
}

func PullCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	// Check input info
	if len(args) != 1 {
		err := fmt.Errorf("Only accepts 1 argument [REGISTRY/REPOSITORY/NAME:TAG]\n")
		return err
	}
	// 1. Prepare connection to remote registry
	// Check that args is correct (repo.Reference gives the info about reg, rep and image)
	repo, err := registry.NewRepoWithExistingDockerCredentials(args[0])
	if err != nil {
		err = fmt.Errorf("Unable to create repo\n")
		err = fmt.Errorf(err.Error())
		return err
	}

	// 2. Create a file store
	// Prepare destination
	fBase, err := cmd.Flags().GetString("destination")
	if err != nil {
		err = fmt.Errorf("Unable to read destination flag\n")
		err = fmt.Errorf(err.Error())
		return err
	}
	err = filesystem.CreateDirIfNotExist(fBase)
	if err != nil {
		err = fmt.Errorf("Unable to create destination dir\n")
		err = fmt.Errorf(err.Error())
		return err
	}
	fs, err := file.New(fBase)
	if err != nil {
		err = fmt.Errorf("Unable to create file store to pull artifact: %s\n", err.Error())
		return err
	}
	defer fs.Close()
	specs, err := registry.Pull(repo, fs)
	if err != nil {
		err = fmt.Errorf("Unable pull artifact: %s\n", err.Error())
		return err
	}
	untar, err := cmd.Flags().GetBool("untar")
	if err != nil {
		err = fmt.Errorf("Unable to read untar flag\n")
		err = fmt.Errorf(err.Error())
		return err
	} else if untar {
		err = fmt.Errorf("Proceeding to untar artifact content\n")
		// Single layer artifact support for now
		// TODO: we need a way to find the name of the inner directory name so that we can tell the client
		fName := specs.ImageManifest.Layers[0].Annotations["org.opencontainers.image.title"]
		if fName == "" {
			err = fmt.Errorf("File name not availbale in manifest.")
			// check if the file can be inferred by title and version (helm)
			fName = specs.ImageManifest.Annotations["org.opencontainers.image.title"] + "-" + specs.ImageManifest.Annotations["org.opencontainers.image.version"] + ".tgz"
			_, err := os.Stat(fName)
			if os.IsNotExist(err) {
				err = fmt.Errorf("Cannot untar")
				return err
			}
		}
		err = fmt.Errorf("File to untar %s\n", filepath.Join(fBase, fName))
		// Verify the new artifact
		a, err := artifact.NewLocalArtifact(filepath.Join(fBase, fName))
		if err != nil {
			return err
			// verification is automatically
		} else if found, err := ConfigCmdInstance.AddCorrectInspectorFunction(a, true); err != nil {
			err = fmt.Errorf("Error in Artifact: %s", err.Error())
			return err
		} else if !found {
			return errors.New("unable to find the right inspector")
		}

		err = archive.UnGzip(&a.InMemoryArchive, fBase)
		if err != nil {
			err = fmt.Errorf("Error unzipping the artifact: %s\n", err.Error())
			return err
		}
		err = fmt.Errorf("untarred artifact in destination directory\n")
		os.Remove(filepath.Join(fBase, fName)) // Only delete if correctly untar
	}
	fmt.Println("Pulled correctly")
	return nil

}
