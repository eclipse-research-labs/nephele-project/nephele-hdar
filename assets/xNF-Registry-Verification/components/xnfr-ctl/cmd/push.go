package cmd

import (
	"context"
	"errors"
	"fmt"

	"strings"

	"github.com/spf13/cobra"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

var pushCmd = &cobra.Command{
	Use:   "push [ARTIFACT] [REGISTRY/PROJECT]",
	Args:  cobra.ExactArgs(2),
	Short: "Pushes a local artifact to a remote registry",
	Long: `Takes a local artifact and creates an OCI artifact which is then pushed to a remote registry.
			The artifact name and version is obtained from the local artifact tar.gz file.`,
	RunE: PushCmdRun,
}

func ConfigurePushCmd(cmd *cobra.Command) {
	return
}

func PushCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	// Check input info
	if len(args) != 2 {
		err := fmt.Errorf("Only accepts 2 argument [ARTIFACT] [REGISTRY/PROJECT] \n")
		return err
	}
	fPath := args[0]
	rName := args[1]

	a, err := artifact.NewLocalArtifact(fPath)
	if err != nil {
		return err
		// verification is automatically
	} else if found, err := ConfigCmdInstance.AddCorrectInspectorFunction(a, true); err != nil {
		err = fmt.Errorf("Error in Artifact: %s", err.Error())
		return err
	} else if !found {
		return errors.New("unable to find the right inspector")
	}

	// We need to ensure that no uniqueName:version was added to the registry/repo arg[1]
	if strings.HasSuffix(rName, fmt.Sprintf("%s:%s", a.OCIPush.Name, a.OCIPush.Version)) {
		err = fmt.Errorf("Please do not add artifact info to the registry. It will be obtained from the descriptor")
		return err
	}
	repo, err := registry.NewRepoWithExistingDockerCredentials(fmt.Sprintf("%s/%s:%s", rName, a.OCIPush.Name, a.OCIPush.Version))
	if err != nil {
		err = fmt.Errorf("Invalid reference to the repository: %s", err.Error())
		return err
	}

	err = registry.Push(repo, a)
	if err != nil {
		err = fmt.Errorf("Unable to copy manifest to remote: %s", err.Error())
		return err
	}
	fmt.Println("Pushed correctly")
	ctx := context.WithValue(cmd.Context(), CmdFailed, false)
	cmd.SetContext(ctx)
	return nil
}
