package cmd

import (
	"context"
	"fmt"

	credentials "github.com/oras-project/oras-credentials-go"
	"github.com/spf13/cobra"
	"go-common/components/utils/logger"
)

var logoutCmd = &cobra.Command{
	Use:   "logout [REGISTRY]",
	Short: "Log out to a remote registry",
	Long:  `Log out to a remote registry. Access will be lost to all repositories.`,
	Args:  cobra.ExactArgs(1),
	RunE:  LogoutCmdRun,
}

func LogoutCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	ctx := context.Background()
	storeOpts := credentials.StoreOptions{}
	credStore, err := credentials.NewStoreFromDocker(storeOpts)
	if err != nil {
		err = fmt.Errorf("Unable to load docker cred store %s\n", err.Error())
		return err
	}
	if err = credentials.Logout(ctx, credStore, args[0]); err != nil {
		err = fmt.Errorf("Unable to correctly log out\n")
		return err
	}
	err = fmt.Errorf("Logout Succeeded\n")
	ctx = context.WithValue(cmd.Context(), CmdFailed, true)
	cmd.SetContext(ctx)
	return nil
}
