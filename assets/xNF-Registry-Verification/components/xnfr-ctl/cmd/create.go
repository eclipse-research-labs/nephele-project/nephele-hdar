package cmd

import (
	"fmt"
	"strings"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/template"

	"github.com/spf13/cobra"
)

var createCmd = &cobra.Command{
	Use:   "create [TYPE] [flags]",
	Short: "Create an artifact",
	Long:  `Create a baseline artifact for [TYPE=(VNF,NS)] based on template`,
	Args:  cobra.ExactArgs(1),
	RunE:  CreateCmdRun,
}

// Used for mocks. We just want to test the Cobra logic
var CMDGenerateLocalArtifact = template.GenerateLocalArtifact

func ConfigureCreateCmd(c *cobra.Command) {
	c.PersistentFlags().StringP("destination", "d", ".", "Path to the base folder that will contain the artifact")
}

func CreateCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	outputBasePath, _ := cmd.Flags().GetString("destination")
	aTypeFound := false
	for _, v := range ConfigCmdInstance.GetArtifactTypes() {
		if v == strings.ToUpper(args[0]) {
			aTypeFound = true
		}
	}
	if len(args) != 1 || !aTypeFound {
		err := fmt.Errorf("Expecting only the artifact type as argument. Choose %v\n", ConfigCmdInstance.GetArtifactTypes())
		return err
	}
	tContent, ok := ConfigCmdInstance.GetTemplateMapFunction()[strings.ToUpper(args[0])]
	if !ok {
		err := fmt.Errorf("Unable to obtain template content")
		return err
	}
	logger.Log.Debugf("Baseline path %s \n", outputBasePath)

	if !CMDGenerateLocalArtifact(outputBasePath, tContent, "") {
		return fmt.Errorf("unable to create local artifact")
	}
	fmt.Println("artifact created")
	return nil
}
