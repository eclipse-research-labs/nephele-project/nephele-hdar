package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var CmdFailed = "ok"

var RootCmd = &cobra.Command{
	Use:          "xnfrv",
	Short:        "Control tool for the xNF Registry and Verification",
	SilenceUsage: true,
	Long: `xNF Registry and Verification is a future-proof, full-fledged asset for Telco service storage, distribution and exposure
Smart Networks & Services R&D unit in Eviden (ATOS business).
Complete documentation is available at the repo`,
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		// Print is automatic
		os.Exit(1)
	}
}

func init() {
	// Hide internal command from Cobra
	RootCmd.CompletionOptions.DisableDefaultCmd = true
	ConfigurePullCmd(pullCmd)
	ConfigurePushCmd(pushCmd)
	ConfigureLoginCmd(loginCmd)
	ConfigureListCmd(listCmd)
	ConfigurePackageCmd(packageCmd)
	ConfigureCreateCmd(createCmd)
	ConfigureManifestCmd(manifestCmd)

	RootCmd.AddCommand(pullCmd)
	RootCmd.AddCommand(pushCmd)
	RootCmd.AddCommand(loginCmd)
	RootCmd.AddCommand(logoutCmd)
	RootCmd.AddCommand(listCmd)
	RootCmd.AddCommand(packageCmd)
	RootCmd.AddCommand(createCmd)
	RootCmd.AddCommand(lintCmd)
	RootCmd.AddCommand(manifestCmd)
}
