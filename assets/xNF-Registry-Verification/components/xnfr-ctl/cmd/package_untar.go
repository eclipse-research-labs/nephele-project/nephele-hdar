package cmd

import (
	"errors"
	"fmt"

	"go-common/components/utils/logger"

	"github.com/spf13/cobra"

	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
)

var unTarCmd = &cobra.Command{
	Use:   "untar [PATH]",
	Short: "Extracts content from tar.gz file.",
	Long:  `Extracts the file or directory from a tar.gz file.`,
	Args:  cobra.ExactArgs(1),
	RunE:  UnTarCmdRun,
}

func init() {
	packageCmd.AddCommand(unTarCmd)
}

func UnTarCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	path, dPath, err := ValidatePackageCmdInput(args, cmd.Flags())
	if err != nil {
		return err
	}
	// Verify the new artifact
	a, err := artifact.NewLocalArtifact(path)
	if err != nil {
		return err
		// verification is automatically
	} else if found, err := ConfigCmdInstance.AddCorrectInspectorFunction(a, true); err != nil {
		err = fmt.Errorf("Error in Artifact: %s", err.Error())
		return err
	} else if !found {
		return errors.New("unable to find the right inspector")
	}
	err = archive.UnGzip(&a.InMemoryArchive, dPath)
	if err != nil {
		err = fmt.Errorf("Unable to extract contents\n")
		return err
	}
	fmt.Println("Untarred correctly")
	return nil
}
