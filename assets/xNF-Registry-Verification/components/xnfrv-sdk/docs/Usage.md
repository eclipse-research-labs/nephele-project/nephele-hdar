[[_TOC_]]

# Using CLI

For this first demo only standalone CLIs are used. We are combining Helm, docker and ORAS to create an OCI registry that
hosts artifacts for all of those technologies.


## Prerequisites


1. Configure CLIs: Only Helm needs additional config ``export HELM_EXPERIMENTAL_OCI=1``

2. Install ORAS



### With local docker registry

1. Install the CNCF distribution ["registry"](https://docs.docker.com/registry/deploying/)

    ```bash
    # Local Docker Registry
    docker run -d \
    -p 5000:5000 \
    --restart=always \
    --name registry \
    -v ./registry_vol:/var/lib/registry \
    registry:2
    ```

### With Harbor in K8s

ToDo
## Demo

1. Upload a docker image

```bash
docker pull busybox:latest
docker tag busybox:latest localhost:5000/docker/busybox:latest
docker push localhost:5000/docker/busybox:latest
```

2. Upload a Helm chart

```bash
helm create nginx
helm package nginx
helm push nginx-0.1.0.tgz oci://localhost:5000/charts
```

3. Upload a VNF

```bash
oras push localhost:5000/nfv/openldap:1.0.0 --config docs/nfv-specification-config-example.json:application/vnd.oci.etsi.nfv.config.v1+json resources/osm-packages-master-openldap_knf.tar.gz:application/vnd.oci.etsi.nfv.content.tar+gzip
```

## Check what is inside the registry

```bash
oras repo ls localhost:5000
>    docker/busybox
>    charts/nginx
>    nfv/openldap
```


# Pull artifacts

```bash
oras pull localhost:5000/nfv/openldap:1.0.0 # Download the layers
# artifact is saved as "resources/osm-packages-master-openldap_knf.tar.gz"
oras manifest fetch localhost:5000/nfv/openldap:1.0.0 # Download the manifest
oras manifest fetch-config localhost:5000/nfv/openldap:1.0.0 # Download the config
```

