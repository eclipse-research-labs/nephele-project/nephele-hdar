# Debian due to Helm
FROM registry.atosresearch.eu:18488/golang:latest-debian

# Install Helm CLI for the redis worker to run tests
# https://helm.sh/docs/intro/install/#from-apt-debianubuntu
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
RUN chmod 700 get_helm.sh
RUN ./get_helm.sh

# Install Trivy to run scans
RUN curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.48.3

# Install Cosign key files loaded via secret and env vars
RUN curl -O -L "https://github.com/sigstore/cosign/releases/latest/download/cosign-linux-amd64"
RUN mv cosign-linux-amd64 /usr/local/bin/cosign
RUN chmod +x /usr/local/bin/cosign

RUN apt-get update \
    && apt-get install git curl -y --no-install-recommends 

# Pushed as registry.atosresearch.eu:18467/xnfv_base:develop