# Docker only used for the cicd pipeline
FROM registry.atosresearch.eu:18488/golang:latest-alpine as builder

# Use to skip tests and binary builds to be able to use tools/dev.docker-compose.yml 
ARG DEV

WORKDIR /go/src/components/xnfrv-sdk 

# Only copy require code and delete as much as possible (second stage)
COPY assets/go-common/components/utils ../../assets/go-common/components/utils
COPY assets/go-common/tools/dockerfile_tools/ .
COPY components/xnfrv-sdk .

# Neeed for tests
RUN apk --no-cache add build-base

# Make sure that the code is up to standards and docs are updated
# Run tests and keep report
RUN if [ -z "$DEV"]; then \
    ./go_tools.sh \
    && ./go_test.sh; \
    fi

# No binary generated

# Reduce final image tag as much as possible
FROM registry.atosresearch.eu:18520/library/alpine

WORKDIR /root/

COPY --from=builder /go/src/components/xnfrv-sdk/testReport.txt testReport.txt