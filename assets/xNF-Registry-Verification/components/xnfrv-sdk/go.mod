module xnfrv/components/xnfrv-sdk

go 1.22.7

replace go-common/components/utils => ../../assets/go-common/components/utils

require (
	github.com/opencontainers/image-spec v1.1.0
	github.com/oras-project/oras-credentials-go v0.4.0
	github.com/stretchr/testify v1.9.0
	github.com/xeipuuv/gojsonschema v1.2.0
	gitlab.com/metakeule/scaffold v1.7.2
	go-common/components/utils v0.0.0-00010101000000-000000000000
	gopkg.in/yaml.v2 v2.4.0
	oras.land/oras-go/v2 v2.5.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20180127040702-4e3ac2762d5f // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	golang.org/x/sync v0.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
