package utils_test

import (
	"go-common/components/utils/logger"
	"testing"
	"xnfrv/components/xnfrv-sdk/pkg/utils"
)

func TestExtension(t *testing.T) {
	// perform actual check
	filePath := "example.tar.gz"
	logger.Log.Info(utils.GetExtension(filePath))
	filePath = "example.tgz"
	logger.Log.Info(utils.GetExtension(filePath))
}
