package utils

import (
	"go-common/components/utils/logger"

	"github.com/xeipuuv/gojsonschema"
)

func VerifyAgainstSchema(schema, document string) ([]string, error) {
	// Verifies syntax the document with the given schema and
	// returns a list of the schema errors found, and potential internal errors
	var schemaErrors []string
	schemaLoader := gojsonschema.NewStringLoader(schema)
	documentLoader := gojsonschema.NewStringLoader(document)

	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return schemaErrors, err
	}

	if result.Valid() {
		logger.Log.Infof("The document is valid\n")
		return schemaErrors, nil
	}
	logger.Log.Warning("The document is not valid. see errors :\n")
	for _, desc := range result.Errors() {
		logger.Log.Warningf("- %s\n", desc.String())
		schemaErrors = append(schemaErrors, desc.String())
	}
	return schemaErrors, nil
}
