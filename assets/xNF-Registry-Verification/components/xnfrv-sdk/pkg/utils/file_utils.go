package utils

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
)

// Returns a map string with 3 keys, name, version, extension (has to be .tgz or .tar.gz)
func GetFileFormat(artifactPath string) (map[string]string, error) {
	fileFormat := make(map[string]string, 2)
	filesystem.DeleteDirIfEmpty("ss")
	// Ensure file format -> id-version.extension
	// Split on "-"
	_, fName := filepath.Split(artifactPath)
	if strings.HasSuffix(filepath.Base(fName), ".tgz") {
		fileFormat["extension"] = ".tgz"
	} else if strings.HasSuffix(filepath.Base(fName), ".tar.gz") {
		fileFormat["extension"] = ".tar.gz"
	} else {
		return fileFormat, fmt.Errorf("file '%s' not aligned with expected format {uniqueName}-{version}.{extension}", fName)
	}
	parts := strings.Split(strings.TrimSuffix(filepath.Base(fName), fileFormat["extension"]), "-")
	// TODO: This could fail if there is metadata in version according to SemVer
	if len(parts) > 2 {
		// Verify that we aren't confusing a version for a uuid
		fileFormat["name"] = strings.Join(parts[:len(parts)-1], "-")
		fileFormat["version"] = parts[len(parts)-1]
	} else if len(parts) == 2 {
		fileFormat["name"] = parts[0]
		fileFormat["version"] = parts[1]
	} else {
		return fileFormat, fmt.Errorf("file '%s' not aligned with expected format {uniqueName}-{version}.{extension}", fName)
	}
	return fileFormat, nil
}

func EnsureVersionFormat(version interface{}) (string, error) {
	var v string
	switch version.(type) {
	case float64:
		v = fmt.Sprintf("%.1f", version.(float64))
	case int:
		v = fmt.Sprintf("%d", version.(int))
	case string:
		v = fmt.Sprintf("%s", version.(string))
	default:
		return "", errors.New("Unknown version type in descriptor")
	}
	return v, nil
}

// get extension of file
func GetExtension(path string) string {
	if strings.HasSuffix(path, ".tgz") {
		return ".tgz"
	} else if strings.HasSuffix(path, ".tar.gz") {
		return ".tar.gz"
	} else {
		logger.Log.Warning("Not a compressed file")
		return ""
	}
}
