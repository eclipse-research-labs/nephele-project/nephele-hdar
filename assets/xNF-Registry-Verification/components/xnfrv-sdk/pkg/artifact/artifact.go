package artifact

import (
	"errors"
	"fmt"
	"path/filepath"
	"reflect"
	"unicode"
	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/interfaces"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/utils"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
)

// Provides the entry point to handling an artifact
type Artifact struct {
	// Custom implementation for each artifact. Not directly interacted with
	inspector interfaces.ArtifactInspector
	// Loaded content of the artifact
	InMemoryArchive archive.Archive
	// Info for pushing to an OCI Registry
	OCIPush models.OCIConfig
	// Info pulled from an OCI Registry
	OCIPull models.OCISpec
	// Keep the verification errors for log purposes
	Errors []string
}

func NewEmptyArtifact() *Artifact {
	return &Artifact{}
}

// Creates a baseline Artifact
func NewLocalArtifact(fPath string) (*Artifact, error) {
	// Load base tar.gz in memory
	rawContents, err := archive.NewBaselineArchive(fPath)
	if err != nil {
		logger.Log.Info("Unable to load file in memory")
		return nil, err
	}
	a := NewEmptyArtifact()
	a.InMemoryArchive = *rawContents
	return a, nil
}

func NewTmpArtifactFromFolder(fPath string) (a *Artifact, err error) {
	// Has to be in two steps because the injection of the inspector cannot be here (import cycle)
	// We need to infer the unique name and version to create the proper file name
	// This is a tmp file for the rest of the code to work
	aName := "tmp.tar.gz"
	err = filesystem.WriteGZFile(aName, fPath, ".")
	if err != nil {
		logger.Log.Infof("Unable to write to GZ temp file: %s", err.Error())
		return nil, err
	}
	// Double check that the artifact can be loaded
	a, err = NewLocalArtifact(aName)
	if err != nil {
		logger.Log.Infof(err.Error())
		return nil, err
	}
	return a, nil
}

func TmpArtifactToLocalArtifact(a *Artifact, dPath string) (string, error) {
	// Has to be in two steps because the injection of the inspector cannot be here (import cycle)
	// Tmp artifacts has to have the right inspector already injected
	// This is generic for any implementation
	if a.inspector == nil {
		err := fmt.Errorf("load the inspector first")
		logger.Log.Warning(err.Error())
		return "", err
	}
	// Rename in new location and delete tmp file
	oPath := filepath.Join(a.InMemoryArchive.FileRaw.Dir, a.InMemoryArchive.FileRaw.Name)
	aName := filepath.Join(dPath, fmt.Sprintf("%s-%s.tar.gz", a.OCIPush.Name, a.OCIPush.Version))
	err := filesystem.RenameFile(oPath, aName, true)
	if err != nil {
		logger.Log.Errorf("Unable to write to final GZ file: %s", err.Error())
		return "", err
	}
	return aName, nil
}

// This method is called in inspector selector
func (a *Artifact) LoadWithInspector(inspector *interfaces.ArtifactInspector) error {
	// We assume that with an error, it is not the right inspector for the artifact
	a.inspector = *inspector
	// Clean artifact
	if isRightInspector := a.inspector.IsRightInspector(&a.InMemoryArchive); !isRightInspector {
		a.inspector = nil
		return fmt.Errorf("%s not the right inspector", reflect.TypeOf(inspector).Elem().Name())
	}
	oConfig, err := a.GetOCIConfig()
	if err != nil {
		logger.Log.Debug(err.Error())
		return err
	}
	a.OCIPush = *oConfig
	if a.OCIPush.Name == "" || a.OCIPush.Version == "" || a.OCIPush.ConfigMediaType == "" || len(a.OCIPush.ConfigContent) == 0 {
		logger.Log.Debug("Artifact did not produce an OCI image spec correctly")
		return fmt.Errorf("Artifact did not produce an OCI image spec correctly")
	}
	return nil
}

func (a *Artifact) Lint() error {
	// Global method to perform static verifications
	// Todo: Verify that dependencies exist -> Dynamic check in an independent implementation. Not static
	if a.inspector == nil {
		msg := "Should not have called lint if inspector is not selected"
		logger.Log.Warning(msg)
		return errors.New(msg)
	}
	err := a.verifyStructure()
	if err != nil {
		a.Errors = append(a.Errors, err.Error())
		logger.Log.Infof("Artifact structure of files incorrectly formatted: %s\n", err.Error())
		return err
	}
	err = a.verifySyntax()
	if err != nil {
		a.Errors = append(a.Errors, err.Error())
		logger.Log.Infof("Artifact syntax of descriptor incorrectly formatted: %s\n", err.Error())
		return err
	}
	err = a.verifyFileFormat()
	if err != nil {
		a.Errors = append(a.Errors, err.Error())
		logger.Log.Infof("Artifact name incorrectly formatted: %s\n", err.Error())
		return err
	}
	return nil
}

func (a *Artifact) verifyFileFormat() error {
	// Verifies naming convention id-version.ext (tar.gz or tgz) against contents in the descriptor
	// Same for all implementations
	fileNameFormat, err := utils.GetFileFormat(a.InMemoryArchive.FileRaw.Name)
	if err != nil {
		return err
	}
	if utils.GetExtension(a.InMemoryArchive.FileRaw.Name) != ".tar.gz" && utils.GetExtension(a.InMemoryArchive.FileRaw.Name) != ".tgz" {
		return fmt.Errorf("extension of artifact not supported %s", err.Error())
	}
	descNameFormat := a.inspector.GetNameFormat(&a.InMemoryArchive)

	if err != nil {
		return fmt.Errorf("error obtaining Name from descriptor %s", err.Error())
	}
	if descNameFormat["name"] != fileNameFormat["name"] || descNameFormat["version"] != fileNameFormat["version"] {
		return fmt.Errorf("file %s-%s and descriptor content %s-%s do not match", fileNameFormat["name"], fileNameFormat["version"], descNameFormat["name"], descNameFormat["version"])
	}
	// https://github.com/wujunwei/helm/blob/f0fd37d2c50f947ce056e14372a3952e83251b04/pkg/chartutil/verify_name.go#L27-L37
	for _, char := range descNameFormat["name"] {
		if unicode.IsUpper(char) {
			return fmt.Errorf("unique Names cannot contain capitalized letters (CN convention): %s --> %c", descNameFormat["name"], char)
		}
	}
	return nil
}

func (a *Artifact) verifyStructure() error {
	// This check only looks at the pattern of files, not content
	return a.inspector.VerifyStructure(&a.InMemoryArchive)
}

func (a *Artifact) verifySyntax() error {
	// This check validates all potential descriptors inside
	return a.inspector.VerifySyntax(&a.InMemoryArchive)
}

func (a *Artifact) GetOCIConfig() (*models.OCIConfig, error) {
	// Provides the required information to interact with the OCI Registry
	return a.inspector.GetOCIConfig(&a.InMemoryArchive)
}
