package inspectors

import (
	"embed"
	"encoding/json"
	"fmt"
	"path/filepath"
	"strings"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/utils"
)

//go:embed docs/osm*
var nfvFiles embed.FS

type OSMInspector struct{}

var MediaTypeNFVConfig = "application/vnd.etsi.nfv.config.v1+json"

/*
This is fully customizable and should allow us to implement adaptations for OSM, TOSCA and others.
Each case should obtain the info from the corresponding descriptor.
*/
type NFVConfig struct {
	// SchemaVersion to xNFRV version: 1 = [v0.3.0, ]. Mandatory in all implementations
	SchemaVersion string `json:"schemaVersion"`
	// Config MediaType. For all ETSI NFV CSARs. Mandatory in all implementations
	ConfigMediaType string `json:"configMediaType"`
	// NFV Level described in the descriptor. Mandatory in all implementations and needs to match GetTemplateMap keys
	ArtifactType string `json:"artifactType"`
	// Reference to the specification of the CSAR
	PackageSpec string `json:"packageSpec"`
	// Reference to the specification of the content's structure
	DataModelSpec string `json:"dataModelSpec"`
	// Path to the core descriptor file
	Descriptor Descriptor `json:"descriptor"`
	// Dependencies of the descriptor
	Dependencies []Dependency `json:"dependencies"`
}

type Dependency struct {
	// NFV Level described in the descriptor. Mandatory in all implementations and needs to match GetTemplateMap keys
	ArtifactType string `json:"artifactType"`
	// Unique identifier
	Ref string `json:"ref"`
	// Location [local, implicit, external]
	Location string `json:"location"`
	// Mode [soft, hard]
	Mode string `json:"mode"`
}

type Descriptor struct {
	// Optional Path in the artifact after untar
	Path string `json:"name"`
	// Id in the descriptor
	Id string `json:"id"`
	// Version in the descriptor
	Version string `json:"version"`
}

func NewOSMInspector() *OSMInspector {
	return &OSMInspector{}
}

// Infers if the archive corresponds to the inspector
func (ai *OSMInspector) IsRightInspector(arc *archive.Archive) bool {
	// Only one yaml file in base path
	descriptor := ""
	for _, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			if descriptor != "" {
				logger.Log.Debug("Too many yaml files to be an OSM artifact")
				return false
			}
			// yaml file has to contain one of the known YANG top container labels
			content := string(f.Data)
			if strings.Contains(content, "vnfd:") || strings.Contains(content, "vnfd-catalog:") || strings.Contains(content, "nsd:") || strings.Contains(content, "nsd-catalog:") {
				descriptor = f.Name
				logger.Log.Debugf("%s descriptor seems to contain OSM content", descriptor)
			}
		}
	}
	return descriptor != ""
}

// Everything after this is executed with something we are sure is an OSM artifact

// Returns the name convention based on the descriptor. "name", "version"
func (ai *OSMInspector) GetNameFormat(arc *archive.Archive) map[string]string {
	nFormat := map[string]string{"version": "", "name": ""}
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	content, _ := ai.loadDescriptorContent(arc.ContentRaw[index].Data)
	if val, ok := content["id"]; ok && val.(string) != "" {
		nFormat["name"] = val.(string)
	}

	if val, ok := content["version"]; ok {
		v, err := utils.EnsureVersionFormat(val)
		if err != nil {
			logger.Log.Errorf("Invalid version in descriptor: %s", err.Error())
			return nFormat
		}
		nFormat["version"] = v
	}
	return nFormat
}

// Ensure all expected files are present (just the file format and locations)
func (ai *OSMInspector) VerifyStructure(arc *archive.Archive) error {
	// IsRightInspector is enough check
	return nil
}

// Ensure all expected files are sintactically correct
func (ai *OSMInspector) VerifySyntax(arc *archive.Archive) error {
	// It is a single descriptor to verify
	// Need the content in json format
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	dMap, err := filesystem.LoadYamlContent(arc.ContentRaw[index].Data)
	if err != nil {
		return err
	}
	descriptorJson, err := json.Marshal(dMap)
	if err != nil {
		return fmt.Errorf("unable to convert descriptor to json: %s", err.Error())
	}
	dContent := string(descriptorJson)
	sContent := ""
	if strings.Contains(dContent, "nsd:") || strings.Contains(dContent, "nsd-catalog:") {
		sContent = ai.GetSchemaMap()["NS"]
	} else {
		sContent = ai.GetSchemaMap()["VNF"]
	}

	// Proceed with the verification
	sErrors, iError := utils.VerifyAgainstSchema(sContent, dContent)
	logger.Log.Debugf("sErrors %+v\n", sErrors)
	if iError != nil {
		return fmt.Errorf("internal Error: %s", iError.Error())
	} else if len(sErrors) > 0 {
		return fmt.Errorf("%d errors found:\n%s", len(sErrors), strings.Join(sErrors, "\n"))
	} else {
		logger.Log.Infof("No errors detected")
	}
	return nil
}

func (ai *OSMInspector) GetSchemaMap() map[string]string {
	// For OSM descriptors the test is not representative, we would need to
	// use the actual osm CLI since we do not want to host the complete IM schema (in yang...)
	knfData, _ := nfvFiles.ReadFile("docs/osm_knf_schema.json")
	nsData, _ := nfvFiles.ReadFile("docs/osm_ns_schema.json")
	return map[string]string{
		"VNF": string(knfData),
		"NS":  string(nsData),
	}
}

func (ai *OSMInspector) GetTemplateMap() map[string]string {
	// Provides a baseline archive (folder format) of the artifact
	knfData, _ := nfvFiles.ReadFile("docs/osm_knf.templ")
	nsData, _ := nfvFiles.ReadFile("docs/osm_ns.templ")
	return map[string]string{
		"VNF": string(knfData),
		"NS":  string(nsData),
	}
}

func (ai *OSMInspector) GetOCIConfig(arc *archive.Archive) (*models.OCIConfig, error) {
	// Prepare layers for OCI
	arc.FileRaw.MediaLayer = models.DefaultMediaTypeLayer
	oci := &models.OCIConfig{}

	// Prepare annotations for manifest
	annotations := make(map[string]string)
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	content, err := ai.loadDescriptorContent(arc.ContentRaw[index].Data)
	if err != nil {
		return oci, err
	}
	if val, ok := content["description"]; ok {
		annotations["org.opencontainers.image.description"] = val.(string)
	}
	if val, ok := content["designer"]; ok { // NS
		annotations["org.opencontainers.image.authors"] = val.(string)
	} else if val, ok := content["provider"]; ok { // VNF
		annotations["org.opencontainers.image.authors"] = val.(string)
	}
	// Cannot be added to Manifest annotations as it breaks the fileStore
	nFormat := ai.GetNameFormat(arc)
	annotations["org.opencontainers.image.title"] = nFormat["name"]
	annotations["org.opencontainers.image.version"] = nFormat["version"]

	oci.Annotations = annotations
	oci.Name = nFormat["name"]
	oci.Version = nFormat["version"]
	// Load config content
	oci.ConfigMediaType = MediaTypeNFVConfig
	dType := "VNF"
	if strings.Contains(string(arc.ContentRaw[index].Data), "nsd:") || strings.Contains(string(arc.ContentRaw[index].Data), "nsd-catalog:") {
		dType = "NS"
	}
	config := &NFVConfig{
		SchemaVersion:   "2",
		PackageSpec:     "SOL004",
		DataModelSpec:   "SOL006",
		ConfigMediaType: MediaTypeNFVConfig,
		Descriptor: Descriptor{
			Path:    filepath.Join(arc.ContentRaw[index].Dir, arc.ContentRaw[index].Name),
			Id:      annotations["org.opencontainers.image.title"],
			Version: annotations["org.opencontainers.image.version"],
		},
		ArtifactType: dType,
		Dependencies: ai.GetDependencies(arc),
	}
	oci.ConfigContent, _ = json.MarshalIndent(config, "", "\t")

	return oci, nil
}

func (ai *OSMInspector) GetDependencies(arc *archive.Archive) []Dependency {
	logger.Log.Debug("Getting Dependencies")
	var dependencies []Dependency
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	// to know the type (VNF/NSD) wrapper
	wContent, err := filesystem.LoadYamlContent(arc.ContentRaw[index].Data)
	if err != nil {
		logger.Log.Errorf("Unable to load descriptor to find dependencies: %s", err.Error())
		return dependencies
	}
	logger.Log.Debugf("wContent: %+v", wContent)
	// To remove the wrapper in the descriptor
	content, err := ai.loadDescriptorContent(arc.ContentRaw[index].Data)
	if err != nil {
		logger.Log.Errorf("Unable to load descriptor to find dependencies: %s", err.Error())
		return dependencies
	}
	logger.Log.Debugf("content: %+v", content)
	// Descriptor is a NSD
	if swContent, ok := wContent["nsd"]; ok {
		logger.Log.Debug("Is a NSD")
		// check for implicit dependencies
		if temp_val, ok := swContent.(filesystem.MapStr)["vnfd"]; ok {
			// There are implicit vnfds in the same descriptor
			logger.Log.Info("Implicit VNFD in NSD\n")
			for _, vnfd := range temp_val.([]filesystem.MapStr) {
				// dVersion, _ := vnfd["version"]
				dependencies = append(dependencies, Dependency{
					ArtifactType: "VNF",
					Ref:          vnfd["id"].(string),
					// DescriptorVersion: dVersion.(string),
					Location: "implicit",
					Mode:     "hard",
				})
			}
		} else {
			// There are external VNFs required for this
			logger.Log.Info("External VNFD in NSD\n")
			for _, vnfdId := range content["vnfd-id"].([]interface{}) {
				dependencies = append(dependencies, Dependency{
					ArtifactType: "VNF",
					Ref:          vnfdId.(string),
					// DescriptorVersion: "",
					Location: "external",
					Mode:     "hard",
				})
			}

		}
		// Descriptor is a VNFD
	} else if _, ok := wContent["vnfd"]; ok {
		logger.Log.Debug("Is a VNFD")
		if val, ok := content["sw-image-desc"]; ok {
			for _, dependency := range val.([]interface{}) {
				dependencies = append(dependencies, Dependency{
					ArtifactType: "VM",
					Ref:          dependency.(filesystem.MapStr)["image"].(string),
					// DescriptorVersion: "",
					Location: "external",
					Mode:     "soft",
				})
			}
		} else if val, ok := content["kdu"]; ok {
			for _, dependency := range val.([]interface{}) {
				if _, ok := dependency.(filesystem.MapStr)["juju-bundle"]; ok {
					dependencies = append(dependencies, Dependency{
						ArtifactType: "JUJU",
						Ref:          dependency.(filesystem.MapStr)["juju-bundle"].(string),
						// DescriptorVersion: "",
						Location: "external",
						Mode:     "soft",
					})
				} else if _, ok := dependency.(filesystem.MapStr)["helm-chart"]; ok {
					// Check if it is local or external
					cName := dependency.(filesystem.MapStr)["helm-chart"].(string)
					location := "external"
					mode := "soft"
					if !strings.Contains(cName, "/") {
						location = "internal"
						mode = "hard"
					}
					dependencies = append(dependencies, Dependency{
						ArtifactType: "HELM",
						Ref:          dependency.(filesystem.MapStr)["helm-chart"].(string),
						// DescriptorVersion: "",
						Location: location,
						Mode:     mode,
					})
				} else {
					logger.Log.Errorf("Kdu dependency not known %+v\n", dependency.(filesystem.MapStr))
				}
			}
		} else {
			logger.Log.Errorf("Descriptor VNFD subtype not known %+v\n", content)
		}
	} else {
		logger.Log.Errorf("Descriptor type not known %+v\n", content)
	}
	logger.Log.Debug("Done getting dependencies")
	return dependencies
}

func (ai *OSMInspector) loadDescriptorContent(raw []byte) (filesystem.MapStr, error) {
	mapPointer, err := filesystem.LoadYamlContent(raw)
	if err != nil {
		logger.Log.Errorf("Error reading descriptor: %s", err.Error())
		return nil, err
	}
	var content filesystem.MapStr
	if val, ok := mapPointer["nsd"]; ok {
		if temp_val, ok := val.(filesystem.MapStr)["nsd"]; ok {
			logger.Log.Info("Nested nsd entry in IM\n")
			content = temp_val.([]interface{})[0].(filesystem.MapStr)
			logger.Log.Infof("%+v\n", content)
		} else {
			content = val.(filesystem.MapStr)
		}
	} else if val, ok := mapPointer["vnfd"]; ok {
		if temp_val, ok := val.(filesystem.MapStr)["vnfd"]; ok {
			logger.Log.Info("Nested vnfd entry in IM\n")
			content = temp_val.(filesystem.MapStr)
		} else {
			content = val.(filesystem.MapStr)
		}
	} else {
		logger.Log.Error("Unable to process descriptor")
		return nil, fmt.Errorf("unable to process descriptor")
	}
	return content, nil
}
