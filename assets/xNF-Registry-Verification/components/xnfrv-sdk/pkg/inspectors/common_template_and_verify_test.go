// Keep as a different package to enforce testing as a consumer of the lib
package inspectors_test

import (
	"os"
	"testing"

	"go-common/components/utils/filesystem"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
	"xnfrv/components/xnfrv-sdk/pkg/template"

	// Installed as a consumer
	"xnfrv/components/xnfrv-sdk/pkg/inspectors"
)

func TestVerifyTemplate(t *testing.T) {
	// Make sure no local artifact is left
	// Table-Driven testing
	data := []struct {
		input         string
		errorMsg      string
		created       bool
		expectedAName string
		expectedDName string
		expectedDType string
	}{
		{
			input:         inspectors.NewOSMInspector().GetTemplateMap()["NS"],
			errorMsg:      "should have located template file correctly",
			created:       true,
			expectedAName: "unique-id-ns-0.1.0.tar.gz",
			expectedDName: "unique-id-ns",
			expectedDType: "NS",
		},
		{
			input:         inspectors.NewOSMInspector().GetTemplateMap()["VNF"],
			errorMsg:      "should have located template file correctly",
			created:       true,
			expectedAName: "unique-id-knf-0.1.0.tar.gz",
			expectedDName: "unique-id-knf",
			expectedDType: "NS",
		},
		{
			input:         inspectors.NewVReportInspector().GetTemplateMap()["VREPORT"],
			errorMsg:      "should have located template file correctly",
			created:       true,
			expectedAName: "report-unique-id-knf-0.1.0.tar.gz",
			expectedDName: "report-unique-id-knf",
			expectedDType: "VREPORT",
		},
	}
	for _, val := range data {
		aName := val.expectedAName
		dName := val.expectedDName
		defer os.RemoveAll(aName)
		defer os.RemoveAll(dName)
		created := template.GenerateLocalArtifact(".", val.input, "")
		if created != val.created {
			t.Errorf("%s test -> %t vs %t: %s\n", val.expectedAName, created, val.created, val.errorMsg)
			return
		}
		if created {
			_, err := os.Stat(dName)
			if err != nil {
				t.Errorf("Should not have created the base dir for artifact %s", err.Error())
				return
			}
			// create the tar.gz to pass later the verification

			err = filesystem.WriteGZFile(aName, dName, ".")
			if err != nil {
				t.Errorf("Unable to write to GZ temp file: %s", err.Error())
				return
			}
			// Pass verification
			a, err := artifact.NewLocalArtifact(aName)
			if err != nil {
				t.Error(err.Error())
			}
			found, err := inspectors.AddCorrectInspector(a, true)
			if !found {
				t.Error("Unable to load descriptor with inspector provided.")
			} else if err != nil {
				t.Errorf("Error found in artifact: %s\n", err.Error())
			}
		}
	}
}
