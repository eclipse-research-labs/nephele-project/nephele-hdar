package inspectors

import (
	"reflect"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
	"xnfrv/components/xnfrv-sdk/pkg/interfaces"
)

// try-n-error to find the artifact's required Inspector
// This is extended in other projects to reuse this baseline artifacts
func AddCorrectInspector(a *artifact.Artifact, verify bool) (found bool, err error) {
	inspectorList := []interfaces.ArtifactInspector{NewOSMInspector(), NewVReportInspector()}
	for _, inspector := range inspectorList {
		if err := a.LoadWithInspector(&inspector); err == nil {
			logger.Log.Infof("Found that the artifact is type %s\n", reflect.TypeOf(inspector).Elem().Name())
			oConfig, err := a.GetOCIConfig()
			logger.Log.Debug("after oci config")
			if err != nil {
				logger.Log.Debug(err.Error())
				return true, err
			}
			a.OCIPush = *oConfig
			if a.OCIPush.Name == "" || a.OCIPush.Version == "" || a.OCIPush.ConfigMediaType == "" || len(a.OCIPush.ConfigContent) == 0 {
				logger.Log.Debug("Artifact did not produce an OCI image spec correctly")
				return true, err
			}
			if verify {
				if err := a.Lint(); err != nil {
					logger.Log.Debug(err.Error())
					return true, err
				}
				logger.Log.Debug("No errors on verification")
			}
			return true, nil
		} else {
			logger.Log.Debug(err.Error())
		}
	}
	logger.Log.Debug("No inspector found")
	return false, err
}

// Wrapper to extend the CLI in other project which needs to have access to the right AddCorrectInspectorFunction and GetTemplateMapFunction
// Configured at the cmd/root_cmd_config.go level at the xnfrctl component
type ConfigCmd struct {
	AddCorrectInspectorFunction func(a *artifact.Artifact, validateFileName bool) (found bool, err error)
	GetTemplateMapFunction      func() map[string]string
}

func (t *ConfigCmd) GetArtifactTypes() []string {
	// Used in help of create command. Does not need re-implementing in new projects
	aTypes := make([]string, 0)
	for v := range t.GetTemplateMapFunction() {
		aTypes = append(aTypes, v)
	}
	return aTypes
}
