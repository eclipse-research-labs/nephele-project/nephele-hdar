package inspectors

import (
	"embed"
	"encoding/json"
	"path/filepath"
	"strings"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"

	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/utils"
)

//go:embed docs/vreport*
var vreportFiles embed.FS

type VReportInspector struct{}

/*
This is fully customizable
*/
type VReportConfig struct {
	// SchemaVersion to xNFRV version: 1 = [v0.3.0, ]. Mandatory in all implementations
	SchemaVersion string `json:"schemaVersion"`
	// Config MediaType = MediaTypeVReportConfig.
	ConfigMediaType string `json:"configMediaType"`
	// Path to the core descriptor file
	Descriptor Descriptor `json:"descriptor"`
}

func NewVReportInspector() *VReportInspector {
	return &VReportInspector{}
}

// Infers if the archive corresponds to the inspector
func (ai *VReportInspector) IsRightInspector(arc *archive.Archive) bool {
	// Only one yaml file in base path
	descriptor := ""
	for _, f := range arc.ContentRaw {
		if f.Name == "report.yaml" {
			if descriptor != "" {
				logger.Log.Debug("Too many yaml files to be an VReport artifact")
				return false
			}
			// yaml file has to contain one of the known YANG top container labels
			content := string(f.Data)
			if strings.Contains(content, "artifactFullRepo:") {
				descriptor = f.Name
				logger.Log.Debugf("%s descriptor seems to contain VReport content", descriptor)
			}
		}
	}
	return descriptor != ""
}

// Everything after this is executed with something we are sure is an VReport artifact

// Returns the name convention based on the descriptor. "name", "version"
func (ai *VReportInspector) GetNameFormat(arc *archive.Archive) map[string]string {
	nFormat := map[string]string{"version": "", "name": ""}
	index := 0
	for i, f := range arc.ContentRaw {
		if f.Name == "report.yaml" {
			index = i
			break
		}
	}
	content, err := filesystem.LoadYamlContent(arc.ContentRaw[index].Data)
	if err != nil {
		logger.Log.Debugf("Unable to load document: %s", err.Error())
		return nFormat
	}
	if val, ok := content["id"]; ok && val.(string) != "" {
		nFormat["name"] = val.(string)
	}

	if val, ok := content["artifact"].(filesystem.MapStr)["artifactVersion"]; ok {
		v, err := utils.EnsureVersionFormat(val)
		if err != nil {
			logger.Log.Errorf("Invalid version in descriptor: %s", err.Error())
			return nFormat
		}
		nFormat["version"] = v
	}
	return nFormat
}

// Ensure all expected files are present (just the file format and locations)
func (ai *VReportInspector) VerifyStructure(arc *archive.Archive) error {
	// IsRightInspector is enough check
	return nil
}

// Ensure all expected files are sintactically correct
func (ai *VReportInspector) VerifySyntax(arc *archive.Archive) error {
	// It is a single descriptor to verify
	return nil
}

func (ai *VReportInspector) GetSchemaMap() map[string]string {
	vreportData, _ := vreportFiles.ReadFile("docs/vreport_schema.json")
	return map[string]string{
		"VREPORT": string(vreportData),
	}
}

func (ai *VReportInspector) GetTemplateMap() map[string]string {
	// Provides a baseline archive (folder format) of the artifact
	vreportData, _ := vreportFiles.ReadFile("docs/vreport.templ")
	return map[string]string{
		"VREPORT": string(vreportData),
	}
}

func (ai *VReportInspector) GetOCIConfig(arc *archive.Archive) (*models.OCIConfig, error) {
	// Prepare layers for OCI
	arc.FileRaw.MediaLayer = models.DefaultMediaTypeLayer
	oci := &models.OCIConfig{}

	// Prepare annotations for manifest
	annotations := make(map[string]string)
	index := 0
	for i, f := range arc.ContentRaw {
		if f.Name == "report.yaml" {
			index = i
			break
		}
	}

	annotations["org.opencontainers.image.description"] = "Verification Report Artifact"
	annotations["org.opencontainers.image.authors"] = "Eviden"

	// Cannot be added to Manifest annotations as it breaks the fileStore
	nFormat := ai.GetNameFormat(arc)
	annotations["org.opencontainers.image.title"] = nFormat["name"]
	annotations["org.opencontainers.image.version"] = nFormat["version"]

	oci.Annotations = annotations
	oci.Name = nFormat["name"]
	oci.Version = nFormat["version"]
	// Load config content
	oci.ConfigMediaType = models.MediaTypeVReportConfig

	config := &NFVConfig{
		SchemaVersion:   "1",
		ConfigMediaType: oci.ConfigMediaType,
		Descriptor: Descriptor{
			Path:    filepath.Join(arc.ContentRaw[index].Dir, arc.ContentRaw[index].Name),
			Id:      annotations["org.opencontainers.image.title"],
			Version: annotations["org.opencontainers.image.version"],
		},
	}
	oci.ConfigContent, _ = json.MarshalIndent(config, "", "\t")

	return oci, nil
}
