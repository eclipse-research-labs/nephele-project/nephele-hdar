package interfaces

import (
	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

// The "NewInspector" is responsability of the implementer (inspector module + other projects)
// to load it directly to the artifact struct
type ArtifactInspector interface {
	// Infers if the archive corresponds to the inspector
	IsRightInspector(arc *archive.Archive) bool
	// Returns the name convention based on the descriptor. "name", "version"
	GetNameFormat(arc *archive.Archive) map[string]string
	// Ensure all expected files are present (just the file format and locations)
	VerifyStructure(arc *archive.Archive) error
	// Ensure all expected files are sintactically correct
	VerifySyntax(arc *archive.Archive) error
	// Provide the required annotations for the
	GetOCIConfig(arc *archive.Archive) (*models.OCIConfig, error)
	// Provide a JSON schema path map based on the artifact type
	GetSchemaMap() map[string]string
	// Provide a template path map based on the artifact type
	GetTemplateMap() map[string]string
}
