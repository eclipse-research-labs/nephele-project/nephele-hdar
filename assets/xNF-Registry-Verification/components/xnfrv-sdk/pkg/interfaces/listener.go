package interfaces

import (
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

// Implementers shall fill the corresponding field of the VerificationReport using whatever tools needed
type ListenerInterface interface {
	// Arrange the execution of the stages
	ExecutePipeline(report *models.VerificationReport)
	// Scan for CVEs
	StageScan(report *models.VerificationReport, artifactName string)
	// Execute Checks for Syntax, Structure etc
	StageLint(report *models.VerificationReport, artifactName string)
	// Generate signature
	StageIntegrity(report *models.VerificationReport, artifactName string)
}
