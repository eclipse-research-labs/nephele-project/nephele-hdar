package template

// Pending to move to go-common/components/util
// Scaffolding based on https://pkg.go.dev/gitlab.com/metakeule/scaffold/lib/scaffold

import (
	"os"
	"strings"

	"gitlab.com/metakeule/scaffold/lib/scaffold"
	"go-common/components/utils/logger"
)

func GenerateLocalArtifact(baseDir, templateContent, customContent string) bool {
	defaultInput, template := scaffold.SplitTemplate(templateContent)
	if defaultInput == "" {
		logger.Log.Error("Template file should contain the default input as a json and the template instructions separated by an empty line\n%s\n", templateContent)
		return false
	}
	var inputContent *strings.Reader
	if customContent == "" {
		inputContent = strings.NewReader(defaultInput)
	} else {
		inputContent = strings.NewReader(customContent)
	}
	logger.Log.Info("Generating local archieve")
	err := scaffold.Run(baseDir, template, inputContent, os.Stdout, false)
	if err != nil {
		logger.Log.Errorf("Unable to generate artifact: %s\n", err.Error())
		return false
	}
	logger.Log.Info("Archieve generated")
	return true
}
