package listeners

import (
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	"xnfrv/components/xnfrv-sdk/pkg/interfaces"
)

// Wrapper struct to be able to extend the Listener implementations in other projects
// Configured at the listeners/listeners_config.go level in xnfv-listeners
type SelectListener struct {
	SelectListenerFunction func(manifest v1.Manifest) interfaces.ListenerInterface
}

// Since the listeners are implemented at the xnfv-listener component,
// (they need many local tools not required by the api or ctl components,
// there is no baseline function here.
func AddCorrectListener(manifest v1.Manifest) interfaces.ListenerInterface {
	return nil
}
