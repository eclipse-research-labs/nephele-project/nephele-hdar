// Keep as a different package to enforce testing as a consumer of the lib
package models_test

import (
	"go-common/components/utils/filesystem"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v2"

	// Installed as a consumer
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

func TestGenerateAndVerifyVReport(t *testing.T) {
	expectedYAML := `id: report-someid
timestamp: 1702478636
artifact:
	artifactId: someid
	artifactVersion: 1.0.1
	artifactFullRepo: http://registry:5000/reg/repo/someid:1.0.1
	checksum: somechecksum
lint:
	report:
		description: some description
		tool: xnfrv
		success: false
		logs:
		- some message
	resultsFile: ""
scan:
	report:
		description: some description
		tool: trivy
		success: false
		logs:
		- some message
	summary:
		unknown: 0
		low: 0
		medium: 0
		high: 0
		critical: 0
	resultsFile: ""
integrity:
	report:
		description: some description
		tool: cosign
		success: false
		logs:
		- some message
	signatureImage: ""
`
	filesystem.LoadEnvFile("../../install/.env-tests")
	event := &models.OCIEvent{}
	event.Target.Repository = "reg/repo/someid"
	event.Target.Tag = "1.0.1"

	vReport := models.NewVerificationReport(event)
	vReport.Timestamp = 1702478636 // time.Now().Unix()
	vReport.Artifact.Checksum = "somechecksum"
	vReport.Integrity.Report.Description = "some description"
	vReport.Integrity.Report.Tool = "cosign"
	vReport.Integrity.Report.Success = false
	vReport.Integrity.SignatureImage = ""
	vReport.Integrity.Report.Logs = []string{"some message"}
	vReport.Lint.Report.Description = "some description"
	vReport.Lint.Report.Tool = "xnfrv"
	vReport.Lint.Report.Success = false
	vReport.Lint.Report.Logs = []string{"some message"}
	vReport.Scan.Report.Description = "some description"
	vReport.Scan.Report.Tool = "trivy"
	vReport.Scan.Report.Success = false
	vReport.Scan.Report.Logs = []string{"some message"}
	vReport.Scan.ResultsFile = ""
	vReport.Scan.Summary.Unknown = 0
	vReport.Scan.Summary.Low = 0
	vReport.Scan.Summary.Medium = 0
	vReport.Scan.Summary.High = 0
	vReport.Scan.Summary.Critical = 0
	// Test write to file
	data, err := yaml.Marshal(vReport)
	assert.NoError(t, err)
	err = os.WriteFile("output.yaml", data, 0644)
	defer os.Remove("output.yaml")
	assert.NoError(t, err)
	assert.Equal(t, expectedYAML, strings.ReplaceAll(string(data), "  ", "\t"))
	// Test read from file
	data2, err := os.ReadFile("output.yaml")
	assert.NoError(t, err)
	var vReport2 models.VerificationReport
	err = yaml.Unmarshal(data2, &vReport2)
	assert.NoError(t, err)
	assert.EqualValues(t, vReport, &vReport2)
}
