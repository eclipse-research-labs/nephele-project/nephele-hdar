package models

import (
	"fmt"
	"go-common/components/utils/logger"
	"os"
	"strings"
	"time"
)

var LintMessage = "Static analysis of artifact: syntax, semantics, structure, format."
var ScanMessage = "Scan analysis in search of known common vulnerabilities or missconfiguration."
var IntegrityMessage = "Generated a signature to verify integrity before running artifact."

type ArtifactMetadata struct {
	ArtifactID       string `yaml:"artifactId"`
	ArtifactVersion  string `yaml:"artifactVersion"`
	ArtifactFullRepo string `yaml:"artifactFullRepo"`
	Checksum         string `yaml:"checksum"`
}

type VerificationReport struct {
	Id        string           `yaml:"id"`
	Timestamp int64            `yaml:"timestamp"`
	Artifact  ArtifactMetadata `yaml:"artifact"`
	Lint      LintReport       `yaml:"lint"`
	Scan      ScanReport       `yaml:"scan"`
	Integrity IntegrityReport  `yaml:"integrity"`
}

type CommonReport struct {
	Description string   `yaml:"description"`
	Tool        string   `yaml:"tool"`
	Success     bool     `yaml:"success"`
	Logs        []string `yaml:"logs"`
}

type Summary struct {
	Unknown  int64 `yaml:"unknown"`
	Low      int64 `yaml:"low"`
	Medium   int64 `yaml:"medium"`
	High     int64 `yaml:"high"`
	Critical int64 `yaml:"critical"`
}

type LintReport struct {
	Report      CommonReport `yaml:"report"`
	ResultsFile string       `yaml:"resultsFile"`
}

type IntegrityReport struct {
	Report         CommonReport `yaml:"report"`
	SignatureImage string       `yaml:"signatureImage"`
	// public key or similar to verify?
}

type ScanReport struct {
	Report  CommonReport `yaml:"report"`
	Summary Summary      `yaml:"summary"`
	// String of the result json
	ResultsFile string `yaml:"resultsFile"`
}

func NewVerificationReport(event *OCIEvent) *VerificationReport {
	repoURL := fmt.Sprintf("%s/%s:%s", os.Getenv("REGISTRY_URL"), event.Target.Repository, event.Target.Tag)
	logger.Log.Debugf("repoURL: %s\n", repoURL)
	imageId := strings.Split(event.Target.Repository, "/")[len(strings.Split(event.Target.Repository, "/"))-1]
	return &VerificationReport{
		Id:        fmt.Sprintf("report-%s", imageId),
		Timestamp: time.Now().Unix(),
		Artifact: ArtifactMetadata{
			// TODO Add checksum/digest of the full artifact???
			// check if we can do it based on the content of the manifest-image
			ArtifactID:       imageId,
			ArtifactVersion:  event.Target.Tag,
			ArtifactFullRepo: repoURL,
		},
		Lint: LintReport{
			Report: CommonReport{
				Description: LintMessage,
			},
		},
		Scan: ScanReport{
			Report: CommonReport{
				Description: ScanMessage,
			},
		},
		Integrity: IntegrityReport{
			Report: CommonReport{
				Description: IntegrityMessage,
			},
		},
	}
}
