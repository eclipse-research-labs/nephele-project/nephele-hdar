package models

var MediaTypeVReportConfig = "application/vnd.eviden.verification.config.v1+json"
var MediaTypeSignatureConfig = "application/vnd.oci.image.config.v1+json"
var MediaTypeSignatureLayer = "application/vnd.dev.cosign.simplesigning.v1+json"
var DefaultMediaTypeLayer = "application/tar+gzip"
