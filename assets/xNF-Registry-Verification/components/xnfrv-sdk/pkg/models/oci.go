package models

import (
	"time"

	v1 "github.com/opencontainers/image-spec/specs-go/v1"
)

// Provide the OCI info for interactions with OCI Registry
type OCIConfig struct {
	// json object for the custom OCI config
	ConfigContent   []byte
	ConfigMediaType string
	Annotations     map[string]string
	Name            string
	Version         string
}

// Once in an OCI Registry, an artifact is defined by an
// Image manifest and a Config Manifest
// Each manifest has a descriptor that defines its storage
type OCISpec struct {
	ImageDescriptor       v1.Descriptor
	ImageManifestContent  []byte
	ImageManifest         v1.Manifest
	ConfigDescriptor      v1.Descriptor
	ConfigManifestContent []byte
}

type OCIDistributionNotification struct {
	Events []OCIEvent `json:"events"`
}

type OCIEvent struct {
	ID        string    `json:"id"`
	Timestamp time.Time `json:"timestamp"`
	Action    string    `json:"action"`
	Target    struct {
		MediaType  string `json:"mediaType"`
		Size       int    `json:"size"`
		Digest     string `json:"digest"`
		Length     int    `json:"length"`
		Repository string `json:"repository"`
		URL        string `json:"url"`
		Tag        string `json:"tag"`
	} `json:"target"`
	Request struct {
		ID        string `json:"id"`
		Addr      string `json:"addr"`
		Host      string `json:"host"`
		Method    string `json:"method"`
		UserAgent string `json:"useragent"`
	} `json:"request"`
	Actor  struct{} `json:"actor"`
	Source struct {
		Addr       string `json:"addr"`
		InstanceID string `json:"instanceID"`
	} `json:"source"`
}
