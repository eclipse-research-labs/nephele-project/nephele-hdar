package archive

import (
	"errors"
	"fmt"
	"go-common/components/utils/logger"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

// pending to generalize and put in go-common/components/utils/filesystem

func EnsureGZFormat(a Archive) error {
	if !strings.HasSuffix(a.FileRaw.Name, ".tgz") && !strings.HasSuffix(a.FileRaw.Name, ".tar.gz") {
		return errors.New("Cannot load a file that is not a gz tarball")
	} else if contentType := http.DetectContentType(a.FileRaw.Data); contentType != "application/x-gzip" {
		return fmt.Errorf("%s does not seem to be a tar.gz file. Has contentType %s", a.FileRaw.Name, contentType)
	}
	return nil
}

// should probably depend on a struct and be private
func UnGzip(a *Archive, target string) error {
	// Prepare gz+tar reader from file
	for _, fRaw := range a.ContentRaw {
		os.MkdirAll(filepath.Join(target, fRaw.Dir), 0777)
		if fRaw.Name == "" {
			logger.Log.Debugf("Empty entry in ContentRaw when unGzipping %+v\n", fRaw)
			continue
		}
		if err := os.WriteFile(filepath.Join(target, fRaw.Dir, fRaw.Name), fRaw.Data, 0644); err != nil {
			return err
		}
	}
	return nil
}
