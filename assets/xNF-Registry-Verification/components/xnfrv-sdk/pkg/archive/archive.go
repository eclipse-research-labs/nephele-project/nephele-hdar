package archive

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"go-common/components/utils/logger"
)

/*
Utilities for artifact files in local storage.
TODO: Implement interface approach for zip ?
*/

// Works with an in-memory tar.gz archive to generate the OCI specs
type Archive struct {
	// Tar.gz content
	FileRaw FileRaw
	// Content of each file inside the tar.gz
	ContentRaw []FileRaw
}

type FileRaw struct {
	Name       string
	Dir        string
	Data       []byte
	MediaLayer string
}

// TODO: Should have a clean memory utility to free space

// Prepares the struct instance to be used
func NewBaselineArchive(path string) (*Archive, error) {
	arc := Archive{}
	f, err := os.Stat(path)
	if err != nil {
		return nil, err
	} else if f.IsDir() {
		// We always work from local archives, not directories
		return nil, errors.New("Needs a tar.gz file. Please use command 'package tar <PATH>' first.")
	}

	// Prepare global layer of the archive
	raw, err := ioutil.ReadFile(path)
	if err != nil && err != io.EOF {
		return nil, fmt.Errorf("file '%s' cannot be read: %s", path, err)
	}
	dir, fileName := filepath.Split(path)
	arc.FileRaw = FileRaw{
		Name: fileName,
		Dir:  dir,
		Data: raw,
	}
	if err = EnsureGZFormat(arc); err != nil {
		return nil, err
	}
	// Load internal files
	if err = arc.loadArchiveFiles(); err != nil {
		return nil, err
	}

	return &arc, nil
}

func NewEmptyArchive() *Archive {
	emptyFileRaw := FileRaw{
		Data: make([]byte, 0),
		Dir:  "",
		Name: "",
	}
	return &Archive{
		FileRaw:    emptyFileRaw,
		ContentRaw: []FileRaw{emptyFileRaw},
	}
}

func (arc *Archive) loadArchiveFiles() error {
	// Prepare gz+tar reader from file
	fReader, err := os.Open(filepath.Join(arc.FileRaw.Dir, arc.FileRaw.Name))
	defer fReader.Close()
	if err != nil {
		return err
	}
	unzipped, err := gzip.NewReader(fReader)
	defer unzipped.Close()
	if err != nil {
		return err
	}

	tr := tar.NewReader(unzipped)
	for { // While not EOF
		hd, err := tr.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		// Contents need to come in a directory, check if the tarball was correctly created
		dir, fileName := filepath.Split(hd.Name)
		if dir == "" {
			// This cannot happen because we would extract contents directly on the work dir
			return fmt.Errorf("tarball content '%s' bad formatted. Expecting a parent directory", hd.Name)
		}
		buffer := make([]byte, hd.Size)
		_, err = tr.Read(buffer)
		if err != nil && err != io.EOF {
			return fmt.Errorf("file '%s' cannot be read: %s", fileName, err)
		}
		logger.Log.Debugf("Adding '%s' '%s' to artifact inmemory\n", dir, fileName)
		arc.ContentRaw = append(arc.ContentRaw, FileRaw{
			Name:       fileName,
			Dir:        dir,
			Data:       buffer,
			MediaLayer: "application/" + filepath.Ext(fileName),
		})
	}
	return nil
}
