// Keep as a different package to enforce testing as a consumer of the lib
package archive_test

import (
	"testing"

	// Installed as a consumer
	"xnfrv/components/xnfrv-sdk/pkg/archive"
)

func TestCreateFromTarball(t *testing.T) {
	// We are working with a helm chart for simplicity and to test .tgz structure.
	// ToDo: Should probably try to fall back to helm commands if this is detected.
	// Users should only use xnfr for custom oci artifacts (osm for now)
	filepath := "../../resources/nginx-0.1.0.tgz"
	a, err := archive.NewBaselineArchive(filepath)
	if a == nil || err != nil {
		t.Errorf("Unable to process the file %s", err)
	} else if a.FileRaw.Name != "nginx-0.1.0.tgz" || len(a.FileRaw.Data) == 0 {
		t.Errorf("Global info was not extracted %s", err)
	}

}

func TestCannotCreateFromDir(t *testing.T) {
	filepath := "../../resources/demoartifact"
	_, err := archive.NewBaselineArchive(filepath)
	if err == nil {
		t.Error("We should not be able to load an archive from a dir")
	}

}
