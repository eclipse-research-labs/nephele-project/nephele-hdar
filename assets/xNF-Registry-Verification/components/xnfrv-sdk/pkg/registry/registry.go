package registry

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"slices"
	"sort"
	"strings"

	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	credentials "github.com/oras-project/oras-credentials-go"
	"oras.land/oras-go/v2"
	"oras.land/oras-go/v2/content"
	"oras.land/oras-go/v2/content/file"
	"oras.land/oras-go/v2/registry/remote"
	"oras.land/oras-go/v2/registry/remote/auth"
	"oras.land/oras-go/v2/registry/remote/retry"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"

	"xnfrv/components/xnfrv-sdk/pkg/artifact"
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

func NewRepoWithExistingDockerCredentials(url string) (*remote.Repository, error) {
	// Used to access specific repos in a registry, auth should be in docker already
	repo, err := newRepoWithOptions(url)
	if err != nil {
		return nil, err
	}
	auth, err := newAuthenticatorFromDocker()
	if err != nil {
		return nil, err
	}
	repo.Client = auth
	// Now check if the tag is available
	t := repo.Reference.Reference
	if repo.Reference.Reference == "" {
		// TODO: Find the newest one from all tags when used for pull
		ctx := context.Background()
		err = repo.Tags(ctx, "", func(tags []string) error {
			if slices.Contains(tags, "latest") {
				t = "latest"
				return nil
			}
			// TODO check order and get latest
			// We are taking the first one returned (last pushed?)
			logger.Log.Debugf("tags are '%+v'\n", tags)
			for _, tag := range tags {
				if strings.HasSuffix(tag, ".sig") {
					// This is a signature. ignore it
					continue
				}
				t = tag
				break
			}
			return nil
		})
		if err != nil {
			logger.Log.Infof("Unable to access tags %s, using '%s'\n", err.Error(), t)
		}
		repo.Reference.Reference = t
	}
	if t == "" {
		return nil, err
	}
	return repo, nil
}

func NewRegWithExistingDockerCredentials(url string) (*remote.Registry, error) {
	// Used to access registry-level APIs (list cmd). auth should be in docker already
	reg, err := newRegWithOptions(url)
	if err != nil {
		return nil, err
	}
	auth, err := newAuthenticatorFromDocker()
	if err != nil {
		return nil, err
	}
	reg.Client = auth
	return reg, nil

}

func NewRegWithStaticCredentials(url, username, password string) (*remote.Registry, error) {
	// Only way to add new credentials. User may do docker/helm login instead
	reg, err := newRegWithOptions(url)
	if err != nil {
		return nil, err
	}
	// by default uses credStore of docker during credentials.Login
	a, err := newAuthenticatorStatic(reg.Reference.Reference, username, password)
	if err != nil {
		logger.Log.Info("Failed here")
		return nil, err
	}
	reg.Client = a

	ctx := context.Background()
	// Login returns ErrPlaintextPutDisabled if native (from docker engine) store is
	// not available and StoreOptions.AllowPlaintextPut is set to false.
	storeOpts := credentials.StoreOptions{
		AllowPlaintextPut: true,
	}
	credStore, err := credentials.NewStoreFromDocker(storeOpts)
	if err != nil {
		logger.Log.Errorf("Unable to load docker cred store %s\n", err.Error())
		return nil, err
	}
	creds := auth.Credential{
		Username: username,
		Password: password,
	}

	if err = credentials.Login(ctx, credStore, reg, creds); err != nil {
		logger.Log.Errorf("Username or Password incorrect %s\n", err.Error())
		return nil, err
	}

	return reg, nil

}

func newAuthenticatorFromDocker() (*auth.Client, error) {
	// We will be using the docker approach for storing credentials
	// ToDo: might need to support login without docker
	storeOpts := credentials.StoreOptions{}
	credStore, err := credentials.NewStoreFromDocker(storeOpts)
	if err != nil {
		return nil, err
	}
	// Prepare the auth client for the registry and credential store
	return &auth.Client{
		Client:     retry.DefaultClient,
		Cache:      auth.DefaultCache,
		Credential: credentials.Credential(credStore), // Use the credential store
	}, nil

}
func newAuthenticatorStatic(url, u, p string) (*auth.Client, error) {
	// Prepare the auth client for the registry/repository and credential store
	return &auth.Client{
		Client: retry.DefaultClient,
		Cache:  auth.DefaultCache,
		Credential: auth.StaticCredential(url, auth.Credential{
			Username: u,
			Password: p,
		}),
	}, nil

}

func newRegWithOptions(url string) (*remote.Registry, error) {
	ref := url
	PlainHTTP := false
	if strings.HasPrefix(url, "http://") {
		ref = strings.TrimPrefix(url, "http://")
		PlainHTTP = true
	}
	logger.Log.Infof("ref: %s", ref)
	reg, err := remote.NewRegistry(ref)
	if err != nil {
		return nil, err
	}
	reg.PlainHTTP = PlainHTTP
	return reg, err
}

func newRepoWithOptions(url string) (*remote.Repository, error) {
	ref := url
	PlainHTTP := false
	if strings.HasPrefix(url, "http://") {
		ref = strings.TrimPrefix(url, "http://")
		PlainHTTP = true
	}
	repo, err := remote.NewRepository(ref)
	if err != nil {
		return nil, err
	}
	repo.PlainHTTP = PlainHTTP
	return repo, err
}

func FetchManifest(repo *remote.Repository) (*models.OCISpec, error) {
	ctx := context.Background()
	// fetch image descriptor only (info about target content -image manifest-)
	imageDesc, err := repo.Resolve(ctx, repo.Reference.Reference)
	if err != nil {
		logger.Log.Errorf("Unable pull image descriptor: %s\n", err.Error())
		return &models.OCISpec{}, err
	}
	// fetch image manifest content
	imageManifestContent, err := content.FetchAll(ctx, repo, imageDesc)

	if err != nil {
		logger.Log.Errorf("Unable to pull image content: %s\n", err.Error())
		return &models.OCISpec{}, err
	}
	// unmarshal manifest content to extract config descriptor
	var imageManifest v1.Manifest
	if err := json.Unmarshal(imageManifestContent, &imageManifest); err != nil {
		return &models.OCISpec{}, err
	}
	logger.Log.Infof("Manifest %+v\n", imageManifest)
	// fetch config manifest content
	configContent, err := content.FetchAll(ctx, repo, imageManifest.Config)
	if err != nil {
		logger.Log.Errorf("Unable to pull config content: %s\n", err.Error())
		return &models.OCISpec{}, err
	}
	return &models.OCISpec{
		ImageDescriptor:       imageDesc,
		ImageManifestContent:  imageManifestContent,
		ImageManifest:         imageManifest,
		ConfigDescriptor:      imageManifest.Config,
		ConfigManifestContent: configContent,
	}, nil
}

func List(reg *remote.Registry) (map[string]string, error) {
	// TODO: Allow registry repo filtering
	contentMap := map[string]string{}
	ctx := context.Background()
	logger.Log.Infof("Checking repos in %+v\n", reg.Reference.Registry)
	// "Last" option not working
	err := reg.Repositories(ctx, "", func(repos []string) error {
		contentMap = make(map[string]string, len(repos))
		logger.Log.Debugf("Analyzing %d different artifacts. Might take a while\n", len(repos))
		for _, repoName := range repos {
			url := fmt.Sprintf("%s/%s", reg.Reference.Registry, repoName)
			if reg.PlainHTTP {
				url = fmt.Sprintf("http://%s", url)
			} else if strings.Contains(url, ":") {
				url = fmt.Sprintf("https://%s", url)
			}
			//Credentials are valid but repository does not have tags
			repo, err := NewRepoWithExistingDockerCredentials(url)
			if err != nil {
				logger.Log.Infof("Unable to create Repo %s: %s\n", url, err.Error())
				continue
			}
			// Access the artifact info
			manifests, err := FetchManifest(repo)
			if err != nil {
				logger.Log.Infof("Unable to access artifact credentials %+v: %s\n", repo.Reference, err.Error())
				return err
			}
			// cannot work with signtaures, replace with info about valid tag
			if manifests.ConfigDescriptor.MediaType == models.MediaTypeSignatureConfig {
				logger.Log.Infof("We have found a signature of an models. We need a different tag than '%s'\n", url)
				err := repo.Tags(context.Background(), "", func(tags []string) error {
					logger.Log.Debugf("Found %+v tags", tags)
					// Take one that is not the signature
					finalTag := ""
					for _, t := range tags {
						if strings.HasSuffix(t, ".sig") {
							// This is a signature. ignore it
							continue
						}
						finalTag = t
					}
					if finalTag == "" {
						return fmt.Errorf("unable to find tag that is not a signature: %+v", tags)
					}
					// replace tag and re-request  info
					repo.Reference.Reference = finalTag
					manifests, err = FetchManifest(repo)
					if err != nil {
						logger.Log.Error(err.Error())
						err = fmt.Errorf("unable to fetch manifest of %s: %s", url, err.Error())
						return err
					}
					contentMap[url] = manifests.ConfigDescriptor.MediaType
					return nil
				})
				if err != nil {
					logger.Log.Error(err.Error())
					err = fmt.Errorf("unable to replace signature tag with something else %s: %s", url, err.Error())
					return err
				}
			} else {
				contentMap[url] = manifests.ConfigDescriptor.MediaType
			}

			// Single layer artifact support for now
			mediaType := manifests.ConfigDescriptor.MediaType
			contentMap[url] = mediaType

		}
		return nil
	})
	if err != nil {
		logger.Log.Infof("Unable to access artifacts %s\n", err.Error())
		return contentMap, err
	}
	if len(contentMap) == 0 {
		logger.Log.Info("Not found any repository matching description.")
		return contentMap, nil
	}
	// Order by repo
	orderedRepos := make([]string, 0)
	orderedContentMap := map[string]string{}
	for k := range contentMap {
		orderedRepos = append(orderedRepos, k)
	}
	sort.Strings(orderedRepos)
	for _, k := range orderedRepos {
		orderedContentMap[k] = contentMap[k]
	}
	return orderedContentMap, nil
}

func Pull(repo *remote.Repository, fs oras.Target) (*models.OCISpec, error) {
	ctx := context.Background()
	// Copy from the remote repository to the destination
	logger.Log.Infof("Pulling %s:%s\n", repo.Reference.Repository, repo.Reference.Reference)
	// TODO: tgz helm charts are not copied for some reason
	desc, err := oras.Copy(ctx, repo, repo.Reference.Reference, fs, repo.Reference.Reference, oras.DefaultCopyOptions)
	if err != nil {
		logger.Log.Errorf("Unable to copy artifact from remote: %s\n", err.Error())
		return &models.OCISpec{}, err
	}
	logger.Log.Debugf("Pulled desc %+v\n", desc)
	logger.Log.Info("Pull successful\n")
	// There is no direct way to now the name of the file. Bring the manifest content
	manifests, err := FetchManifest(repo)
	if err != nil {
		logger.Log.Errorf("Unable to get manifests: %s\n", err.Error())
		return &models.OCISpec{}, err
	}
	return manifests, nil
}

func Push(repo *remote.Repository, a *artifact.Artifact) error {
	// Copy the artifact to /tmp so that there are no local folders with the same name creating conflicts
	workDir := "/tmp/xnfrv"
	err := filesystem.CreateDirIfNotExist(workDir)
	if err != nil {
		logger.Log.Errorf("could not prepare workdir in /tmp: %s", err.Error())
		return err
	}
	tmpF := filepath.Join(workDir, a.InMemoryArchive.FileRaw.Dir, a.InMemoryArchive.FileRaw.Name)
	err = filesystem.RenameFile(filepath.Join(a.InMemoryArchive.FileRaw.Dir, a.InMemoryArchive.FileRaw.Name), tmpF, false)
	if err != nil {
		logger.Log.Errorf("could not prepare workdir in /tmp: %s", err.Error())
		return err
	}
	defer os.Remove(tmpF)

	// 0. Create a file store to build
	fs, err := file.New(workDir)
	if err != nil {
		logger.Log.Info("Unable to create local file store\n")
		logger.Log.Info(err.Error())
		return err
	}
	defer fs.Close()
	ctx := context.Background()

	// 1. Add files to a file store and create the manifest layer

	// 1.1 Layers
	// Working with a single layer of the entire tar.gz
	// TODO: Does file-level layers reduce mem storage or improve layer sharing?
	filePaths := []string{filepath.Join(a.InMemoryArchive.FileRaw.Dir, a.InMemoryArchive.FileRaw.Name)}
	fileDescriptors := make([]v1.Descriptor, 0, len(filePaths))
	for _, path := range filePaths {
		fileDescriptor, err := fs.Add(ctx, a.InMemoryArchive.FileRaw.Name, a.InMemoryArchive.FileRaw.MediaLayer, path)
		logger.Log.Infof("Descriptor Manifest Layer %+v\n", fileDescriptor)
		if err != nil {
			logger.Log.Info("Unable to create descriptor from layer\n")
			logger.Log.Info(err.Error())
			return err
		}
		fileDescriptors = append(fileDescriptors, fileDescriptor)
	}
	// No additional annotations required for the layer
	// 1.2 Config file
	// Prepare the config file
	configFile := filepath.Join(workDir, "config.json")
	f, _ := os.Create(configFile)
	defer f.Close()
	defer os.Remove(configFile)
	f.Write(a.OCIPush.ConfigContent)
	configDesc, err := fs.Add(ctx, "$config", a.OCIPush.ConfigMediaType, configFile)
	if err != nil {
		logger.Log.Info("Unable to create config descriptor\n")
		logger.Log.Info(err.Error())
		return err
	}
	configDesc.Annotations = make(map[string]string)

	// 2. Pack the files and tag the packed manifest

	packOpts := oras.PackOptions{
		PackImageManifest:   true,
		ConfigDescriptor:    &configDesc,
		ManifestAnnotations: a.OCIPush.Annotations,
	}

	manifestDesc, err := oras.Pack(ctx, fs, "", fileDescriptors, packOpts)
	if err != nil {
		// TODO: Would be great to fix this somehow
		logger.Log.Info("Unable to Pack. Cannot push from where the artifact base folder is located\n")
		logger.Log.Info(err.Error())
		return err
	}
	// oras.Pack generates the manifest as a json file and stores it in fs, which is PWD in our case.
	// we don't want it there. keep it in-memory only
	//defer os.Remove(result.Value["org.opencontainers.image.title"])
	logger.Log.Infof("manifest descriptor: %+v\n", manifestDesc)

	srcTag := a.OCIPush.Version
	dstTag := srcTag // we are not re-tagging
	if err = fs.Tag(ctx, manifestDesc, srcTag); err != nil {
		logger.Log.Info("Unable to tag manifest descriptor\n")
		logger.Log.Info(err.Error())
		return err
	}

	// 3. Copy from the file store to the remote repository
	_, err = oras.Copy(ctx, fs, srcTag, repo, dstTag, oras.DefaultCopyOptions)
	logger.Log.Debugf("%s --- %s --- %+v", srcTag, dstTag, repo.Reference)
	return err
}
