**Latest Tag Release**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfrvsdk-coverage-latesttag.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfrvsdk-version-latesttag.svg)

**Develop**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfrvsdk-coverage-develop.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/wiki/xnfrvsdk-version-develop.svg)

# xNF Registry & Verification SDK

Library to develop and extend with the xNF Registry and Verification Asset.
Extension to project-specific artifacts (Nephele) via golang interfaces called "inspectors" (for the xnfrctl) and listeners (for the xnfv).

## Description

For the xnfrctl: We have developed libraries that leverage the ORAS project to create custom OCI artifacts which involves the creation of a global manifest, a config file and the annotations. At the xNFR-CTL we only want to keep those implementations that are used by SNS's assets. Currently available:
- By-default: docker and helm charts.
- OSM VNF and NS
- Verification Engine Report
- Already extended in the Nephele project for: Project-specific Helm chart, project-specific artifact (HDA) 

Once created the artifact can be pushed/pulled to an OCI-compatible registry (e.g., Distribution, Dockerhub, Harbor, Artifactory, Nexus, Github...). 

For the xnfv: We have developed a custom pipeline that executes verification checks by leveraging custom tools and also those from the telco and CN industry over each artifacts and generates a verification report. Currently includes:
- Syntax (single and multi-file)
- Structure (expected files/folders etc)
- Scan (CVE)
- Integrity (signature)

## Getting Started

- archive: Low-level module that deals with the local filesystem-bundles (as directories and as tar.gz). e.g., It loads them into memory so that it can be used to create the OCI artifacts.
- artifact: Top-level module which aggregates an archive and the selected implementation of the interfaces used to inspect and extract information of the archive depending on the type.
- interfaces: Definition of the xnfrv-sdk interfaces to extend the asset. 
  + inspectors: Contains the baseline implementation of the xnfr part, to create OCI artifacts
  + ../xnfv-listeners/pkg/listeners: Contains the baseline implementation of the xnfv part, to execute the verification pipelines.
- registry: Deals with the OCI-compatible registry using the ORAS SDK. It offers access at the registry and repository level depending on the needs.
- template: Enables a way to generate a baseline artifact using scaffolding techniques.
- models: Data models and definitions specific to the xNFRV asset and OCI

### Prerequisites

Full configuration via ENV Vars:

- Check $PWD/install/.env-test for local deployment and move it to $PWD/.env

```bash
cp install/.env-tests .env 
export $(grep -v '^#' .env | xargs)
```

#### Software

Project built using Golang 1.22.

#### Hardware

No major requirements found. 1vCPU, 1GiB RAM

### Installation
Project developed using WSL Golang installation

1. Go auto formatting and linting. Execute before pushing to origin. It is automated in CI pipeline and will fail if not ok

```bash
go fmt $(go list ./... | grep -v /vendor/)
go vet $(go list ./... | grep -v /vendor/)
```

2. Test it

```bash
go test $(go list ./... | grep -v /vendor/) -v
```

## Usage

This is a module for a Golang sdk (as a library) and thus is part of other modules which create a binary. Only dry-run tests are executed here (no direct connection to external services or tools)
- xnfr-ctl
- xnfv-api
- xnfv-listeners
- Nephele repository

For the xnfv implementations, they require a baseline docker image containing the required tools to perform the verification pipelines
Upgrade the base image if new tools are added:
```bash
docker build -t registry.atosresearch.eu:18467/xnfv_base:develop -f install/xnfv_base.dockerfile .
```

## Contribution

Tech:

- Guillermo Gomez guillermo.gomezchavez@eviden.com

Asset Owner:

- Sonia Castro sonia.castro@eviden.com

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or PR created from GitHub as draft.
3. Commit and push frequently.
4. When ready, set PR as ready, tag team and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```
