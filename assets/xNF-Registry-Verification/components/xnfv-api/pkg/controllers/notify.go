package controllers

import (
	"net/http"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfv-listeners/pkg/redis"
)

// Based on the list of events received filter out those that are not push. Let the listeners ignore
// since they are many more.
func ProcessEvents(n *models.OCIDistributionNotification) (httpCode int) {
	// Load publisher
	publisher := redis.NewPublisher()
	for _, event := range n.Events {
		if event.Action != "push" {
			// Our only interest right now is on push events
			logger.Log.Debugf("Ignoring %s for manifest %s\n", event.Action, event.Target.Repository)
			continue
		}
		logger.Log.Infof("Processing %s for manifest %s\n", event.Action, event.Target.Repository)
		logger.Log.Debugf("%+v\n", event)
		// publish event
		err := publisher.PublishPushEvent(event)
		if err != nil {
			logger.Log.Infof("Unable to publish event: %s", err.Error())
			return http.StatusInternalServerError
		}
		logger.Log.Info("Event sent")

	}
	return http.StatusAccepted
}

// Resend configure request to listeners
func ProcessConfigure() (httpCode int) {
	// Load publisher
	logger.Log.Info("Requesting configure")
	publisher := redis.NewPublisher()
	err := publisher.PublishConfigureEvent()
	if err != nil {
		logger.Log.Infof("Unable to publish configure: %s", err.Error())
		return http.StatusInternalServerError
	}
	return http.StatusAccepted
}
