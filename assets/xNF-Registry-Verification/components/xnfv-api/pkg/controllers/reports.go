package controllers

import (
	"fmt"
	"go-common/components/utils/logger"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func GetReportData(artifact string) (int, []byte) {
	// Make sure it is correctly formatted
	// Should check that there is no info about registry?
	aComp := strings.Split(artifact, ":")
	if len(aComp) != 2 {
		return http.StatusBadRequest, []byte("Artifact needs to contain the version/tag and no references to registry base URL")
	}
	// The report comes with a fixed id structure of report-artifactId:artifactId
	project, repo, tag := "", strings.Split(artifact, ":")[0], strings.Split(artifact, ":")[1]
	i := strings.LastIndex(artifact, "/")
	if i != -1 {
		// there is a project prefix-> RBAC implemented in OCI Registry
		project = artifact[:i+1]
		repo = artifact[i+1:]
	}
	reportId := fmt.Sprintf("report-%s", repo)
	defer os.RemoveAll(reportId)
	fullPath := fmt.Sprintf("%s/%s%s:%s", os.Getenv("REGISTRY_URL"), project, reportId, tag)
	logger.Log.Debugf("Checking %s, %s = %s\n", project, repo, fullPath)
	logger.Log.Infof("Pulling report from %s\n", fullPath)
	// TODO: need to parametrice the name of the CLI
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "pull", fullPath, "--untar")
	output, err := cmd.CombinedOutput()
	if err != nil {
		msg := fmt.Sprintf("Cannot pull artifact: %s -> %s\n", err.Error(), string(output))
		logger.Log.Error(msg)
		return http.StatusInternalServerError, []byte(msg)
	}
	logger.Log.Debugf("Pull output: %s\n", string(output))

	data, err := os.ReadFile(filepath.Join(reportId, "report.yaml"))
	if err != nil {
		msg := fmt.Sprintf("Cannot find report: %s -> %s\n", filepath.Join(reportId, "report.yaml"), err.Error())
		logger.Log.Errorf(msg)
		return http.StatusInternalServerError, []byte(msg)
	}
	return http.StatusOK, data

}
