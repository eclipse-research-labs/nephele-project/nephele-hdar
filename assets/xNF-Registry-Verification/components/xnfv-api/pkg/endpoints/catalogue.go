package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"net/http"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfv-api/pkg/controllers"
)

// GetReport godoc
// @Summary Verification Report
// @Description Obtain the Verification Report for an artifact
// @Accept  plain
// @Produce  application/yaml
// @Param artifact query string true "Full name of the artifact e.g., myartifact:1.0.0, someproject/myartifact:1.0.0"
// @Router /vreport [get]
func GetReport(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Received a request")
	// Obtain the name from the path
	artifact := r.URL.Query().Get("artifact")
	// Check if 'artifact' parameter is not empty
	if artifact == "" {
		http.Error(w, "Artifact parameter is required", http.StatusBadRequest)
		return
	}
	returnCode, data := controllers.GetReportData(artifact)
	w.WriteHeader(returnCode)
	w.Header().Set("Content-Type", "application/yaml")
	_, _ = w.Write(data)
}
