package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"encoding/json"
	"fmt"
	"net/http"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfv-api/pkg/controllers"
)

// notify godoc
// @Summary Webhook for OCIDistributionNotifications
// @Description Endpoint triggered by events over the xNF Registry
// @Accept  json
// @Produce  plain
// @Param OCIDistributionNotification body models.OCIDistributionNotification true "OCIDistributionNotification payload from an OCI Registry"
// @Router /notify [post]
func Notify(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Received a OCIDistributionNotification")
	var OCIDistributionNotification models.OCIDistributionNotification
	err := json.NewDecoder(r.Body).Decode(&OCIDistributionNotification)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	b, err := json.Marshal(OCIDistributionNotification.Events[0])
	if err != nil {
		logger.Log.Errorf("Error processing input: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Internal Error reading event")
		return
	}
	logger.Log.Debugf("Received %+v\n", string(b))
	// Run the scans/verification in the background here
	returnCode := controllers.ProcessEvents(&OCIDistributionNotification)
	w.WriteHeader(returnCode)
	fmt.Fprintf(w, "OCIDistributionNotification processed")
}
