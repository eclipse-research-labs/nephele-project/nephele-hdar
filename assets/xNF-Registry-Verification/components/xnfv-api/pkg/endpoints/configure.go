package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfv-api/pkg/controllers"
)

// Configure godoc
// @Router /configure [get]
// @Tags  internal
// @Summary Registry Login
// @Description Configures the login credentials towards the OCI Registry
// @Produce  plain
// @Success		200
func Configure(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Requested to configure")
	// Create login record with admin privileges
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "login", os.Getenv("REGISTRY_URL"), "-u", os.Getenv("REGISTRY_ADMIN_USER"), "-p", os.Getenv("REGISTRY_ADMIN_PASSWORD"))
	stdout, err := cmd.Output()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Cannot log to registry: %s\n%s\n", err.Error(), stdout)
		return
	}
	// Run the scans/verification in the background here
	returnCode := controllers.ProcessConfigure()
	w.WriteHeader(returnCode)
	fmt.Fprint(w, "API ready, listeners requested")

}
