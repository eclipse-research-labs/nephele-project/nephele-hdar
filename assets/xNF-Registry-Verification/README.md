# xNF-Registry & Verification Engine

Provides an extension of OCI-Registries for xNFs and a Verification Engine for telco and cloud-native artifacts.

# Documentation

The High level System's Architecture and functional block is shown below.

![Architecture high level](./docs/HighLevel.png)

A more detailed version of what is planned to be achieved is shown below.

![Architecture low level](./docs/LowLevel.png)

Workflow

![Workflow](./docs/workflow-0.5.0.png)

More info
- Check [Teams Asset folder](https://eviden.sharepoint.com/:f:/r/sites/SNSUnit/Shared%20Documents/General/Assets%20(documentation)?csf=1&web=1&e=yt11qS) for related deliverables and other non-tech information
- Check D4.2 of Nephele for the implementation of the asset 

References
- https://medium.com/@nizepart/automation-of-building-signing-and-verifying-docker-images-kaniko-cosign-kyverno-769d4ccccf3d
- https://medium.com/@seifeddinerajhi/sign-and-verify-container-images-with-cosign-and-kyverno-a-complete-guide-b32b1f6e6264
- https://docs.docker.com/registry/notifications/#endpoints
- https://docs.docker.com/registry/notifications/#monitoring

# Usage

```bash
# first time
git submodule update init --recursive --remote
# next times
git submodule update --recursive --remote
```

Each component is developed individually and then deployed in K8s via helm charts from the deploy/k8s folder

## xnfr

The xNF Registry is the combination of the OCI-Registry with the xnfrv cli to be able to integrate custom OCI artifacts.

Currently supports storage of:
- Containers
- Helm Charts (v3)
    + **Helm issue to push behind a keycloak is currently a PR. Please use the helm binary from tools/ folder**. Else compile from here https://github.com/helm/helm/pull/13138
- OSM VNF and NS

Planned storage of:
- VMs (VMDK)
- RPMs

Additional support of other artifacts in project's implementation. This is achieved by extending the xnfrv-sdk library.
- Nephele:
    + Custom Helm Charts (Virtual Objects)
    + Custom Hyper-Distributed Application Graph

Supported OCI Registries [Compatibility list](https://oras.land/docs/compatible_oci_registries). From the asset side we just need to make sure that it allows notifications on push to connect the xNF Verificaton Engine):
- Github
- Nexus
- Harbor
- Distribution (Docker Registry) <-- Used by default

## xnfv

The xNF Verification is a custom service which listens to notifications from OCI Registry (xNFR) and executes verification pipelines (scanning, integrity, structure, syntax ..) using the required tools:
- Trivy: For Containers, Helm, VM images to find vulnerabilities & IaC misconfigurations
- OSM CLI: Extended with advanced syntax, topology and integrity checks.
- xnfrv: Custom CLI for additional implementations supported by the xNFR
- cosign: To provide an integrity validation of all artifacts uploaded. 


# Roadmap

- [X] Evolution of the NFV Registry using OCI Registries as Storage (ORAS)
- [X] Specification for OCI image and metadata for ETSI-NFV artifacts
- [X] Backend for service catalog and discovery
- [X] Extensibility for other artifact implementations
- Multilayer creation of ETSI-NFV OCI artifacts
- [X] Private and Public repos & multitenancy
- [X] Artefact integrity certification
- Plan to detect NFV specific vulnerabilities
- OSM feature to integrate with asset
    - Helm pull
    - OSM repository pull
- Additional proposals in wiki
- Integration with xNF Smart Contract Asset
    - Event notification to Oracles
    - Accept/Refuse artifact usage based on Smart Contract
- Integration with a E2E Orchestration Engine
    - Onboarding to ETSI MANO
    - Predictive proxy of artifacts to extreme edge
- Aligment with IDnomic project and roadmap
    - Digital signatures and integrity validation of artifacts using Smart Contracts and a DLT.
- Align with latest ETSI NFV full-CN specifications
