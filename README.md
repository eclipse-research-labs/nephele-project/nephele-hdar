# xNF-RV-NEPHELE-HDAR

**The HDA-Registry (HDAR) is the common storage, distribution and verification system for all the artifacts involved in the deployment of HDA via the Nephele SMO framework.** Each artifact is stored in a repository inside the HDAR where all tags of the same artifact are available.

HDAR leverages an Open Container Initiative ([OCI](https://opencontainers.org/)) compatible image Registry as Storage ([ORAS](https://oras.land/)) to finally align the storage, distribution and exposure mechanisms for artifacts of the Vertical Service Providers (VSP) and the Communication Service Providers (CSP).
The HDAR is built as the combination of multiple components

![Architecture](./docs/HDAR_architecture.png)

## Description

This multi-project repository hosts the commitments of T4.1. Please refer to WP4 deliverables.

![Full T4.1](./docs/t4-1.png)

- The Control CLI currently focuses on: Interacts with the registry via **registry.nephele-hdar.netmode.ece.ntua.gr**
  1. CLI to interact with the HDAR will be provided as a single executable file for ubuntu envs-> hdarctl.
  2. Basic interaction with an OCI-Registry for a custom type of artifact allowing to push OSM NS and VNF and Nephele applications
  3. Development toolkit to manage the artifacts involved in a HDAG. 
      - Templating mechanism to create Nephele applications
      - Verification of syntax, semantic and integrity of components
  4. **The HDAR is prepared to be interacted with by devs using CLI tools**:
      - docker
      - helm (v3): No support for V2 repo index.yaml
      - hdar: commands following the Helm structure. Library and CLI to interact with the Hyper Distributed Application Registry.

- The REST API currently focuses on: Available **hdarapi.nephele-hdar.netmode.ece.ntua.gr/swagger/index.html**
  1. Custom functionalities for ease the use of the Nephele meta-orchestration framework
  2. Service catalogue and discovery

- The Verification Engine focuses on: Available indirectly via the HDAR REST API and on Available **hdarv.nephele-hdar.netmode.ece.ntua.gr/swagger/index.html**
  1. Verification of syntax, semantic and integrity of components
  2. Scanning of Docker images and VM images searching for CVE
  3. Scanning Helm charts and OSM descriptors searching for missconfigurations

- The development environment sandbox is a template repository in gitlab that is used by HDA developers and that automates the generation of CICD pipelines based on the hdarctl tool. 
- The development environment documentation hosts the guidelines to develop a HDA and to interact with the NEPHELE Platform. This is done via a sphinx project similar to readthedocs. Available **documentation.nephele-hdar.netmode.ece.ntua.gr**


The T4.1 contributions in the NEPHELE Platform. Note: The OCI config manifest be requested to the HDAR REST API to know what the artifact contains and how to process it before accessing the actual artifact.

![Summary](./docs/summaryHDAR.png)

More in the WP4 deliverables of Nephele.

## Usage

```bash
# first time
git submodule update --init --recursive --remote
# next iterations
git submodule update --recursive --remote
```

Each component is developed individually and then deployed in K8s via helm charts from the deploy/k8s folder

## Contribution

Tech:

- Guillermo Gomez guillermo.gomezchavez@eviden.com

Asset Owner:

- Sonia Castro sonia.castro@eviden.com

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or PR created from GitHub as draft.
3. Commit and push frequently.
4. When ready, set PR as ready, tag team and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```
