**Latest Tag Release**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarsdk-coverage-latesttag.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarsdk-version-latesttag.svg)

**Develop**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarsdk-coverage-develop.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarsdk-version-develop.svg)

# HDAR SDK

Extends the xNFRV-SDK to implement the inspectors (Listeners are inplemented in the hdarv) required by the NEPHELE project which are then consumed by:
- hdarctl
- hdarv
- hdarapi
- dev-env-sandbox

## Description

Library extending the xNFRV SDK

## Getting Started

Check deliverable D2.1, D2.2 and D4.1

- pkg/listeners: For (c)VOs, HDAGs and helm charts
- pkg/dev_env:  custom functionalities for the dev-env-sandbox which are exposed via the hdarctl

### Prerequisites

Full configuration via ENV Vars:

- Check $PWD/install/.env-test for local deployment and move it to $PWD/.env

#### Software

#### Hardware

### Installation

Project developed using WSL Golang installation

1. Go auto formatting and linting. Execute before pushing to origin. It is automated in CI pipeline and will fail if not ok

```bash
go fmt $(go list ./... | grep -v /vendor/)
go vet $(go list ./... | grep -v /vendor/)
```

2. Make sure tests pass

```bash
go test $(go list ./... | grep -v /vendor/) -v
```

## Usage

This project contains the source code for a library, no actual binary/release is done.

Only for testing and to replicate the CICD you can create docker image locally or pull the one generated by the CICD pipeline

```bash
# Needs the full repo context
cd ../../
docker build -t registry.atosresearch.eu:18498/hdarsdk:latest --network host -f components/hdar-sdk/install/dockerfile . 
cd components/hdar-sdk
```

## Contribution

Tech:

- Guillermo Gomez guillermo.gomezchavez@eviden.com

Asset Owner:

- Sonia Castro sonia.castro@eviden.com

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or PR created from GitHub as draft.
3. Commit and push frequently.
4. When ready, set PR as ready, tag team and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```
