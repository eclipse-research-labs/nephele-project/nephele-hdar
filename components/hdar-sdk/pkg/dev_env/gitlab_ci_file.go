package devenv

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"go-common/components/utils/logger"
	"gopkg.in/yaml.v2"
)

// Based on the relative path to an artifact, populates the gitlab.ci file as required
// The caller makes sure that the artifact is created in the right folder
// .gitlab-ci.yml and build-and-push.sh are transfered to the dev env sandbox
func AdaptLocalFile(aPath, project string) error {
	baseFolder, aName := filepath.Split(aPath)
	if baseFolder == "" || aName == "" {
		return fmt.Errorf("%s not formated with <baseFolder>/artifactFolder>", aPath)
	}
	switch baseFolder {
	case "vertical-apps/":
	case "virtual-objects/":
	case "hda-graphs/":
	default:
		return fmt.Errorf("unknown location for artifact, is not one of the standard base folders: %s", baseFolder)
	}
	// Set the file path to the GitLab CI YAML file. The location is important
	ciFile := ".gitlab-ci.yml"
	// Check if the file exists
	if _, err := os.Stat(ciFile); os.IsNotExist(err) {
		logger.Log.Errorf("Error: %s file not found", ciFile)
		return err
	}
	stages := []string{"test", "push"}
	// Read the .gitlab-ci.yml file
	config, err := readYAMLFile(ciFile)
	if err != nil {
		logger.Log.Errorf("Error reading YAML file: %v", err)
		return err
	}
	for _, stage := range stages {
		// Add a new job that belongs to the new stage
		j := addJob(baseFolder, aName, stage, project)
		if j == nil {
			logger.Log.Error("Unable to create the job")
			return fmt.Errorf("error for %s, %s", aName, stage)
		}
		name := strings.TrimSuffix(baseFolder, "/") + "-" + aName + "-" + stage
		newJob := map[string]interface{}{
			name: *j,
		}
		for k, v := range newJob {
			config[k] = v
		}
	}

	// Rewrite the updated configuration back to the .gitlab-ci.yml file
	err = writeYAMLFile(ciFile, &config)
	if err != nil {
		logger.Log.Errorf("Error writing YAML file: %v", err)
		return err
	}

	return nil
}

// Job defines the structure of a CI job
type Job struct {
	Rules  []Rule   `yaml:"rules"`
	Stage  string   `yaml:"stage"`
	Script []string `yaml:"script"`
}
type Rule struct {
	When    string   `yaml:"when"`
	Changes []string `yaml:"changes"`
}

func addJob(directory, artifactId, stage, project string) *Job {
	steps := []string{"export PATH=${CLIS_PATH}:$PATH"}
	if stage == "test" {
		steps = append(steps, fmt.Sprintf(`hdarctl lint %s%s`, directory, artifactId))
		steps = append(steps, `if [ $? -eq 1 ]; then echo "Command failed" && exit 1; fi`)
	} else if stage == "push" {
		steps = append(steps, `echo "login to HDAR"`)
		steps = append(steps, "hdarctl login registry.nephele-hdar.netmode.ece.ntua.gr -u $HDAR_USER -p $HDAR_PASSWORD")
		steps = append(steps, `if [ $? -eq 1 ]; then echo "Command failed" && exit 1; fi`)
		if project != "" {
			steps = append(steps, fmt.Sprintf("bash tools/build-and-push.sh -d %s -a %s -p %s", directory, artifactId, project))
		} else {
			steps = append(steps, fmt.Sprintf("bash tools/build-and-push.sh -d %s -a %s", directory, artifactId))
		}
		steps = append(steps, `if [ $? -eq 1 ]; then echo "Command failed" && exit 1; fi`)

	} else {
		logger.Log.Infof("no stage known %s", stage)
		return nil
	}
	return &Job{
		Rules: []Rule{{
			When:    "always",
			Changes: []string{filepath.Join(directory, artifactId) + "/**"},
		}},
		Stage:  stage,
		Script: steps,
	}
}

// Function to read the gitlab-ci.yml file as a mapstr.
// We want to reuse the pre-checks, variables etc, but we want to
// add new jobs labeled as the <directory>-<artifactId>
func readYAMLFile(filename string) (map[string]interface{}, error) {
	var content map[string]interface{}
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(data, &content)
	if err != nil {
		return nil, err
	}
	return content, nil
}

// Function to write the updated gitlab-ci.yml file
func writeYAMLFile(filename string, config *map[string]interface{}) error {
	data, err := yaml.Marshal(config)
	if err != nil {
		return err
	}

	err = os.WriteFile(filename, data, 0644)
	if err != nil {
		return err
	}

	return nil
}
