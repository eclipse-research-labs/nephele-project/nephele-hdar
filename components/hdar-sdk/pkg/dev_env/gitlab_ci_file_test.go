package devenv

import (
	"os"
	"testing"
)

// Test function to verify adding a stage and job to the .gitlab-ci.yml file
func TestAdaptLocalFile(t *testing.T) {

	yamlContent := `name: example-app
version: "1.0.0"
dependencies:
- dependency1
- dependency2
`

	// Create a YAML file
	ciFile := ".gitlab-ci.yml"
	file, err := os.Create(ciFile)
	if err != nil {
		t.Fatalf("error creating file: %v", err)
	}
	defer os.Remove(ciFile)

	// Write the YAML content to the file
	if _, err := file.WriteString(yamlContent); err != nil {
		t.Fatalf("error writing to file: %v", err)
		return
	}
	file.Close()

	err = AdaptLocalFile("pascual", "juan")
	if err == nil {
		t.Fatal("Should have got the error in the path argument")
		return
	}
	err = AdaptLocalFile("pascual/marquez", "juan")
	if err == nil {
		t.Fatal("Should have got the error that baseFolder is standardized")
		return
	}
	err = AdaptLocalFile("vertical-apps/marquez", "juan")
	if err != nil {
		t.Fatalf("It should have worked: %s", err.Error())
		return
	}
	config, err := readYAMLFile(ciFile)
	if err != nil {
		t.Fatalf("Error reading back the file: %s", err.Error())
		return
	}
	if _, ok := config["vertical-apps-marquez-test"]; !ok {
		t.Fatalf("Expected new job not found")
		return
	}
	if _, ok := config["vertical-apps-marquez-push"]; !ok {
		t.Fatalf("Expected new job not found")
		return
	}
}
