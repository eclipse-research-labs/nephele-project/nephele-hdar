package utils

import (
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chartutil"
	"helm.sh/helm/v3/pkg/engine"
)

// Extracted from https://github.com/helm/helm/blob/v3.13.1/pkg/lint/rules/template.go
func RenderTemplates(chart *chart.Chart) (map[string]string, error) {
	cvals, err := chartutil.CoalesceValues(chart, map[string]interface{}{})
	if err != nil {
		return map[string]string{}, err
	}
	valuesToRender, err := chartutil.ToRenderValues(chart, cvals, chartutil.ReleaseOptions{}, nil)
	if err != nil {
		return map[string]string{}, err
	}
	var e engine.Engine
	e.LintMode = false
	renderedContentMap, err := e.Render(chart, valuesToRender)
	return renderedContentMap, err
}
