package inspectors

import (
	"embed"
	"encoding/json"
	"fmt"
	"path/filepath"
	"strings"

	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/lint"

	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/models"
)

//go:embed docs/helm*
var helmFiles embed.FS

type HelmInspector struct{}

// It is a Helm chart and we need to replicate the OCI spec
var MediaTypeHelmConfig = "application/vnd.cncf.helm.config.v1+json"
var MediaTypeHelmLayer = "application/vnd.cncf.helm.chart.content.v1.tar+gzip"

// The config is the chart.Metadata (Chart.yaml file)

func NewHelmInspector() *HelmInspector {
	return &HelmInspector{}
}

// Infers if the archive corresponds to the inspector
func (ai *HelmInspector) IsRightInspector(arc *archive.Archive) bool {
	descriptor := ""
	for _, f := range arc.ContentRaw {
		if strings.HasSuffix(f.Dir, "scripts/") && f.Name == "config.yaml" {
			// It is a VO
			return false
		}
		if f.Name == "Chart.yaml" || f.Name == "Chart.yml" {
			// assign but wait in case it is a VO
			descriptor = f.Name
		}
	}
	return descriptor != ""
}

// Everything after this is executed with something we are sure is an Helm artifact

// Returns the name convention based on the descriptor. "name", "version"
func (ai *HelmInspector) GetNameFormat(arc *archive.Archive) map[string]string {
	nFormat := map[string]string{"version": "", "name": ""}
	// Load chart
	chart, err := loader.Load(filepath.Join(arc.FileRaw.Dir, arc.FileRaw.Name))
	if err != nil {
		return nFormat
	}
	nFormat["name"] = chart.Metadata.Name
	nFormat["version"] = chart.Metadata.Version
	return nFormat
}

// Ensure all expected files are present (just the file format and locations)
func (ai *HelmInspector) VerifyStructure(arc *archive.Archive) error {
	// It should be a helm chart with expected folders and manifests
	l := lint.All(arc.FileRaw.Dir, map[string]interface{}{}, "", true)
	if len(l.Messages) > 0 {
		fullMsg := ""
		for _, msg := range l.Messages {
			if msg.Severity == 4 {
				fullMsg = fmt.Sprintf("\t-%s\n", msg.Err.Error())
			}
		}
		if fullMsg != "" {
			return fmt.Errorf(fullMsg)
		}
	}
	return nil
}

// Ensure all expected files are sintactically correct
func (ai *HelmInspector) VerifySyntax(arc *archive.Archive) error {
	// Structure has already validated that we have the required files for each implementation
	// The lint in struct is enough
	return nil
}

func (ai *HelmInspector) GetSchemaMap() map[string]string {
	helmData, _ := helmFiles.ReadFile("docs/helm_schema.json")
	return map[string]string{
		"HELM": string(helmData),
	}
}

func (ai *HelmInspector) GetTemplateMap() map[string]string {
	helmData, _ := helmFiles.ReadFile("docs/helm.templ")
	return map[string]string{
		"HELM": string(helmData),
	}
}

func (ai *HelmInspector) GetOCIConfig(arc *archive.Archive) (*models.OCIConfig, error) {
	// Prepare layers for OCI
	arc.FileRaw.MediaLayer = MediaTypeHelmLayer
	oci := &models.OCIConfig{}
	// Load chart
	chart, err := loader.Load(filepath.Join(arc.FileRaw.Dir, arc.FileRaw.Name))
	if err != nil {
		return oci, err
	}
	oci.Annotations = chart.Metadata.Annotations
	oci.Name = chart.Metadata.Name
	oci.Version = chart.Metadata.Version
	// Load config content
	oci.ConfigMediaType = MediaTypeHelmConfig
	oci.ConfigContent, _ = json.MarshalIndent(chart.Metadata, "", "\t")

	return oci, nil
}
