package inspectors

import (
	"reflect"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
	xnfr_inspectors "xnfrv/components/xnfrv-sdk/pkg/inspectors"
	"xnfrv/components/xnfrv-sdk/pkg/interfaces"
)

// try-n-error to find the artifact's required Inspector
func AddCorrectInspector(a *artifact.Artifact, verify bool) (found bool, err error) {
	// Check if it is a OSM artifact
	if found, err := xnfr_inspectors.AddCorrectInspector(a, verify); found {
		return found, err
	}
	err = nil
	logger.Log.Debug("Not found in baseline xnfrv library")
	inspectorList := []interfaces.ArtifactInspector{NewHDAGInspector(), NewVOInspector(), NewHelmInspector()}
	for _, inspector := range inspectorList {
		if err := a.LoadWithInspector(&inspector); err == nil {
			logger.Log.Infof("Found that the artifact is type %s\n", reflect.TypeOf(inspector).Elem().Name())
			if verify {
				if err := a.Lint(); err != nil {
					logger.Log.Debug(err.Error())
					return true, err
				}
				logger.Log.Debug("No errors on verification")
			}
			return true, nil
		} else {
			logger.Log.Debug(err.Error())
		}
	}
	logger.Log.Debug("No inspector found")
	return false, err

}
