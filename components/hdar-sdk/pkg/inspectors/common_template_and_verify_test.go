// Keep as a different package to enforce testing as a consumer of the lib
package inspectors_test

import (
	"os"
	"testing"

	// Installed as a consumer
	"hdar/components/hdar-sdk/pkg/inspectors"

	"go-common/components/utils/filesystem"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"

	xnfr_inspectors "xnfrv/components/xnfrv-sdk/pkg/inspectors"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/template"
)

func TestVerifyTemplate(t *testing.T) {
	// Make sure no local artifact is left
	// Table-Driven testing
	data := []struct {
		input         string
		errorMsg      string
		created       bool
		expectedAName string
		expectedDName string
		expectedDType string
	}{
		{
			input:         inspectors.NewHDAGInspector().GetTemplateMap()["HDAG"],
			errorMsg:      "should have located template file correctly",
			created:       true,
			expectedAName: "myfirsthdag-1.0.0.tar.gz",
			expectedDName: "myfirsthdag",
			expectedDType: "HDAG",
		},
		{
			input:         inspectors.NewVOInspector().GetTemplateMap()["VO_WOT"],
			errorMsg:      "should have located template file correctly",
			created:       true,
			expectedAName: "unique-id-vo-0.1.0.tar.gz",
			expectedDName: "unique-id-vo",
			expectedDType: "VO_WOT",
		},
	}
	for _, val := range data {
		aName := val.expectedAName
		dName := val.expectedDName
		defer os.RemoveAll(aName)
		defer os.RemoveAll(dName)
		created := template.GenerateLocalArtifact(".", val.input, "")
		if created != val.created {
			t.Errorf("%s test -> %t vs %t: %s\n", val.expectedAName, created, val.created, val.errorMsg)
			return
		}
		if created {
			_, err := os.Stat(dName)
			if err != nil {
				t.Errorf("Should not have created the base dir for artifact %s", err.Error())
				return
			}
			// create the tar.gz to pass later the verification

			err = filesystem.WriteGZFile(aName, dName, ".")
			if err != nil {
				t.Errorf("Unable to write to GZ temp file: %s", err.Error())
				return
			}
			// Pass verification
			a, err := artifact.NewLocalArtifact(aName)
			if err != nil {
				t.Error(err.Error())
			}
			found, err := inspectors.AddCorrectInspector(a, true)
			if !found {
				t.Error("Unable to load descriptor with inspector provided.")
			} else if err != nil {
				t.Errorf("Error found in artifact: %s\n", err.Error())
			}
		}
	}
}

func TestArtifactInspectors(t *testing.T) {
	// Pass verification
	data := []struct {
		path                    string
		expectedLayerMediaType  string
		expectedConfigMediaType string
	}{
		{
			path:                    "../../resources/nginx-0.1.0.tgz",
			expectedLayerMediaType:  inspectors.MediaTypeHelmLayer,
			expectedConfigMediaType: inspectors.MediaTypeHelmConfig,
		},
		{
			path:                    "../../resources/unique-id-vo-0.1.0.tar.gz",
			expectedLayerMediaType:  inspectors.MediaTypeHelmLayer,
			expectedConfigMediaType: inspectors.MediaTypeHelmConfig,
		},
		{
			path:                    "../../resources/myfirstvnf-0.1.0.tar.gz",
			expectedLayerMediaType:  models.DefaultMediaTypeLayer,
			expectedConfigMediaType: xnfr_inspectors.MediaTypeNFVConfig,
		},
		{
			path:                    "../../resources/myfirstns-0.1.0.tar.gz",
			expectedLayerMediaType:  models.DefaultMediaTypeLayer,
			expectedConfigMediaType: xnfr_inspectors.MediaTypeNFVConfig,
		},
		{
			path:                    "../../resources/myfirsthdag-0.1.0.tar.gz",
			expectedLayerMediaType:  models.DefaultMediaTypeLayer,
			expectedConfigMediaType: inspectors.MediaTypeHDAGConfig,
		},
	}
	for _, val := range data {
		a, err := artifact.NewLocalArtifact(val.path)
		if err != nil {
			t.Error(err.Error())
		}
		found, err := inspectors.AddCorrectInspector(a, true)
		if !found || err != nil {
			t.Errorf("Unable to load descriptor with inspector provided for %s.: %+v", val.path, err)
		} else if a.InMemoryArchive.FileRaw.MediaLayer != val.expectedLayerMediaType {
			t.Errorf("Invalid mediaLayer %s.", val.path)
		} else if a.OCIPush.ConfigMediaType != val.expectedConfigMediaType {
			t.Errorf("Invalid mediaConfig %s.", val.path)
		}
	}
}
