{
  "description": "some very long description",
  "hdaGraphIntent": "OPTION1",
  "id": "myfirsthdag",
  "designer": "ATOS",
  "version": "1.0.0",
  "services": [
    {
      "deploymentType": "auto",
      "intent": {
        "network": "OPTION1",
        "resources": "OPTION1",
        "metrics": true
      },
      "id": "vo",
      "ref": "nephele-hdar.netmode.ece.ntua.gr/myhdag:1.0.0"
    }
  ]
}

>>>{{ .id }}/
>>>descriptor.yaml
hdaGraph:
  imVersion: 0.4.0
  id: {{ if .id }}{{ .id }}{{ else }}unique_id_hdag{{ end }}
  version: {{ if .version }}"{{ .version }}"{{ else }}"1.0.0"{{ end }}
  designer: {{ if .designer }}{{ .designer }}{{ else }}ATOS{{ end }}
  hdaGraphIntent: 
    security: # Either colocation either VO-related security either app-graph deployed using the same Blueprint
      enabled: False
    highAvailability: # Application graph is not considered for reconfigurations. Placement is final
      enabled: False
    highPerformance: # Defined end-to-end for all services. If selected, overwrites the per service latency qos intent
      enabled: False
    energyEfficiency: # Cost reduction of cloud-deployed services
      enabled: False
  description: >-
    {{ .description }}
  services:
  {{- range .services }}
    - id: {{ .id }}
      deployment:
        trigger: {{ if eq .deploymentType "auto" }}
          auto:
            dependencies: []{{ else }}
          event:
            condition: ANY
            events:
              - id: "PLACEHOLDER"
                source: 
                  - serviceId: "PLACEHOLDER"
                    metricId: "PLACEHOLDER"
                condition: # This needs to be mapped to a prometheus query by the monitoring component
                  promQuery: absent(up{prometheus="kc1/kube-prometheus-operator"})
                  gracePeriod: 2m
                  description: "High vo-metric"{{ end }}
        intent:
          network:
            deviceProximity: # Only relevant for the VO
                enabled: False # If true, enable TSN
            latencies:
              - connectionPoint: "" # Relevant for the next service in the application graph
                qos: "best-effort"
                # "ultralow"    - under 10ms
                # "low"         - 1hop maximum
                # "best-effort" - default value
          compute:
            cpu: "small"
            # "light"  - 0.5 vCPU
            # "small"  - 1   vCPU
            # "medium" - 4   vCPU
            # "large"  - 8   vCPU
            ram: "small"
            # "light"  - 500MB RAM
            # "small"  - 1  GB RAM
            # "medium" - 2  GB RAM
            # "large"  - 8  GB RAM
            storage: "small"
            # "small"  - 10GB
            # "medium" - 20GB
            # "large"  - 40GB
            gpu:
              enabled: True 
          coLocation: []
            # - id: 2
            #   groundedGraphId: UUID
          connectionPoints: []{{ if .intent.metrics }}
          metrics: # list of metrics to be scraped by centralized monitoring system.
            - id: "PLACEHOLDER" # used for events
              metricName: "PLACEHOLDER" #name of metric exposed to prometheus
              selector: # the target service (label). Cannot be to a pod directly, has to point to the service
                PLACEHOLDER: PLACEHOLDER
              endpoint: # why would we need more than one??. a second metric will have an additional entry in this list 
                port: metrics # name of exposed port
                interval: 1s # scraping period of prometheus{{ end }} 
      artifact:
        # if tag is not added, newest tag will be selected by the docker, helm and hdarctl CLIs. 
        # removing oci:// because should not customize the descriptor for implementations
        #   The OCI thing is only for helm, SMO/FRM should add it based on the target
        #   e.g., tag has a different way of adding it in helm (":1.0.0" --> "--version 1.0.0").  which would also need a custom entry in the descriptor. Better to have translation of descriptor
        ociImage: {{ .ref }}
        ociConfig:
          type: VO
          implementer: WOT
        ociRun:
          name: HELM
          version: v3
        valuesOverwrite: {}
 {{- end }}
<<<descriptor.yaml
<<<{{ .id }}/
