package inspectors

import (
	"embed"
	"encoding/json"
	"fmt"
	"path/filepath"
	"strings"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/inspectors"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/utils"
)

//go:embed docs/hdag*
var hdagFiles embed.FS

type HDAGInspector struct{}

var MediaTypeHDAGConfig = "application/vnd.nephele.hdag.config.v1+json"

/*
This is fully customizable and should allow us to implement adaptations for HDAG, TOSCA and others.
Each case should obtain the info from the corresponding descriptor.
*/
type HDAGConfig struct {
	// SchemaVersion to xNFRV version: 1 = [v0.3.0, ]
	SchemaVersion string `json:"schemaVersion"`
	// Config MediaType used to characterize the artifact quickly
	ConfigMediaType string `json:"configMediaType"`
	// Provides a more accurate characteristic than ConfigMediaType
	ArtifactType string `json:"artifactType"`
	// Reference to the specification/version of the content's structure
	DataModelSpec string `json:"dataModelSpec"`
	// Path to the core descriptor file
	Descriptor inspectors.Descriptor `json:"descriptor"`
	// Dependencies of the descriptor
	Dependencies []inspectors.Dependency `json:"dependencies"`
}

func NewHDAGInspector() *HDAGInspector {
	return &HDAGInspector{}
}

// Infers if the archive corresponds to the inspector
func (ai *HDAGInspector) IsRightInspector(arc *archive.Archive) bool {
	// Only one yaml file in base path
	descriptor := ""
	for _, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			if descriptor != "" {
				logger.Log.Debug("Too many yaml files to be an HDAG artifact")
				return false
			}
			// yaml file has to contain one of the known keys in the IM
			content := string(f.Data)
			if strings.Contains(strings.ToUpper(content), "HDAGRAPH:") {
				descriptor = f.Name
				logger.Log.Debugf("%s descriptor seems to contain HDAG content", descriptor)
			}
		}
	}
	return descriptor != ""
}

// Everything after this is executed with something we are sure is an HDAG artifact

// Returns the name convention based on the descriptor. "name", "version"
func (ai *HDAGInspector) GetNameFormat(arc *archive.Archive) map[string]string {
	nFormat := map[string]string{"version": "", "name": ""}
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	content, _ := ai.loadDescriptorContent(arc.ContentRaw[index].Data)
	if val, ok := content["id"]; ok && val.(string) != "" {
		nFormat["name"] = val.(string)
	}

	if val, ok := content["version"]; ok {
		v, err := utils.EnsureVersionFormat(val)
		if err != nil {
			logger.Log.Errorf("Invalid version in descriptor: %s", err.Error())
			return nFormat
		}
		nFormat["version"] = v
	}
	return nFormat
}

// Ensure all expected files are present (just the file format and locations)
func (ai *HDAGInspector) VerifyStructure(arc *archive.Archive) error {
	if len(arc.ContentRaw) != 1 {
		return fmt.Errorf("HDAG archive should not have more than one file. Found '%d'", len(arc.ContentRaw))
	}
	return nil
}

// Ensure all expected files are sintactically correct
func (ai *HDAGInspector) VerifySyntax(arc *archive.Archive) error {
	// It is a single descriptor to verify
	// Need the content in json format
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	dMap, err := filesystem.LoadYamlContent(arc.ContentRaw[index].Data)
	if err != nil {
		return err
	}
	descriptorJson, err := json.Marshal(dMap)
	if err != nil {
		return fmt.Errorf("unable to convert descriptor to json: %s", err.Error())
	}
	dContent := string(descriptorJson)
	sContent := ai.GetSchemaMap()["HDAG"]

	// Proceed with the verification
	sErrors, iError := utils.VerifyAgainstSchema(sContent, dContent)
	logger.Log.Debugf("sErrors %+v\n", sErrors)
	if iError != nil {
		return fmt.Errorf("internal Error: %s", iError.Error())
	} else if len(sErrors) > 0 {
		return fmt.Errorf("%d errors found:\n%s", len(sErrors), strings.Join(sErrors, "\n"))
	} else {
		logger.Log.Infof("No errors detected")
	}
	return nil
}

func (ai *HDAGInspector) GetSchemaMap() map[string]string {
	// For HDAG descriptors the test is not representative, we would need to
	// use the actual HDAG CLI since we do not want to host the complete IM schema (in yang...)
	hdagData, _ := hdagFiles.ReadFile("docs/hdag_schema.json")
	return map[string]string{
		"HDAG": string(hdagData),
	}
}

func (ai *HDAGInspector) GetTemplateMap() map[string]string {
	// Provides a baseline archive (folder format) of the artifact
	hdagData, _ := hdagFiles.ReadFile("docs/hdag.templ")
	return map[string]string{
		"HDAG": string(hdagData),
	}
}

func (ai *HDAGInspector) GetOCIConfig(arc *archive.Archive) (*models.OCIConfig, error) {
	// Prepare layers for OCI
	arc.FileRaw.MediaLayer = models.DefaultMediaTypeLayer
	oci := &models.OCIConfig{}

	// Prepare annotations for manifest
	annotations := make(map[string]string)
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	content, err := ai.loadDescriptorContent(arc.ContentRaw[index].Data)
	if err != nil {
		return oci, err
	}
	if val, ok := content["description"]; ok {
		annotations["org.opencontainers.image.description"] = val.(string)
	}
	if val, ok := content["designer"]; ok {
		annotations["org.opencontainers.image.authors"] = val.(string)
	}
	// Cannot be added to Manifest annotations as it breaks the fileStore
	nFormat := ai.GetNameFormat(arc)
	annotations["org.opencontainers.image.title"] = nFormat["name"]
	annotations["org.opencontainers.image.version"] = nFormat["version"]

	oci.Annotations = annotations
	oci.Name = nFormat["name"]
	oci.Version = nFormat["version"]
	// Load config content
	oci.ConfigMediaType = MediaTypeHDAGConfig
	config := &HDAGConfig{
		SchemaVersion:   "2",
		ConfigMediaType: MediaTypeHDAGConfig,
		ArtifactType:    "HDAG",
		DataModelSpec:   "0.4.0",
		Descriptor: inspectors.Descriptor{
			Path:    filepath.Join(arc.ContentRaw[index].Dir, arc.ContentRaw[index].Name),
			Id:      oci.Name,
			Version: oci.Version,
		},
		Dependencies: ai.GetDependencies(arc),
	}
	oci.ConfigContent, _ = json.MarshalIndent(config, "", "\t")

	return oci, nil
}

func (ai *HDAGInspector) GetDependencies(arc *archive.Archive) []inspectors.Dependency {
	logger.Log.Debug("Getting Dependencies")
	var dependencies []inspectors.Dependency
	index := 0
	for i, f := range arc.ContentRaw {
		if filepath.Ext(f.Name) == ".yaml" || filepath.Ext(f.Name) == ".yml" {
			index = i
			break
		}
	}
	content, err := ai.loadDescriptorContent(arc.ContentRaw[index].Data)
	if err != nil {
		logger.Log.Debug("This should not have happened")
		return dependencies
	}
	for _, service := range content["services"].([]interface{}) {
		dependencies = append(dependencies, inspectors.Dependency{
			ArtifactType: service.(filesystem.MapStr)["artifact"].(filesystem.MapStr)["ociConfig"].(filesystem.MapStr)["type"].(string),
			Ref:          service.(filesystem.MapStr)["artifact"].(filesystem.MapStr)["ociImage"].(string),
			Location:     "external",
			Mode:         "hard",
		})
	}
	logger.Log.Debug("Done getting dependencies")
	return dependencies
}

func (ai *HDAGInspector) loadDescriptorContent(raw []byte) (filesystem.MapStr, error) {
	mapPointer, err := filesystem.LoadYamlContent(raw)
	if err != nil {
		logger.Log.Errorf("Error reading descriptor: %s", err.Error())
		return nil, err
	}
	return mapPointer["hdaGraph"].(filesystem.MapStr), nil
}
