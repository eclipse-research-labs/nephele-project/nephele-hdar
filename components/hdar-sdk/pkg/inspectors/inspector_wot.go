package inspectors

import (
	"embed"
	"encoding/json"
	"fmt"
	"path/filepath"
	"strings"

	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/lint"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/archive"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	xnfrv_utils "xnfrv/components/xnfrv-sdk/pkg/utils"

	"hdar/components/hdar-sdk/pkg/utils"
)

//go:embed docs/vo*
var voFiles embed.FS

type VOInspector struct{}

var MediaTypeVOConfig = "application/vnd.cncf.helm.config.v1+json"
var MediaTypeVOLayer = "application/vnd.cncf.helm.chart.content.v1.tar+gzip"

/*
In this case we are relying on Helm not reading the OCI config since
in their case is just a copy of the Chart.yaml file. We add an annotation to the config
*/

func NewVOInspector() *VOInspector {
	return &VOInspector{}
}

// Infers if the archive corresponds to the inspector
func (ai *VOInspector) IsRightInspector(arc *archive.Archive) bool {
	descriptor := ""
	for _, f := range arc.ContentRaw {
		if strings.HasSuffix(f.Dir, "scripts/") && f.Name == "config.yaml" {
			if descriptor != "" {
				logger.Log.Debug("Too many yaml files to be an vo artifact")
				return false
			}
			descriptor = f.Name
			logger.Log.Debugf("%s descriptor seems to contain vo content", descriptor)
		}
	}
	return descriptor != ""
}

// Everything after this is executed with something we are sure is an vo artifact

// Returns the name convention based on the descriptor. "name", "version"
func (ai *VOInspector) GetNameFormat(arc *archive.Archive) map[string]string {
	nFormat := map[string]string{"version": "", "name": ""}
	chart, err := loader.Load(filepath.Join(arc.FileRaw.Dir, arc.FileRaw.Name))
	if err != nil {
		logger.Log.Errorf("Invalid Name in descriptor: %s", err.Error())
		return nFormat
	}
	nFormat["name"] = chart.Metadata.Name
	v, err := xnfrv_utils.EnsureVersionFormat(chart.Metadata.Version)
	if err != nil {
		logger.Log.Errorf("Invalid version in descriptor: %s", err.Error())
		return nFormat
	}
	nFormat["version"] = v
	return nFormat
}

// Ensure all expected files are present (just the file format and locations)
func (ai *VOInspector) VerifyStructure(arc *archive.Archive) error {
	// It should be a helm chart with expected folders and manifests
	l := lint.All(arc.FileRaw.Dir, map[string]interface{}{}, "", true)
	if len(l.Messages) > 0 {
		fullMsg := ""
		for _, msg := range l.Messages {
			if msg.Severity == 4 {
				fullMsg = fmt.Sprintf("\t-%s\n", msg.Err.Error())
			}
		}
		if fullMsg != "" {
			return fmt.Errorf(fullMsg)
		}
	}
	logger.Log.Debug("VO chart is valid")
	// Ensure the (c)VO required files exist
	expectedFiles := map[string]bool{
		"scripts/app.py":      false,
		"scripts/config.yaml": false,
		"scripts/td.json":     false,
	}
	artifactType := ""
	for _, fRaw := range arc.ContentRaw {
		_, after, _ := strings.Cut(fRaw.Dir, "/")
		name := fmt.Sprintf("%s%s", after, fRaw.Name)
		if name == "scripts/config.yaml" {
			if strings.Contains(strings.ToUpper(string(fRaw.Data)), "WOT") {
				artifactType = "WOT"
			} else {
				artifactType = "OMA"
			}
			logger.Log.Debugf("Struct check decided VO is '%s'", artifactType)
		}
		if val, ok := expectedFiles[name]; ok {
			if val {
				logger.Log.Debugf("File duplicated\n")
				return fmt.Errorf("%s duplicated", name)
			}
			expectedFiles[name] = true
			continue
		}
		logger.Log.Debugf("%s not found in required files", fRaw.Name)
	}
	if !expectedFiles["scripts/config.yaml"] {
		return fmt.Errorf("VO config file not found un chart")
	}
	if artifactType == "WOT" {
		if !expectedFiles["scripts/app.py"] || !expectedFiles["scripts/td.json"] {
			return fmt.Errorf("missing script files %+v", expectedFiles)
		}
	} else {
		return fmt.Errorf("No other implementations allowed yet")
	}

	return nil
}

// Ensure all expected files are sintactically correct
func (ai *VOInspector) VerifySyntax(arc *archive.Archive) error {
	// Structure has already validated that we have the required files for each implementation
	// Load chart and parse templates
	chart, err := loader.Load(filepath.Join(arc.FileRaw.Dir, arc.FileRaw.Name))
	if err != nil {
		return err
	}
	contents, err := utils.RenderTemplates(chart)
	if err != nil {
		logger.Log.Errorf("Error rendering files: %s", err.Error())
		return err
	}

	configFileContents, err := filesystem.LoadYamlContent([]byte(contents[filepath.Join(chart.Metadata.Name, "templates", "configmap.yaml")]))
	if err != nil {
		logger.Log.Errorf("Error loading configMap contents: %s", err.Error())
		return err
	}
	fToCheck := []string{"config.yaml", "td.json"}
	for _, f := range fToCheck {
		voConfigContent, err := filesystem.LoadYamlContent([]byte(configFileContents["data"].(filesystem.MapStr)[f].(string)))
		if err != nil {
			logger.Log.Errorf("Error extracting VO config.yaml: %s", err.Error())
			return err
		}
		descriptorJson, err := json.Marshal(voConfigContent)
		if err != nil {
			return fmt.Errorf("unable to convert descriptor to json: %s", err.Error())
		}
		dContent := string(descriptorJson)
		sContent := ai.GetSchemaMap()[f]
		// Proceed with the verification
		sErrors, iError := xnfrv_utils.VerifyAgainstSchema(sContent, dContent)
		logger.Log.Debugf("'%s': sErrors %+v\n", f, sErrors)
		if iError != nil {
			return fmt.Errorf("'%s': internal Error: %s", f, iError.Error())
		} else if len(sErrors) > 0 {
			return fmt.Errorf("'%s': %d errors found:\n%s", f, len(sErrors), strings.Join(sErrors, "\n"))
		} else {
			logger.Log.Infof("'%s': No errors detected", f)
		}
	}
	return nil
}

func (ai *VOInspector) GetSchemaMap() map[string]string {
	// For vo descriptors the test is not representative, we would need to
	// use the actual vo CLI since we do not want to host the complete IM schema (in yang...)
	configData, _ := voFiles.ReadFile("docs/vo_config_schema.json")
	// from https://github.com/eclipse-thingweb/playground/blob/master/packages/core/td-schema-full.json
	tdData, _ := voFiles.ReadFile("docs/vo_wot_td_schema.json")
	return map[string]string{
		"config.yaml": string(configData),
		"td.json":     string(tdData),
	}
}

func (ai *VOInspector) GetTemplateMap() map[string]string {
	// Provides a baseline archive (folder format) of the artifact
	voData, _ := voFiles.ReadFile("docs/vo_wot.templ")
	return map[string]string{
		"VO_WOT": string(voData),
	}
}

func (ai *VOInspector) GetOCIConfig(arc *archive.Archive) (*models.OCIConfig, error) {
	// Prepare layers for OCI
	arc.FileRaw.MediaLayer = MediaTypeVOLayer
	oci := &models.OCIConfig{}

	// Prepare annotations for manifest
	annotations := make(map[string]string)

	// Load chart and parse templates
	chart, err := loader.Load(filepath.Join(arc.FileRaw.Dir, arc.FileRaw.Name))
	if err != nil {
		return oci, err
	}
	contents, err := utils.RenderTemplates(chart)
	if err != nil {
		logger.Log.Errorf("Error rendering files: %s", err.Error())
		return oci, err
	}
	// We read the config directly from the rendered version
	// Helm renders based on the chart name but there is no garantee that the user has done it right....
	configFileContents, err := filesystem.LoadYamlContent([]byte(contents[filepath.Join(chart.Metadata.Name, "templates", "configmap.yaml")]))
	if err != nil || configFileContents == nil {
		logger.Log.Errorf("Error loading configMap contents: %s", err.Error())
		return oci, err
	}
	configContent, err := filesystem.LoadYamlContent([]byte(configFileContents["data"].(filesystem.MapStr)["config.yaml"].(string)))
	if err != nil {
		logger.Log.Errorf("Error extracting VO config.yaml: %s", err.Error())
		return oci, err
	}

	annotations["org.opencontainers.image.description"] = chart.Metadata.Description
	if len(chart.Metadata.Maintainers) > 0 {
		annotations["org.opencontainers.image.authors"] = chart.Metadata.Maintainers[0].Name
	}

	nFormat := ai.GetNameFormat(arc)
	annotations["org.opencontainers.image.title"] = nFormat["name"]
	annotations["org.opencontainers.image.version"] = nFormat["version"]
	// We add this to ensure that we can create the catalogue easily
	annotations["vnd.nephele.config.artifactType"] = configContent["type"].(string) + "_" + configContent["resourceType"].(filesystem.MapStr)["specification"].(string)

	oci.Annotations = annotations
	oci.Name = nFormat["name"]
	oci.Version = nFormat["version"]
	// Load config content
	oci.ConfigMediaType = MediaTypeVOConfig
	// decided by Helm
	oci.ConfigContent, _ = json.MarshalIndent(chart.Metadata, "", "\t")

	return oci, nil
}
