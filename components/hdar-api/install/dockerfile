FROM registry.atosresearch.eu:18488/golang:latest-alpine as builder

# Use to skip tests and binary builds to be able to use tools/dev.docker-compose.yml 
ARG DEV

WORKDIR /go/src/hdar/components/hdar-api

# Only copy require code and delete as much as possible (second stage) assets/xNF-Registry-Verification/assets/go-common/components/utils
COPY assets/xNF-Registry-Verification/assets/go-common/components/utils ../../assets/xNF-Registry-Verification/assets/go-common/components/utils
COPY assets/xNF-Registry-Verification/assets/go-common/tools/dockerfile_tools/ .
COPY assets/xNF-Registry-Verification/components/xnfrv-sdk ../../assets/xNF-Registry-Verification/components/xnfrv-sdk
COPY components/hdar-sdk ../hdar-sdk
COPY components/hdar-api .

# Neeed for tests
RUN apk --no-cache add build-base

# Make sure that the code is up to standards and docs are updated
# Run tests and keep report
RUN if [ -z "$DEV" ]; then \
    ./go_tools.sh \
    && ./go_test.sh \
    && git reset --hard HEAD && ./go_swag_check.sh; \
    fi

# Finally build binaries
RUN if [ -z "$DEV" ]; then \
    GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o hdarapi . ;\
    fi

# Reduce final image tag as much as possible
FROM registry.atosresearch.eu:18520/library/alpine

WORKDIR /root/
COPY --from=builder /go/src/hdar/components/hdar-api/hdarapi ./hdarapi
COPY --from=builder /go/src/hdar/components/hdar-api/testReport.txt testReport.txt