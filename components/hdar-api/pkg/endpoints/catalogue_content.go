package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"hdar/components/hdar-sdk/pkg/inspectors"
	"xnfrv/components/xnfrv-sdk/pkg/archive"
	xnfrv_artifact "xnfrv/components/xnfrv-sdk/pkg/artifact"
	"xnfrv/components/xnfrv-sdk/pkg/registry"

	"go-common/components/utils/logger"

	"oras.land/oras-go/v2/content/file"
)

// catalogueDisplay godoc
// @Router /catalogue/{id}/content [get]
// @Tags  default
// @Summary Descriptors of artifact
// @Description Get the contents of the relevant descriptors
// @Produce  json
// @Param id path string true "ID of the image in the HDAR"
// @Param project query string false "project"
// @Success 200  {array} ContentObject "Artifact Content"
func CatalogueContent(w http.ResponseWriter, r *http.Request) {
	logger.Log.Infof("Requested to fetch artifact content at %s\n", r.URL.Path)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	imageId := strings.TrimSuffix(r.URL.Path, "/content/")
	imageId = strings.TrimSuffix(imageId, "/content")
	imageId = strings.TrimPrefix(imageId, "/catalogue/")
	imageId = strings.TrimPrefix(imageId, "/catalogue")
	logger.Log.Infof("Received %+v\n", imageId)
	if imageId == "" || strings.Contains(imageId, "/") {
		err := errors.New("unable to decode imageId")
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	filter := r.URL.Query().Get("project")
	if filter != "" {
		imageId = filter + "/" + imageId
	}
	repoRef := os.Getenv("REGISTRY_URL") + "/" + imageId
	logger.Log.Infof("Trying to access %+v\n", repoRef)
	// asset makes sure that the signature tag is not used as latest tag
	repo, err := registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err != nil {
		logger.Log.Errorf("Unable to connect to repo: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return

	}
	// would be better to check that it is not a docker image -> "content size 29150411 exceeds push size limit 4194304: size exceeds limit"
	fs, err := file.New(".")
	if err != nil {
		logger.Log.Infof("Unable to create file store to pull artifact: %s\n", err.Error())
		return
	}
	defer fs.Close()
	manifests, err := registry.Pull(repo, fs)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	// Find the name of the file
	pattern := "*.tar.gz"
	matches, err := filepath.Glob(pattern)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	if len(matches) > 1 {
		err = fmt.Errorf("Cannot process local env with previous artifacts %+v", matches)
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if len(matches) == 0 { // tgz was downloaded
		err = fmt.Errorf("Artifact was not a (c)VO or a HDAG")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	defer os.Remove(matches[0])
	fBase := "."
	fName := matches[0]
	logger.Log.Infof("File to untar %s\n", filepath.Join(fBase, fName))

	a, err := xnfrv_artifact.NewLocalArtifact(filepath.Join(fBase, fName))
	if err != nil {
		logger.Log.Info(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if found, err := inspectors.AddCorrectInspector(a, false); !found {
		err = fmt.Errorf("unable to add correct inspector implementation during pull %+v", a)
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if err != nil {
		err = fmt.Errorf("verification error: %s", err.Error())
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	err = archive.UnGzip(&a.InMemoryArchive, fBase)
	if err != nil {
		logger.Log.Infof("Error unzipping the artifact: %s\n", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	logger.Log.Infof("untarred artifact in destination directory %s\n", a.InMemoryArchive.ContentRaw[0].Dir)
	dirName := filepath.Join(fBase, a.InMemoryArchive.ContentRaw[0].Dir)
	defer os.RemoveAll(dirName) // Only delete if correctly untar

	descResponse := []ContentObject{}
	// If not HDAG or VO/cVO, exit
	switch manifests.ImageManifest.Config.MediaType {
	case inspectors.MediaTypeHDAGConfig:
		// Read descriptor file
		dContent, err := ioutil.ReadFile(filepath.Join(dirName, a.InMemoryArchive.ContentRaw[0].Name))
		if err != nil {
			logger.Log.Infof("Unable to read file %s\n", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			payload, _ := json.Marshal(err)
			_, _ = w.Write(payload)
			return
		}
		descResponse = append(descResponse, ContentObject{
			Type:     "DESC",
			FileName: a.InMemoryArchive.ContentRaw[0].Name,
			Content:  dContent,
		})
	case inspectors.MediaTypeVOConfig:
		// check if helm chart of Vo
		if _, ok := manifests.ImageManifest.Annotations["vnd.nephele.config.artifactType"]; !ok {
			// This is a helm chart. Return the values.yml
			logger.Log.Debugf("%s is a helm chart", dirName)
			w.WriteHeader(http.StatusNotImplemented)
			payload, _ := json.Marshal(fmt.Errorf("helm chart not implemented %s", dirName))
			_, _ = w.Write(payload)
			return
		}

		// Read config file
		dContent, err := ioutil.ReadFile(filepath.Join(dirName, "scripts", "config.yaml"))
		if err != nil {
			logger.Log.Infof("Unable to read file %s\n", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			payload, _ := json.Marshal(err)
			_, _ = w.Write(payload)
			return
		}
		descResponse = append(descResponse, ContentObject{
			Type:     "DESC",
			FileName: "config.yaml",
			Content:  dContent,
		})
		dContent, err = ioutil.ReadFile(filepath.Join(dirName, "scripts", "app.py"))
		if err != nil {
			logger.Log.Infof("Unable to read file %s\n", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			payload, _ := json.Marshal(err)
			_, _ = w.Write(payload)
			return
		}
		descResponse = append(descResponse, ContentObject{
			Type:     "GENERIC",
			FileName: "app.py",
			Content:  dContent,
		})
		dContent, err = ioutil.ReadFile(filepath.Join(dirName, "scripts", "td.json"))
		if err != nil {
			logger.Log.Infof("Unable to read file %s\n", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			payload, _ := json.Marshal(err)
			_, _ = w.Write(payload)
			return
		}
		descResponse = append(descResponse, ContentObject{
			Type:     "TD",
			FileName: "td.json",
			Content:  dContent,
		})
	default:
		err = fmt.Errorf("artifact not supported for content endpoint %s", manifests.ImageManifest.Config.MediaType)
		logger.Log.Warningf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	payload, _ := json.Marshal(descResponse)
	_, _ = w.Write(payload)

}
