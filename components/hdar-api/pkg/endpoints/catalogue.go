package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"go-common/components/utils/logger"
	"hdar/components/hdar-sdk/pkg/inspectors"
	xnfrv_inspectors "xnfrv/components/xnfrv-sdk/pkg/inspectors"
	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

// catalogue godoc
// @Router /catalogue [get]
// @Tags  default
// @Summary Internal catalogue
// @Description Set of artifacts by types
// @Param artifactType query string false "Select artifact type" Enums(HELM, DOCKER, NFV, VREPORT, VO, CVO, HDAG)
// @Produce  json
// @Success 200
func Catalogue(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Requested the catalogue")
	w.WriteHeader(http.StatusAccepted)
	// TODO: how to implement user based...?
	filter := r.URL.Query().Get("artifactType")
	logger.Log.Debugf("Received filter %s\n", filter)
	w.Header().Set("Content-Type", "application/json")
	regURL := os.Getenv("REGISTRY_URL")
	reg, err := registry.NewRegWithExistingDockerCredentials(regURL)
	if err != nil {
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		err = fmt.Errorf("registry not reachable")
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	artifactList, err := registry.List(reg)
	if err != nil {
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		err = fmt.Errorf("list of artifacts not found")
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusAccepted)

	cResponse := map[string][]string{
		"HELM":    []string{},
		"DOCKER":  []string{},
		"NFV":     []string{},
		"VREPORT": []string{},
		"VO":      []string{},
		"CVO":     []string{},
		"HDAG":    []string{},
	}
	// TODO Provide also the verification status
	// asset makes sure that the signature tag is not used as latest tag
	for ref, cMediaType := range artifactList {
		switch cMediaType {
		case inspectors.MediaTypeHDAGConfig:
			_, artifactId, _ := strings.Cut(ref, "/")
			cResponse["HDAG"] = append(cResponse["HDAG"], artifactId)
		case models.MediaTypeVReportConfig:
			_, artifactId, _ := strings.Cut(ref, "/")
			cResponse["VREPORT"] = append(cResponse["VREPORT"], artifactId)
		case xnfrv_inspectors.MediaTypeNFVConfig:
			_, artifactId, _ := strings.Cut(ref, "/")
			cResponse["NFV"] = append(cResponse["NFV"], artifactId)
		case "application/vnd.docker.container.image.v1+json":
			_, artifactId, _ := strings.Cut(ref, "/")
			cResponse["DOCKER"] = append(cResponse["DOCKER"], artifactId)
		case inspectors.MediaTypeVOConfig:
			// HELM and VO/CVO share config mediaType
			repo, err := registry.NewRepoWithExistingDockerCredentials(ref)
			if err != nil {
				logger.Log.Error(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				err = fmt.Errorf("unable to log in repo %s: %s", ref, err.Error())
				_, _ = w.Write([]byte(err.Error()))
				return
			}
			manifests, err := registry.FetchManifest(repo)
			if err != nil {
				logger.Log.Error(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				err = fmt.Errorf("unable to fetch manifest of %s: %s", ref, err.Error())
				_, _ = w.Write([]byte(err.Error()))
				return
			}

			logger.Log.Infof("Checking config %+v\n", manifests.ImageManifest)
			if val, ok := manifests.ImageManifest.Annotations["vnd.nephele.config.artifactType"]; !ok {
				// not a VO
				logger.Log.Debugf("%s is not a VO", ref)
				_, artifactId, _ := strings.Cut(ref, "/")
				cResponse["HELM"] = append(cResponse["HELM"], artifactId)
			} else {
				_, artifactId, _ := strings.Cut(ref, "/")
				aType, _, _ := strings.Cut(val, "_")

				cResponse[aType] = append(cResponse[aType], artifactId)
			}
		default:
			// This should not happen
			logger.Log.Warningf("Unknown artifact in registry %s for %s\n", cMediaType, ref)
		}
	}
	var payload []byte
	if filter == "" {
		payload, err = json.Marshal(cResponse)
	} else {
		payload, err = json.Marshal(cResponse[filter])
	}
	if err != nil {
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		err = fmt.Errorf("unable to marshal response %s", err.Error())
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	_, _ = w.Write(payload)
}
