package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"fmt"
	"go-common/components/utils/logger"
	"net/http"
)

// Health godoc
// @Router /health [get]
// @Tags  internal
// @Summary Health check of the service
// @Description Provides start checks for K8s
// @Produce  plain
// @Success		200
func Health(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Requested a heath check")
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprintf(w, "Status OK")
}
