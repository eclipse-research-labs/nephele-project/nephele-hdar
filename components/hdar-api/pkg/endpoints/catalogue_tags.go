package endpoints

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strings"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

// tags godoc
// @Router /catalogue/{id}/tags [get]
// @Tags  default
// @Summary Tags for an artifact
// @Description Obtain the tags of an artifact
// @Produce  json
// @Param id path string true "ID of the image in the HDAR"
// @Param project query string false "project"
// @Success 200  "tags list"
func GetTags(w http.ResponseWriter, r *http.Request) {
	logger.Log.Infof("Requested to fetch tags at %s\n", r.URL.Path)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	imageId := strings.TrimSuffix(r.URL.Path, "/tags/")
	imageId = strings.TrimSuffix(imageId, "/tags")
	imageId = strings.TrimPrefix(imageId, "/catalogue/")
	imageId = strings.TrimPrefix(imageId, "/catalogue")
	logger.Log.Infof("Received %+v\n", imageId)
	if imageId == "" || strings.Contains(imageId, "/") {
		err := errors.New("unable to decode imageId")
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	filter := r.URL.Query().Get("project")
	if filter != "" {
		imageId = filter + "/" + imageId
	}
	repoRef := os.Getenv("REGISTRY_URL") + "/" + imageId
	logger.Log.Infof("Trying to access %+v\n", repoRef)
	repo, err := registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err != nil {
		logger.Log.Errorf("Unable to connect to repo: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	err = repo.Tags(context.Background(), "", func(tags []string) error {
		logger.Log.Debugf("Found %+v tags", tags)
		// Might need to remove signatures
		finalTags := []string{}
		for _, t := range tags {
			// if strings.HasSuffix(t, ".sig") {
			// 	// This is a signature. ignore it
			// 	continue
			// }
			finalTags = append(finalTags, t)
		}
		payload, err := json.Marshal(finalTags)
		if err != nil {
			logger.Log.Errorf("unable to marshal tags: %s", err.Error())
			w.Write([]byte(err.Error()))
		} else {
			w.Write(payload)
		}
		return nil
	})
	if err != nil {
		logger.Log.Errorf("unable to access tags: %s", err.Error())
		w.Write([]byte(err.Error()))
	}
}
