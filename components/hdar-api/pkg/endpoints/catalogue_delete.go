package endpoints

import (
	"context"
	"errors"
	"net/http"
	"os"
	"strings"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

// delete godoc
// @Router /catalogue/{id} [delete]
// @Tags  default
// @Summary Delete an artifact
// @Description Delete all artifacts from a repository
// @Produce  json
// @Param id path string true "ID of the image in the HDAR"
// @Param project query string false "project"
// @Success 200  "Deleted"
func DeleteRepository(w http.ResponseWriter, r *http.Request) {
	// Todo: FIX why the repository name is left in storage but without tag available
	logger.Log.Infof("Requested to delete repository at %s\n", r.URL.Path)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	imageId := strings.TrimPrefix(r.URL.Path, "/catalogue/")
	imageId = strings.TrimPrefix(imageId, "/catalogue")
	logger.Log.Infof("Received %+v\n", imageId)
	if imageId == "" || strings.Contains(imageId, "/") {
		err := errors.New("unable to decode imageId")
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	filter := r.URL.Query().Get("project")
	if filter != "" {
		imageId = filter + "/" + imageId
	}
	repoRef := os.Getenv("REGISTRY_URL") + "/" + imageId
	logger.Log.Infof("Trying to access %+v\n", repoRef)
	repo, err := registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err != nil {
		logger.Log.Errorf("Unable to connect to repo: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	manifests, err := registry.FetchManifest(repo)
	if err != nil {
		logger.Log.Errorf("Unable to fetch manifests: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	err = repo.Delete(context.Background(), manifests.ImageDescriptor)
	if err != nil {
		logger.Log.Errorf("Unable to delete artifact: %s\n", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	_, _ = w.Write([]byte("Ok"))
	return
}
