package endpoints

// Render HDAG descriptor

type HDAGInput struct {
	Id             string         `json:"id"`
	Version        string         `json:"version"`
	Description    string         `json:"description"`
	HdaGraphIntent string         `json:"hdaGraphIntent"`
	Designer       string         `json:"designer"`
	Services       []ServiceInput `json:"services"`
}

type ServiceInput struct {
	Ref            string        `json:"ref"`
	Id             string        `json:"id"`
	DeploymentType string        `json:"deploymentType" enums:"auto,event"`
	Intent         ServiceIntent `json:"intent"`
}

type ServiceIntent struct {
	Resource string `json:"resource"`
	Network  string `json:"network"`
	Metrics  bool   `json:"metrics"`
}

type ContentObject struct {
	FileName string `json:"fileName"`
	Type     string `json:"type" enums:"TD,DESC,GENERIC"`
	Content  []byte `json:"content"`
}
