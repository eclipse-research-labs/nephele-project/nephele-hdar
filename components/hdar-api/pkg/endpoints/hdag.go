package endpoints

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"go-common/components/utils/filesystem"
	"go-common/components/utils/logger"
	"hdar/components/hdar-sdk/pkg/inspectors"
	"xnfrv/components/xnfrv-sdk/pkg/artifact"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
	"xnfrv/components/xnfrv-sdk/pkg/template"
)

// hdagcreate godoc
// @Router /hdag [post]
// @Tags  default
// @Summary Create an HDAG artifact
// @Description Based on user input. render and push a baseline HDAG descriptor
// @Produce  plain
// @Accept  json
// @Param HDAGRequest body HDAGInput true "Description of what the HDAG Descriptor should have"
// @Param project query string false "project"
// @Success 200 "HDAG descriptor created"
func CreateHDAG(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Requested the creation of HDAG")
	w.Header().Set("Content-Type", "text/plain")
	var input HDAGInput
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		logger.Log.Errorf("Body wrong formatted: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	logger.Log.Infof("Received %+v\n", input)
	// check that the artifact did not exist yet
	imageId := input.Id
	filter := r.URL.Query().Get("project")
	imageRepo := imageId
	if filter != "" {
		imageRepo = filter + "/" + imageId
	}
	repoRef := os.Getenv("REGISTRY_URL") + "/" + imageRepo
	logger.Log.Infof("Trying to access %+v\n", repoRef)
	repo, err := registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err == nil {
		// exists
		err = errors.New("repository already exists. Use PUT and actual descriptor")
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	// Repo does not exist yet (cannot find tags)
	// repoRef needs a specific tag so that it does not access the tags API at push

	// Generate template
	tContent, ok := inspectors.NewHDAGInspector().GetTemplateMap()["HDAG"]
	if !ok {
		err = errors.New("unable to obtain template content")
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	// TODO: Pending to customize the template according to the render input
	// 1. Add services OK
	// 2. pull values.yaml from other artifacts
	// 3. add right ociConfig etc
	// 4. ...?
	iContent, err := json.Marshal(input)
	if err != nil {
		logger.Log.Error("Unable to create template input")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	// Create the archive
	defer os.RemoveAll(input.Id)
	ok = template.GenerateLocalArtifact(".", tContent, string(iContent))
	if !ok {
		logger.Log.Error("Unable create the descriptor locally")
		w.WriteHeader(http.StatusInternalServerError)
		err = errors.New("unable create the descriptor locally")
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	// create the artifact
	aName := input.Id + "-" + input.Version + ".tar.gz"
	err = filesystem.WriteGZFile(aName, input.Id, ".")
	if err != nil {
		logger.Log.Error("Unable create the artifact locally")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	logger.Log.Debugf("Created local artifact %s\n", aName)

	// Push the artifact
	repoRef = os.Getenv("REGISTRY_URL") + "/" + imageRepo + ":" + input.Version
	logger.Log.Infof("Will publish to %+v\n", repoRef)
	repo, err = registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err != nil {
		logger.Log.Info("Invalid reference to the repository\n")
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	a, err := artifact.NewLocalArtifact(aName)
	if err != nil {
		logger.Log.Infof(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if found, err := inspectors.AddCorrectInspector(a, true); !found {
		err = fmt.Errorf("unable to add correct inspector implementation during push")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if err != nil {
		err = fmt.Errorf("verification error during push: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	err = registry.Push(repo, a)
	if err != nil {
		logger.Log.Info("Unable to copy manifest to remote\n")
		logger.Log.Info(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/plain")
	_, _ = w.Write([]byte("Pushed: " + repoRef))
	return
}

// hdagupdate godoc
// @Router /hdag/{id} [put]
// @Tags  default
// @Summary Update an HDAG artifact
// @Description Push a new descriptor to artifact
// @Produce  plain
// @Accept  application/yaml
// @Param Descriptor body string true "Descriptor of the HDAG"
// @Param id path string true "ID of the image in the HDAR"
// @Param project query string false "project"
// @Success 200 "HDAG descriptor updated"
func UpdateHDAG(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Requested the update of an HDAG")
	w.Header().Set("Content-Type", "text/plain")
	imageId := strings.TrimPrefix(r.URL.Path, "/hdag/")
	err := filesystem.CreateDirIfNotExist(imageId)
	defer os.RemoveAll(imageId)
	if err != nil {
		logger.Log.Errorf("Unable to create dir for artifact")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	// Check that Id and project exist
	filter := r.URL.Query().Get("project")
	imageRepo := imageId
	if filter != "" {
		imageRepo = filter + "/" + imageId
	}
	repoRef := os.Getenv("REGISTRY_URL") + "/" + imageRepo
	logger.Log.Infof("Trying to access %+v\n", repoRef)
	repo, err := registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err != nil {
		// does not exist
		err = errors.New("repository does not exist. Use POST and user intent")
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	// save response body into a file
	descriptor, err := os.Create(filepath.Join(imageId, "descriptor.yaml"))

	if err != nil {
		err = fmt.Errorf("unable to create descriptor for artifact: %s", err.Error())
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	defer descriptor.Close()

	io.Copy(descriptor, r.Body)

	// create artifact
	tmpA, err := artifact.NewTmpArtifactFromFolder(imageId)
	defer os.Remove("tmp.tar.gz")
	if err != nil {
		err = fmt.Errorf("unable to create local artifact: %s", err.Error())
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if found, err := inspectors.AddCorrectInspector(tmpA, false); !found {
		err = errors.New("unable to add correct inspector implementation during tar")
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if err != nil {
		err = fmt.Errorf("verification error during tar: %s", err.Error())
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	aName, err := artifact.TmpArtifactToLocalArtifact(tmpA, ".")
	if err != nil {
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	// Push the artifact
	a, err := artifact.NewLocalArtifact(aName)
	if err != nil {
		logger.Log.Infof(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if found, err := inspectors.AddCorrectInspector(a, true); !found {
		err = fmt.Errorf("unable to add correct inspector implementation during push")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	} else if err != nil {
		err = fmt.Errorf("verification error during push: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	repoRef = os.Getenv("REGISTRY_URL") + "/" + a.OCIPush.Name + ":" + a.OCIPush.Version
	repo, err = registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err != nil {
		logger.Log.Info("Invalid reference to the repository\n")
		logger.Log.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	err = registry.Push(repo, a)
	if err != nil {
		logger.Log.Info("Unable to copy manifest to remote\n")
		logger.Log.Info(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("Pushed: " + repoRef))
	return

}
