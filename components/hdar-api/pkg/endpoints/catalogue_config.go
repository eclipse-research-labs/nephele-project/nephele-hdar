package endpoints

import (
	"errors"
	"net/http"
	"os"
	"strings"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

// config godoc
// @Router /catalogue/{id}/config [get]
// @Tags  default
// @Summary Fetch OCI Config Manifest
// @Description Obtain the OCI Config Manifest of an artifact
// @Produce  json
// @Param id path string true "ID of the image in the HDAR"
// @Param project query string false "project"
// @Success 200  "OCI Config Manifest Content"
func GetConfigManifest(w http.ResponseWriter, r *http.Request) {
	logger.Log.Infof("Requested to fetch config manifest at %s\n", r.URL.Path)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	imageId := strings.TrimSuffix(r.URL.Path, "/config/")
	imageId = strings.TrimSuffix(imageId, "/config")
	imageId = strings.TrimPrefix(imageId, "/catalogue/")
	imageId = strings.TrimPrefix(imageId, "/catalogue")
	logger.Log.Infof("Received %+v\n", imageId)
	if imageId == "" || strings.Contains(imageId, "/") {
		err := errors.New("unable to decode imageId")
		logger.Log.Errorf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	filter := r.URL.Query().Get("project")
	if filter != "" {
		imageId = filter + "/" + imageId
	}
	repoRef := os.Getenv("REGISTRY_URL") + "/" + imageId
	logger.Log.Infof("Trying to access %+v\n", repoRef)
	// asset makes sure that the signature tag is not used as latest tag
	repo, err := registry.NewRepoWithExistingDockerCredentials(repoRef)
	if err != nil {
		logger.Log.Errorf("Unable to connect to repo: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	logger.Log.Infof("using tag '%+v'\n", repo.Reference)

	manifests, err := registry.FetchManifest(repo)
	if err != nil {
		logger.Log.Errorf("Unable to fetch manifests: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	_, _ = w.Write(manifests.ConfigManifestContent)
	return
}
