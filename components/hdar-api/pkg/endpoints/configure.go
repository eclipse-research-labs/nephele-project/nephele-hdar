package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/registry"
)

// Configure godoc
// @Router /configure [get]
// @Tags  internal
// @Summary Registry Login
// @Description Configures the login credentials towards the OCI Registry
// @Produce  plain
// @Success		200
func Configure(w http.ResponseWriter, r *http.Request) {
	logger.Log.Info("Requested to configure")
	logger.Log.Info("Saving credentials to OCI Registry")
	regURL := strings.TrimSpace(os.Getenv("REGISTRY_URL"))
	regPass := strings.TrimSpace(os.Getenv("REGISTRY_ADMIN_PASSWORD"))
	regUser := strings.TrimSpace(os.Getenv("REGISTRY_ADMIN_USER"))
	if regURL == "" || regPass == "" || regUser == "" {
		logger.Log.Fatal("Login info not available")
		return
	}
	logger.Log.Debugf("Login info '%s', '%s', '%s'", regURL, regUser, regPass)
	_, err := registry.NewRegWithStaticCredentials(regURL, regUser, regPass)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Unable to login to OCI Registry: %s", err.Error())
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Ready")

}
