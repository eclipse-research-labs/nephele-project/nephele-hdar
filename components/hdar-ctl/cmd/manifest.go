package cmd

import (
	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"

	"github.com/spf13/cobra"
)

var manifestCmd = &cobra.Command{
	Use:   "manifest [REGISTRY/REPOSITORY/NAME:TAG] [FLAGS]",
	Short: "Pulls the OCI Image specification. ",
	Long:  `Pulls the OCI manifests for image and config.`,
	Args:  cobra.ExactArgs(1),
	RunE:  xnfrcmd.ManifestCmdRun,
}
