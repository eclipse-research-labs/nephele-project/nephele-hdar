package cmd

import (
	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"

	"github.com/spf13/cobra"
)

var pullCmd = &cobra.Command{
	Use:   "pull [REGISTRY/REPOSITORY/NAME:TAG]",
	Short: "Pulls an OCI artifact from a the HDAR",
	Long:  `Downloads the tar.gz artifact associated to an OCI artifact and optionally extracts the content.`,
	RunE:  xnfrcmd.PullCmdRun,
}
