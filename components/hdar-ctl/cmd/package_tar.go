package cmd

import (
	"github.com/spf13/cobra"

	xnfr_cmd "xnfrv/components/xnfr-ctl/cmd"
)

var tarCmd = &cobra.Command{
	Use:   "tar [PATH]",
	Short: "Package PATH as a tar.gz artifact.",
	Long:  `Packages the directory pointed by PATH as a tar.gz artfact.`,
	Args:  cobra.ExactArgs(1),
	RunE:  xnfr_cmd.TarCmdRun,
}

func init() {
	packageCmd.AddCommand(tarCmd)
}
