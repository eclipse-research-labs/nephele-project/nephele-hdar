package cmd

import (
	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"

	"github.com/spf13/cobra"
)

var pushCmd = &cobra.Command{
	Use:   "push [ARTIFACT] [REGISTRY/REPOSITORY]",
	Short: "Pushes a local artifact to the HDAR",
	Long:  `Takes a local artifact and creates an OCI image which is then pushed to a HDAR`,
	RunE:  xnfrcmd.PushCmdRun,
}
