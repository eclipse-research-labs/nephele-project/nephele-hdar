package cmd

import (
	"github.com/spf13/cobra"

	xnfr_cmd "xnfrv/components/xnfr-ctl/cmd"
)

var unTarCmd = &cobra.Command{
	Use:   "untar [PATH]",
	Short: "Extracts the contents from a tar.gz file.",
	Long:  `Extracts the contents from a tar.gz file.`,
	Args:  cobra.ExactArgs(1),
	RunE:  xnfr_cmd.UnTarCmdRun,
}

func init() {
	packageCmd.AddCommand(unTarCmd)
}
