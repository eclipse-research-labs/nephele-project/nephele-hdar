package cmd

import (
	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"

	"github.com/spf13/cobra"
)

var logoutCmd = &cobra.Command{
	Use:   "logout [REGISTRY]",
	Short: "Logs out from the HDAR",
	Long:  `Logs out from the HDAR. Access will be lost to all repositories.`,
	Args:  cobra.ExactArgs(1),
	RunE:  xnfrcmd.LogoutCmdRun,
}
