package cmd

import (
	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"

	"github.com/spf13/cobra"
)

var packageCmd = &cobra.Command{
	Use:   "package tar/untar [flags]",
	Short: "Parent command to tar and untar commands",
	Long:  `Parent command to work with local artifacts`,
	Args:  cobra.ExactArgs(1),
	RunE:  xnfrcmd.PackageCmdRun,
}
