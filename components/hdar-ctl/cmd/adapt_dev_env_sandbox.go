package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"go-common/components/utils/logger"
	devenv "hdar/components/hdar-sdk/pkg/dev_env"
)

var adaptCmd = &cobra.Command{
	Use:   "adapt [PATH]",
	Short: `Adapt gitlab-ci of the dev env sandbox to include a new artifact`,
	Long:  `Adapt gitlab-ci of the dev env sandbox to include a new artifact`,
	Args:  cobra.ExactArgs(1),
	RunE:  AdaptCmdRun,
}

func ConfigureAdaptCmd(c *cobra.Command) {
	c.PersistentFlags().StringP("project", "p", "", "Project in the HDAR where the artifact should be added")
}

func AdaptCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	target := strings.TrimSuffix(args[0], "/")
	f, err := os.Stat(target)
	if err != nil {
		err = fmt.Errorf("Adapt command needs to be executed from the base folder of the repository: %s", err.Error())
		return err
	} else if !f.IsDir() {
		err = fmt.Errorf("Not found the correct location to execute the adapt command")
		return err
	}
	project, _ := cmd.Flags().GetString("project")
	err = devenv.AdaptLocalFile(target, project)
	if err != nil {
		err = fmt.Errorf("The local Gitlab-ci file could not be updated correctly due to: %s", err.Error())
		return err
	}
	fmt.Println("Adapted gitlab-ci file")
	return nil
}
