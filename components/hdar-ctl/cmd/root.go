package cmd

import (
	"github.com/spf13/cobra"
	"os"

	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"

	"hdar/components/hdar-sdk/pkg/inspectors"
)

var RootCmd = &cobra.Command{
	Use:          "hdarctl",
	Short:        "Control tool for the HDA Registry",
	Long:         `Hyper Distributed Application Registry Control toolkit`,
	SilenceUsage: true,
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		// print is automatic
		os.Exit(1)
	}
}

func init() {
	// Hide internal command from Cobra
	RootCmd.CompletionOptions.DisableDefaultCmd = true
	xnfrcmd.ConfigurePushCmd(pushCmd)
	xnfrcmd.ConfigurePullCmd(pullCmd)
	xnfrcmd.ConfigureLoginCmd(loginCmd)
	xnfrcmd.ConfigurePackageCmd(packageCmd)
	ConfigureCreateCmd(createCmd)
	ConfigureAdaptCmd(adaptCmd)
	xnfrcmd.ConfigureManifestCmd(manifestCmd)

	RootCmd.AddCommand(pullCmd)
	RootCmd.AddCommand(pushCmd)
	RootCmd.AddCommand(loginCmd)
	RootCmd.AddCommand(logoutCmd)
	RootCmd.AddCommand(packageCmd)
	RootCmd.AddCommand(createCmd)
	RootCmd.AddCommand(lintCmd)
	RootCmd.AddCommand(manifestCmd)
	RootCmd.AddCommand(adaptCmd)

	// Connect to config in base cmd
	xnfrcmd.ConfigCmdInstance.AddCorrectInspectorFunction = inspectors.AddCorrectInspector
	xnfrcmd.ConfigCmdInstance.GetTemplateMapFunction = AggregatedGetTemplateMaps

}
