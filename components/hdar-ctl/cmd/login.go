package cmd

import (
	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"

	"github.com/spf13/cobra"
)

var loginCmd = &cobra.Command{
	Use:   "login [REGISTRY] [flags]",
	Short: "Logs in to the HDAR",
	Long:  `Logs in to HDAR. Registry might be a url or ip:port. No repository is needed.`,
	Args:  cobra.ExactArgs(1),
	RunE:  xnfrcmd.LoginCmdRun,
}
