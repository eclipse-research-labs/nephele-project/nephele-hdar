package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"go-common/components/utils/logger"

	"github.com/spf13/cobra"

	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"
)

var createCmd = &cobra.Command{
	Use:   "create [TYPE] [flags]",
	Short: "Create an artifact [TYPE=(VNF,NS,HDAG,VO_WOT)]",
	Long:  `Create a baseline artifact for [TYPE=(VNF,NS,HDAG,VO_WOT)] based on template`,
	Args:  cobra.ExactArgs(1),
	RunE:  CreateCmdRun,
}

func ConfigureCreateCmd(c *cobra.Command) {
	c.PersistentFlags().StringP("destination", "d", ".", "Path to the base folder that will contain the artifact")
	c.PersistentFlags().StringP("name", "n", "myfirst-$TYPE", "Unique name to give to the artifact")
	c.PersistentFlags().StringP("description", "D", "This is my first artifact", "Description for the artifact")
	c.PersistentFlags().StringP("author", "a", "NEPHELE", "Name of the author/designer/maintainer")
	c.PersistentFlags().StringP("version", "v", "0.1.0", "Semver version of the artifact")
}

func CreateCmdRun(cmd *cobra.Command, args []string) error {
	defer logger.PrintPanic()
	outputBasePath, _ := cmd.Flags().GetString("destination")
	aTypeFound := false
	for _, v := range xnfrcmd.ConfigCmdInstance.GetArtifactTypes() {
		if v == strings.ToUpper(args[0]) {
			aTypeFound = true
		}
	}
	if len(args) != 1 || !aTypeFound {
		return fmt.Errorf("Expecting only the artifact type as argument. Choose %v\n", xnfrcmd.ConfigCmdInstance.GetArtifactTypes())
	}
	tContent, ok := xnfrcmd.ConfigCmdInstance.GetTemplateMapFunction()[strings.ToUpper(args[0])]
	if !ok {
		return fmt.Errorf("Unable to obtain template content")
	}
	logger.Log.Debugf("Baseline path %s \n", outputBasePath)
	// Generate the input data for the template
	input := map[string]interface{}{}
	id, _ := cmd.Flags().GetString("name")
	id = strings.ReplaceAll(id, "-$TYPE", strings.ToLower(args[0]))
	description, _ := cmd.Flags().GetString("description")
	author, _ := cmd.Flags().GetString("author")
	version, _ := cmd.Flags().GetString("version")

	switch strings.ToUpper(args[0]) {
	case "VNF":
		input = map[string]interface{}{
			"name":        strings.ReplaceAll(id, "-", " "),
			"id":          id,
			"description": description,
			"provider":    author,
			"version":     version,
			"kdus": []map[string]interface{}{map[string]interface{}{
				"name":  "PLACEHOLDER",
				"chart": "https://nephele-hdar.netmode.ece.ntua.gr/PLACEHOLDER",
			}},
		}
	case "NS":
		input = map[string]interface{}{
			"name":        strings.ReplaceAll(id, "-", " "),
			"id":          id,
			"description": description,
			"provider":    author,
			"version":     version,
			"vnf": []map[string]interface{}{map[string]interface{}{
				"id": "PLACEHOLDER",
			}},
		}
	case "HDAG":
		input = map[string]interface{}{
			"name":           strings.ReplaceAll(id, "-", " "),
			"id":             id,
			"description":    description,
			"designer":       author,
			"version":        version,
			"hdaGraphIntent": "PLACEHOLDER",
			"services": []map[string]interface{}{map[string]interface{}{
				"deployment": "PLACEHOLDER",
				"intent": map[string]interface{}{
					"network":   "PLACEHOLDER",
					"resources": "PLACEHODLER",
					"metrics":   true,
				},
				"id":  "PLACEHOLDER",
				"ref": "https://nephele-hdar.netmode.ece.ntua.gr/PLACEHOLDER",
			}},
		}
	case "VO_WOT":
		input = map[string]interface{}{
			"id":          id,
			"description": description,
			"designer":    author,
			"version":     version,
		}

	}
	iContent, err := json.Marshal(input)
	if err != nil {
		err = fmt.Errorf("Unable to generate input to template: %s", err.Error())
		return err
	}

	if !xnfrcmd.CMDGenerateLocalArtifact(outputBasePath, tContent, string(iContent)) {
		return fmt.Errorf("unable to create local artifact")
	}
	// Perform initial lint and remove if not valid
	err = xnfrcmd.LintCmdRun(cmd, []string{filepath.Join(outputBasePath, id)})
	if err != nil {
		err = fmt.Errorf("Some of the input parameters are not accepted and lint failed. Check logs and retry: %s", err.Error())
		os.RemoveAll(filepath.Join(outputBasePath, id))
		return err
	}
	fmt.Printf("Artifact generated at %s\nReplace all occurences of 'PLACEHOLDER'\nLint, package and push it\n", filepath.Join(outputBasePath, id))
	return nil
}
