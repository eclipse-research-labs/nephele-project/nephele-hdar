package cmd

import (
	"github.com/spf13/cobra"

	xnfrcmd "xnfrv/components/xnfr-ctl/cmd"
)

var lintCmd = &cobra.Command{
	Use:   "lint [PATH]",
	Short: "Lint an artifact",
	Long:  `Lint verification of an artifact based on the folder or tar.gz`,
	Args:  cobra.ExactArgs(1),
	RunE:  xnfrcmd.LintCmdRun,
}
