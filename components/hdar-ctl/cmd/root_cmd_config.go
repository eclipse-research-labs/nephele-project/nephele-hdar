package cmd

import (
	"hdar/components/hdar-sdk/pkg/inspectors"
	"maps"
	"xnfrv/components/xnfr-ctl/cmd"
)

// Configured at the cmd/root.go level to return all the available templates for the create comand
func AggregatedGetTemplateMaps() map[string]string {
	templMap := cmd.AggregatedGetTemplateMaps()
	maps.Copy(templMap, inspectors.NewHDAGInspector().GetTemplateMap())
	maps.Copy(templMap, inspectors.NewVOInspector().GetTemplateMap())
	return templMap
}
