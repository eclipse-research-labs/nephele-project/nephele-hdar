**Latest Tag Release**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarctl-coverage-latesttag.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarctl-version-latesttag.svg)

**Develop**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarctl-coverage-develop.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdarctl-version-develop.svg)

# HDARCTL

CLI to handle xNFV + nephele artifacts as well as the interactions with the HDAR.

## Description

The Control CLI currently focuses on: Interacts with the registry via **nephele-hdar.netmode.ece.ntua.gr**
1. CLI to interact with the HDAR will be provided as a single [executable file for ubuntu envs](./hdarctl).
2. Basic interaction with an OCI-Registry for a custom type of artifact allowing to push OSM NS and VNF and Nephele applications
3. Development toolkit to manage the artifacts involved in a HDAG. 
  - Templating mechanism to create Nephele applications
  - Verification of syntax, semantic and structure of components
4. **The HDAR is prepared to be interacted with by devs using CLI tools**:
  - docker
  - helm (v3): No support for V2 repo index.yaml. There is a Helm issue to push behind a keycloak, it is currently a PR. **Please use the helm binary from ../../assets/xNF-Registry-Verification/tools/ folder**. (Else compile from here)[https://github.com/helm/helm/pull/13138]
5. Assist the developers in the dev-env-sandbox to facilitate the creation of a Gitlab's CICD pipeline


## Getting Started

Check deliverable D2.1, D2.2 and D4.1

### Prerequisites

Full configuration via ENV Vars:

- Check $PWD/install/.env-test for local deployment and move it to $PWD/.env

#### Software

Project built using Golang 1.22 and requires to have an OCI Registry installed and accessible -> **registry.nephele-hdar.netmode.ece.ntua.gr**.

#### Hardware

No major requirements found.

### Installation

Project developed using WSL Golang installation

1. Go auto formatting and linting. Execute before pushing to origin. It is automated in CI pipeline and will fail if not ok

```bash
go mod tidy
go fmt $(go list ./... | grep -v /vendor/)
go vet $(go list ./... | grep -v /vendor/)
```

4. Make sure tests pass

```bash
go test $(go list ./... | grep -v /vendor/) -v
```
5. Build 

A) Manually
```bash
GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o hdarctl . 
# adapt env vars
cp install/.env-tests .env
export $(grep -v '^#' .env | xargs)
./hdarctl
```
or Manually (macOS M3/Apple Silicon)
```bash
GOOS=darwin GOARCH=arm64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o hdarctl .
# adapt env vars
cp install/.env-tests .env
export $(grep -v '^#' .env | xargs)
./hdarctl
```

B) Create docker image locally or pull the one generated by the CICD pipeline

```bash
# Needs the full repo context
cd ../../
docker build -t registry.atosresearch.eu:18498/hdarapi:latest --network host -f components/hdar-ctl/install/dockerfile . 
cd components/hdar-ctl
```

## Usage

This project generates a binary.

CLI commands :
```bash
./hdarctl --help
Hyper Distributed Application Registry Control toolkit

Usage:
  hdarctl [command]

Available Commands:
  adapt       Adapt gitlab-ci of the dev env sandbox to include a new artifact
  create      Create an artifact [TYPE=(VNF,NS,HDAG,VO_WOT)]
  help        Help about any command
  lint        Lint an artifact
  login       Logs in to the HDAR
  logout      Logs out from the HDAR
  manifest    Pulls the OCI Image specification. 
  package     Parent command to tar and untar commands
  pull        Pulls an OCI artifact from a the HDAR
  push        Pushes a local artifact to the HDAR

Flags:
  -h, --help   help for hdarctl

Use "hdarctl [command] --help" for more information about a command.
```


## Contribution

Tech:

- Guillermo Gomez guillermo.gomezchavez@eviden.com

Asset Owner:

- Sonia Castro sonia.castro@eviden.com

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or PR created from GitHub as draft.
3. Commit and push frequently.
4. When ready, set PR as ready, tag team and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```
