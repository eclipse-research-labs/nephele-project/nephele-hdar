# Development Environment Sandbox

This is considered as a baseline repository to fork and develop your custom HDA in a working CICD environment which
is connected to the HDAR and thus enables you to later deploy and test your applications. See image below.

![architecture](./docs/dev-sandbox.png)

The objective is to simplify how HDAs are developed and provide a common CICD environment which ensures a minimal level of 
quality and reliability to all artifacts entering the Nephele SMO Platform. The Development Environment Sandbox is complemented with a 
second pipeline of verification which is performed at the HDA Registry, which includes more advanced tests, as shown in the figure below:

![architecture](./docs/dev-cycle.png)

For this to work the layout of the repository is fixed and the CICD is centralized in the .gitlab-ci.yaml file.

```
├── vertical-apps/
├── virtual-object/
├── hda-graphs/
├── src/
├── tools/
├── .gitlab-ci.yaml
└── README.md
```

- **vertical-apps**: Directory where all the helm charts of the top level applications must be placed. 
- **hda-graphs**: Directory where all the HDAGs must be placed. The HDAG will be considered ready if both, itself, and all 
  chained services are available.
- **virtual-objects**: Directory where all the helm charts of the (c)VOs must be placed
  + Base images of the VOs are always: 
    - **WoT implementation -> registry.nephele-hdar.netmode.ece.ntua.gr/public/vo-wot:latest** which is developed [here](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-wot)
    - **OMA implementation -> registry.nephele-hdar.netmode.ece.ntua.gr/public/vo-oma:latest** which is developed [here](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-lwm2m)
- **src**: Non-managed directory to freely organize the internal code of vertical apps and (c)VOs. 
- **tools**: binary and scripts required by the dev sandbox
  - hdarctl: to interact with the HDAR and local artifacts
  - helm: is here due to an issue on the official release which is not merged yet. Use this one to be able to push helm charts to the HDAR
- **.gitlab-ci.yaml**: CICD file which checks for updates in all locations, except the src directory, and executes the required 
  builds, checks, and push. 
- **README.md**: This file

**Refer to the official NEPHELE Development Environment Documentation for pre-requisites and tutorials [here](https://documentation.nephele-hdar.netmode.ece.ntua.gr).**
