package main

import (
	"go-common/components/utils/logger"
	"hdar/components/hdav-listeners/pkg/listeners"

	xnfv_listeners "xnfrv/components/xnfv-listeners/pkg/listeners"
	"xnfrv/components/xnfv-listeners/pkg/redis"
)

func main() {
	logger.Log.Info("Starting xNF Verification Engine in HDA")
	// This is how I manage that xnfv code runs things stored in this new repo
	xnfv_listeners.ListenerConfigInstance.SelectListenerFunction = listeners.AddCorrectListener
	// Run listeners as go-routines in the background.
	go redis.CreateAndRunListener()
	go redis.CreateAndRunListener()
	go redis.CreateAndRunListener()
	// Block with the last one
	redis.CreateAndRunListener()
	logger.Log.Info("Finished")
}
