package listeners

import (
	"go-common/components/utils/logger"
	"os"
	"os/exec"

	"xnfrv/components/xnfrv-sdk/pkg/models"
	"xnfrv/components/xnfrv-sdk/pkg/utils"
	"xnfrv/components/xnfv-listeners/pkg/listeners"
)

// Implements ListenerInterface
type ListenerVO struct {
}

func (v *ListenerVO) ExecutePipeline(report *models.VerificationReport) {
	logger.Log.Debug("Detected vo event")
	aName, err := listeners.PullArtifact(report)
	defer os.Remove(aName)
	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, "cannot pull the artifact")
		logger.Log.Errorf("Cannot pull artifact with %s: %s -> %s\n", os.Getenv("DEFAULT_CLI"), err.Error(), "cannot pull the artifact")
		return
	}

	logger.Log.Debugf("aName for pipeline: %s\n", aName)
	v.StageLint(report, aName)
	v.StageScan(report, aName)
	v.StageIntegrity(report, aName)

}

func (v *ListenerVO) StageIntegrity(report *models.VerificationReport, artifactName string) {
	listeners.CosignIntegrity(report)

}
func (v *ListenerVO) StageScan(report *models.VerificationReport, artifactName string) {
	// Begin scan
	nMap, err := utils.GetFileFormat(artifactName)
	if err != nil {
		report.Scan.Report.Success = false
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, err.Error())
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, "Unable to infer name and tag from artifact filename")
		logger.Log.Errorf("Unable to infer name and tag from artifact filename: %s", err.Error())
	}
	scanFile := "scan-" + nMap["name"] + "-" + nMap["version"] + ".json"
	// defer os.Remove(scanFile)
	cmd := exec.Command("trivy", "conf", artifactName, "-f", "json", "-o", scanFile)
	output, err := cmd.CombinedOutput()
	report.Scan.Report.Tool = "trivy conf"
	if err != nil {
		report.Scan.Report.Success = false
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, err.Error())
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, string(output))
		logger.Log.Errorf("Cannot scan artifact with trivy: %s -> %s\n", err.Error(), string(output))
	} else {
		report.Scan.Report.Success = true // TODO set it to false if any Critical CVE is found
		report.Scan.Report.Logs = append(report.Scan.Report.Logs, string(output))
		logger.Log.Debug(string(output))
		report.Scan.ResultsFile = scanFile
	}
}

func (v *ListenerVO) StageLint(report *models.VerificationReport, artifactName string) {
	logger.Log.Debug("Detected Vo image event")
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "lint", artifactName)
	output, err := cmd.CombinedOutput()
	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, string(output))
		logger.Log.Errorf("Cannot lint artifact with Vo: %s -> %s\n", err.Error(), string(output))
	} else {
		report.Lint.Report.Success = true
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, string(output))
		logger.Log.Debug(string(output))
	}

}
