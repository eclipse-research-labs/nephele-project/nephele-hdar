package listeners

import (
	"os"
	"os/exec"

	"go-common/components/utils/logger"
	"xnfrv/components/xnfrv-sdk/pkg/models"

	"xnfrv/components/xnfv-listeners/pkg/listeners"
)

// Implements ListenerInterface
type ListenerHDAG struct {
}

func (v *ListenerHDAG) ExecutePipeline(report *models.VerificationReport) {
	logger.Log.Debug("Detected hdag event")
	aName, err := listeners.PullArtifact(report)
	defer os.Remove(aName)
	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, "cannot pull the artifact")
		logger.Log.Errorf("Cannot pull artifact with %s: %s -> %s\n", os.Getenv("DEFAULT_CLI"), err.Error(), "cannot pull the artifact")
	}
	logger.Log.Debugf("aName for pipeline: %s\n", aName)
	v.StageLint(report, aName)
	v.StageIntegrity(report, aName)

}

func (v *ListenerHDAG) StageIntegrity(report *models.VerificationReport, artifactName string) {
	listeners.CosignIntegrity(report)
}

func (v *ListenerHDAG) StageScan(report *models.VerificationReport, artifactName string) {

}

func (v *ListenerHDAG) StageLint(report *models.VerificationReport, artifactName string) {
	logger.Log.Debug("Detected HDAG image event")
	cmd := exec.Command(os.Getenv("DEFAULT_CLI"), "lint", artifactName)
	output, err := cmd.CombinedOutput()
	if err != nil {
		report.Lint.Report.Success = false
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, err.Error())
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, string(output))
		logger.Log.Errorf("Cannot lint artifact with HDAG: %s -> %s\n", err.Error(), string(output))
	} else {
		report.Lint.Report.Success = true
		report.Lint.Report.Logs = append(report.Lint.Report.Logs, string(output))
		logger.Log.Debug(string(output))
	}

}
