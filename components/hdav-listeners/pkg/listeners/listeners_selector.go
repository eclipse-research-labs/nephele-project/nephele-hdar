package listeners

import (
	v1 "github.com/opencontainers/image-spec/specs-go/v1"

	"go-common/components/utils/logger"

	"hdar/components/hdar-sdk/pkg/inspectors"
	"xnfrv/components/xnfrv-sdk/pkg/interfaces"
	xnfv_listeners "xnfrv/components/xnfv-listeners/pkg/listeners"
)

func AddCorrectListener(manifest v1.Manifest) interfaces.ListenerInterface {
	logger.Log.Debugf("Detecting listener for %s", manifest.Config.MediaType)
	switch manifest.Config.MediaType {
	case inspectors.MediaTypeHDAGConfig:
		return &ListenerHDAG{}
	case inspectors.MediaTypeVOConfig:
		// Need to check if it is truly a VO or not...
		if val, ok := manifest.Annotations["vnd.nephele.config.artifactType"]; ok {
			logger.Log.Debugf("detected a (c)VO (%s): %+v", val, manifest)
			return &ListenerVO{}
		}
	}
	// Try now with the Baseline implementations offered by xnfv
	return xnfv_listeners.AddCorrectListener(manifest)
}
