**Latest Tag Release**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdavlisteners-coverage-latesttag.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdavlisteners-version-latesttag.svg)

**Develop**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdavlisteners-coverage-develop.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdavlisteners-version-develop.svg)


# HDAR Verification Engine Listeners

Extends the xNFRV with custom listeners for the artifacts supported by the Nephele project.
Provides the Redis Workers pool to cary out the verification pipeline of the artifacts received in the OCI Registry.

## Description

The Verification Engine focuses on: Available indirectly via the HDAR REST API
1. Verification of syntax, semantic and integrity of components
2. Scanning of Docker images and VM images searching for CVE
3. Scanning Helm charts and OSM descriptors searching for missconfigurations
4. Creating signatures to execute integrity checks before executing any artifact

## Getting Started

Check deliverable D2.1, D2.2 and D4.1

- listeners/: Implementation of the listener interface for the artifacts of Nephele

Connected to the main xNFV asset via xnfv_listeners.ListenerConfigInstance.SelectListenerFunction in main.go

### Prerequisites

Full configuration via ENV Vars:

- Check $PWD/install/.env-test for local deployment and move it to $PWD/.env

#### Software

Project built using Golang 1.22 and requires the xNFV-API microservice to redirect push events to the listeners.

#### Hardware

No major requirements found. 1vCPU, 1GiB RAM.

### Installation

Project developed using WSL Golang installation

1. Go auto formatting and linting. Execute before pushing to origin. It is automated in CI pipeline and will fail if not ok

```bash
go fmt $(go list ./... | grep -v /vendor/)
go vet $(go list ./... | grep -v /vendor/)
```

2. Make sure tests pass

```bash
go test $(go list ./... | grep -v /vendor/) -v
```

3. Build

A) Manually

```bash
GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o hdavlisteners . 
# adapt env vars
cp install/.env-tests .env
export $(grep -v '^#' .env | xargs)
./hdavlisteners
```

B) Create docker image locally or pull the one generated by the CICD pipeline

```bash
# Needs the full repo context
cd ../../
docker build -t registry.atosresearch.eu:18498/hdavlisteners:develop --network host -f components/hdav-listeners/install/dockerfile . 
cd components/hdav-listeners
```

## Usage

Redis pub/sub



## Contribution

Tech:

- Guillermo Gomez guillermo.gomezchavez@eviden.com

Asset Owner:

- Sonia Castro sonia.castro@eviden.com

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or PR created from GitHub as draft.
3. Commit and push frequently.
4. When ready, set PR as ready, tag team and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```
