# Development Environment Documentation

The objective is to deploy a development-guide which explains how to create an HDA and link to all external documentation.

Project build using Sphinx

## Contribute

Documentation based on [RST language](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html). Check the official Sphinx project for more info [here](https://www.sphinx-doc.org/en/master/index.html)

```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
sphinx-build -b html content _build/html -a
# Check for errors in the terminal output
```

## Build locally

```bash
docker build -t registry.atosresearch.eu:18498/devenvdocs:latest -f install/dockerfile  .
docker run -p 8080:80 registry.atosresearch.eu:18498/devenvdocs:latest
```
