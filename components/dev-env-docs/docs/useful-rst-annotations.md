=============   ===========
Field           Description
=============   ===========
``op``          Interaction verb (e.g. ``readproperty``) associated with this form element.
``href``        The CoAP URL used to interface with the Thing server for this interaction verb.
``mediaType``   This field will always contain the MIME media type for JSON.
=============   ===========



.. code-block:: yaml

    BindingNB:
        securityNB:


.. figure:: images/verifiable_credentials.png


.. note::
    Currently, the credenti


Request::

    GET coap://<host>:<port>/property?thing=<thing_name>&name=<property_name>



.. list-table:: SQLite batabase tables

	* - VO status (self-check)
	* - Status of IoT d