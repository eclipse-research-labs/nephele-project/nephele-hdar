Nephele SMO Platform
==============================================

Description
---------------

The NEPHELE project continues to explore, leverage and innovate over the latest advances of the compute continuum and cloud-native ecosystem, both, for the release of the 
NEPHELE Synergetic Meta-Orchestration (SMO) Platform itself and for the management and orchestration of the workloads of the UCs (HDAs).

**Desired Functionalities (non-exclusive list)**:

#. **The desired extensions and functionalities consider solutions for:**
   
   - Scaling, application scheduling, task scheduling, computation offloading, live migration.
   - Management of ML workflows considering the interplay between the IoT, the edge, and the cloud parts of the infrastructure.
   - Enablers for accelerator’s management (e.g., GPU).
   - Security and trust management.


#. **Dependency Technology Stack:**


   All extensions should seamlessly integrate with the NEPHELE technology stack. NEPHELE emphasizes the use of open-source solutions that can be easily adopted, such as:

   - Kubernetes CRDs.
   - Kubernetes/KARMADA APIs extension.
   - Integration of CNCF projects into NEPHELE.

Architecture
+++++++++++++

.. figure:: ../images/architecture.png
   :align:  center

   **NEPHELE SMO Platform architecture and technology stack**

#. **Dashboard:** provides a set of views to application developers and providers to support validation actions 
   for the developed descriptors, both for application graphs and theirintent, as well as interfaces for managing 
   the registered images on the platform, monitoring registered resources, and providing access to deployment views.

#. **Development Environment (DE):** provides all the services required by service developers and providers to build, 
   test, release and distribute their HDAs. It also serves as verification mechanism for the NEPHELE SMO Platform to perform 
   a series of preliminary checks before trying to instantiate the HDAs. Additionally, the component is used as the full inventory
   of the HDAs and all its components, both, before and after instantiation thanks to a (c)VO discovery service. Lastly, this 
   component has gathered new and existing documentation required to fully understand how the NEPHELE SMO Platform works and how 
   to interact with it.

#. **Service Management and Orchestration (SMO):** is the main entity  responsible for intent-driven deployment and Lifecycle Management
   (LCM) of HDAs over programmable resources in the compute continuum. It undertakes a deployment request through the dashboard, 
   preparing deployment plans by considering both the status and available resources of the underlying infrastructure, as well as the 
   user-defined intents. Continuous monitoring and event-triggered optimization actions are supported, with tasks forwarded to managers in 
   lower levels of the system hierarchy under a system-of-systems approach. Security and trust management mechanisms are provided to improve the 
   platform's resilience against potential threats and to ensure data integrity and confidentiality throughout the orchestration process. 
   These technologies have additionally been integrated into the VOStack to be leveraged by the NEPHELE HDAs. 

#. **Multi-Cluster Resource Manager (MCRM):** is responsible for managing the compute resources across the compute continuum. The MCRM 
   assumes that different clusters (each operated by a different Kubernetes instance) are registered to a unified continuum. Such clusters
   may span from the IoT to the edge to the cloud part of the infrastructure. Actions related to placement, security, migration, and
   elasticity are supported by the MCRM across the available clusters. It includes a federated monitoring engine that aggregates data from 
   various cluster-level monitoring systems.

#. **Network Resource Manager (NRM):** is responsible for providing network performance guarantees to the HDA deployments in a multi-cluster
   infrastructure. It is activated if a network operator is engaged in the deployment. In this case, end-to-end network slice LCM is 
   provided by the network resource manager, including processes for continuous optimization of network resources usage. If a network 
   operator is not engaged, connectivity services among the clusters is provided from the MCRM. In the latter case, less network performance 
   guarantees can be supported.

#. **Edge/Cloud Resource Manager (ECRM):** integrates essential technologies for the resource management at the cluster level as well 
   as custom systems for advanced management based on AI. Additionally, this component is where the VOStack developed in WP3 is executed 
   and therefore it is key to enable the integrations between the two legs of the overall SMO, the NEPHELE SMO Platform and the VOStack.

#. **Integration Fabric (IF):** provides shared services which facilitate secure communication between components and their exposure to 
   external systems. It also provides platform-level monitoring for the maintenance of the NEPHELE SMO Platform and infrastructure. While 
   some collected metrics are utilized by components for management, orchestration, and monitoring of HDA workloads, additional monitoring 
   and observability features are provided by individual components.

#. (**c)VO Stack:** The Virtual Object (VO), is the virtual counterpart/extension of IoT devices deployed on the premises of Edge/Cloud 
   Clusters. The VO is a lightweight software stack, based on two different specifications, W3C Web of Things and Open Mobile Alliance 
   (OMA) Lightweight Machine-to-Machine (LwM2M), that stores the necessary information produced by IoT devices and therefore acts as a 
   broker between the devices and an application graph.

==============   ==============================================================================================================================================
 Component        Additional info
==============   ==============================================================================================================================================
``Dashboard``    Section 4
``DE``           Section 4, `Dev Env Sandbox in Gitlab <https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/nephele-development-sandbox>`_
``MCRM``         `Karmada <https://karmada.io/>`_
``NRM``          `NFV-CL <https://nfvcl-ng.readthedocs.io/en/latest/index.html>`_   
``(c)VO``        `VO-WoT <https://netmode.gitlab.io/vo-wot/>`_, `VO-OMA <https://technical.openmobilealliance.org/index.htm>`_   
==============   ==============================================================================================================================================

Technology Stack
+++++++++++++++++

The final implementation of the NEPHELE SMO Platform achieves a much more dynamic, efficient and reliable delivery of its internal components by leveraging the features of cloud-native,
containerization on top of a virtualized and programmable infrastructure.

Cloud-native architectures built around `Kubernetes (K8s) <https://kubernetes.io/docs/concepts/overview/>`_ are inherently scalable, resilient, and agile, making them well-suited
for the dynamic and heterogeneous nature of the compute continuum. In fact, **the SMO orchestration approach is fully based on K8s environments**, 
making them the focal point of its orchestration strategy.

This approach enables NEPHELE to address the unique challenges and opportunities within such environments, with a focus on inter-cluster communication, workload (re-)placement, 
and the capacity to monitor, adapt, and optimize workloads and the infrastructure itself.
Thus, the general practice for **all developments in the NEPHELE Project will be to deliver containerized applications that will be deployed in K8s.
For the orchestration of these applications, it will be standardized to rely on** `Helm <https://helm.sh>`_ **as a way to create, version, share and publish applications built for K8s.**
Other approaches based on K8s Operators and K8s manifests are also envisioned.


.. figure:: ../images/testbed.png
   :align:  center

   **NEPHELE infrastructure Setup, the network overlay between partners provided infrastructures is connected through Submariner**


Useful Links
  
  * `(c)VO examples <https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-wot/-/tree/main/examples?ref_type=heads>`_
  * `VO descriptor <https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-wot/-/blob/main/examples/descriptors/vo_descriptor_full.yaml?ref_type=heads>`_
  * `VO Example Helm <https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-wot/-/tree/main/examples/descriptors/vo?ref_type=heads>`_
  * `Application Graph Descriptor Example <https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/nephele-hdar/-/blob/main/hda-examples/information-model/hdag/hda_graph-0.3.0.yaml?ref_type=heads>`_
  * `Example Application with Helm Chart <https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-wot/-/tree/main/examples/brussels-demo?ref_type=heads>`_
 


What does the Nephele SMO offer?
-----------------------------------

The NEPHELE SMO Platform is developed and released completely as vendor-neutral open-source software. 
Professional open-source projects are governed according to certain rules, processes, and best practices 
to manage adoption, contributions and development by diverse communities. This is especially
important for vendor-neutral open-source projects which are not managed by a single entity. The aim 
for NEPHELE SMO Platform is to become a community-driven, vendor-neutral open-source project



**From the perspective of a consumer of the NEPHELE SMO Platform**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The NEPHELE SMO Platform offers a series of features for the successful delivery of the UCs, highlighting the contribution of each component. 

#. **Development and verification of artifacts for HDAs:** Developers and Service Providers are provided with the necessary tools and mechanisms to produce high-quality, verified HDAs.  
#. **Intent-based HDA placement in muti-cluster:** HDAGs are described using a human-readable set of intents which are translated into actions in a deployment plan considering the underlaying infrastructure. 
#. **HDAs monitoring, reconfiguration and relocation in multi-cluster:** Based on the deployment plan and the changing conditions of the compute continuum, HDAs are dynamically reconfigured 
   and relocated across the multi-clusters. 
#. **Reliable and high available data storage:** HDAs often run stateful workloads across different geographic areas. The NEPHELE SMO Platform ensures high reliability without
   the complexity of managing many independent application replicas.
#. **Secure and trustworthy communications of the HDAs:** Aligned with the security and trust functions of the VO stack, the NEPHELE SMO Platform provides the appropriate systems across environments.
#. **Multi-cluster communication, monitoring, adaptability and optimization of infrastructure:**  Secured and efficient monitoring and communication of all clusters in the infrastructure to 
   create a seamless experience in the compute continuum.


**From the perspective of the Platform Operator (PO)**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A series of common objectives and concerns have been identified and considered during the implementation of the NEPHELE SMO Platform:

#.  **Objectives:**

    * Ease of scalability of the underlying infrastructure to extend the coverage of the ecosystem while maintaining security.
    * High Availability (HA) and resilience of the synergetic orchestration platform.
    * LCM of the VOs (optional) and the HDA components. Ease of usability and UC-oriented functionalities to promote the use of the platform.
    * Efficient resource utilization of the compute and network resources for a cost-effective and efficient operation of the platform.

#.  **Concerns:**

    * Cost-effective maintenance and scalability.
    * Compliance with regulatory standards.
    * Rapid response to hardware/software failures.
    * Data backup and disaster recovery

What does an HDA offer?
----------------------------

Although the NEPHELE SMO Platform sits mostly in the central cloud, part of its orchestration functions and components can be offloaded towards the managed infrastructure where the HDAs are deployed. In 
particular, the ECRM, and the orchestration capabilities at the VOStack developed by WP3. The figure below epresents how the NEPHELE SMO Platform orchestrates cloud-native 
applications and (c)VOs based on an HDAG descriptor which are building blocks for HDAs to visualize how HDAs are realized in such a distributed environment. 

.. figure:: ../images/hdas-in-continumm.png
   :align:  center

   **HDA destribution of chained services in the continunm** 


How to access & contribute?
---------------------------------------------------

NEPHELE is part of the `Eclipse Foundation research community <https://gitlab.eclipse.org/eclipse-research-labs>`_. We aim for implementing open source best practices and providing 
properly governed open source code as result of the NEPHELE project. 
The requirement for Open Call projects is to develop their code in a public repository (`as part of the NEPHELE project in Eclipse Research Labs GitLab <https://gitlab.eclipse.org/eclipse-research-labs/nephele-project>`_
under a permissive open source license (APACHE-2.0, MIT). Support for correct license compliance will be provided. Tool support is available and will be provided for checking and adding license files and headers 
and for performing third-party license compliance checks. 

Dedicated trainings on topics such as open source in general or open source licenses can be provided to Open Call participants upon request.
As a starting point we suggest to have a look the `good practices wiki page <https://gitlab.eclipse.org/groups/eclipse-research-labs/-/wikis/Some-Open-Source-Good-Practices>`_.
Further background about Eclipse Research Labs can be found `here <https://gitlab.eclipse.org/groups/eclipse-research-labs/-/wikis/About>`_.

