.. _Dev tutorials:


Quickstart: Development Tutorials
===================================

The development of an HDA was presented in :ref:`artifactimage` as a sequential process which involves the generation of:

#. Application docker image
   
   * Vertical application 
   * (c)VO

#. Helm chart
   
   * Vertical application 
   * (c)VO

#. HDAG

Most of the development is performed at your local PC.

* Please refer to :ref:`devenv` for the software pre-requisites
  
Following this example we will be able to develop and deploy our first HDA consisting of:

  - VO exposing IoT information
  - cVO concentrating information from the VOs
  - Vertical app providing a service based on the cVO interface
  - HDA that configures the intent to orchestrate and manage the HDA

.. toctree::
    iot-and-vo
    vo-and-cvo
    vo-and-app
    hdag-to-cvo-app
    