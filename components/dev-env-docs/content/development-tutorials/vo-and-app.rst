Consume a (c)VO from an Application (?)
=======================================

The process to connect an application to the VO is simple. If Python is used, then the process is vastly simplified. The user needs to install the runtime as before:
 
.. figure:: ../images/app-vo-1.png
   :align:  center

   **Installation step**

Afterwards, the library can be used to consume a VO:

.. figure:: ../images/app-vo-2.png
   :align:  center

   **Consume VO from App example**


The “consumed_thing” object can be used with the corresponding API:
-	Property Read:

.. figure:: ../images/app-vo-3.png
   :align:  center

   **Consume VO from App example (read property)**


-	Property Write:
 
.. figure:: ../images/app-vo-4.png
   :align:  center

   **Consume VO from App example (write property)**


-	Action Invocation:

.. figure:: ../images/app-vo-5.png
   :align:  center

   **Consume VO from App example (action invocation)**

-	Event subscription

.. figure:: ../images/app-vo-6.png
   :align:  center

   **Consume VO from App example (event subscription)**
 
`Detailed explanations are available here <https://netmode.gitlab.io/vo-wot/consumed.html>`.
If Python is not used, then the developer will need to use each `protocol's API directly <https://netmode.gitlab.io/vo-wot/protocols.html>`_.
