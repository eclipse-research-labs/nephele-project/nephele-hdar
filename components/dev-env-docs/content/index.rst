NEPHELE Development Environment Documentation
=================================================

The Nephele Development Environment is a set of tools and systems developed under the T4.1 of the NEPHELE project which aim to assist the 
developers and services providers with their tasks related to artifact management.

The present documentation provides information about:

1. How to develop an HDA

2. How to interact with the Nephele SMO Platform


The NEPHELE Synergetic Meta-Orchestration (SMO) Platform, shown in the figure below, is devoted to the management of Hyper-Distributed Applications (HDAs).

.. figure:: ./images/HLarchitecture.png

Table of Contents
-----------------

.. toctree::
    :maxdepth: 6
    :numbered:

    nephele-platform/nephele
    information-models/index
    development-tutorials/index
    platform-tutorials/index

.. rubric:: Disclaimer

.. raw:: html

    <div class="small-font">
        This document is issued within the frame and for the purpose of the NEPHELE project. This project has received funding from the European Union’s Horizon Europe Framework Programme under Grant Agreement No.101070487. The opinions expressed and arguments employed herein do not necessarily reflect the official views of the European Commission. 
    </div>

