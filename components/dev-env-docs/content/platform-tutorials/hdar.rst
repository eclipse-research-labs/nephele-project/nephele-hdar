Hyper-Distributed Applications Registry 
==============================================

The HDA Registry (HDAR) system is the common storage, distribution and verification system for all 
the artifacts involved in the deployment of HDA within the NEPHELE Platform.
HDAR aims to finally align the storage, distribution and exposure mechanisms for artifacts of the 
Vertical Service Providers (VSP) and the Communication Service Providers (CSP). By scanning the 
artifacts, the HDAR will enhance the security posture of the HDAs in the NEPHELE project, ensuring
a robust defence against potential vulnerabilities and misconfigurations throughout the entire CEI 
continuum. 

.. figure:: ../images/hdar-modules.png
  :align:  center

  **HDAR Modules**

Aiming to contribute to this alignment of communities when applied to the compute continuum 
ecosystem, the HDAR will be implemented as a microservice-based system which will include an 
OCI-Registry compatible all the artifacts involved in the deployment of the HDAs, including standard and 
NEPHELE-specific implementations:

* The HDA Graph
* OSM Network Services (NSs) and Virtualized Network Functions (VNF) (rel >=V8)
* Docker containers images
* Helm charts (rel >=V3) including the VO and cVO.

*Note: The terms “repository” and “registry” are often used interchangeably, however, there are some key 
differences between the two that are important to fully understand the HDAR system in the NEPHELE
project. A registry is a storage and distribution system for those artifacts. A registry is organized into 
repositories, where a repository holds all the versions of a specific artifact.*

Interfaces
------------

The HDAR can be interacted with in two modes depending on the required functionality

#. Using the hdarctl CLI which is available as part of the development environment sandbox, refer to :ref:`devenv`.
   
   This utility is meant for developers and works in a similar way as the Helm and Docker CLIs. In fact, they are all 
   CLIs to interact with an OCI-Registry and provide custom functionalities for their target artifact.
   The hdarctl is meant to be used for local HDAGs and (c)VOs and to push them to the HDAR. The HDAR can be seamlessly
   interacted with by the helm and docker CLIs for their corresponding artifact.

.. figure:: ../images/hdarctl.png
  :align:  center

  **hdarctl commands**

#. Using the Swagger UI to interact with a REST API. This is available under hdarapi.nephele-hdar.netmode.ece.ntua.gr/swagger/index.html
   and offers functionalities related to the management of the artifacts once inside of the Nephele Platform. These functionalities are 
   consumed by the Dashboard and SMO but are open to users with credentials.

.. figure:: ../images/hdar-swagger.png
  :align:  center

  **HDAR Swagger UI**