.. _dashboard:

Dashboard  
==================

Check also the following `video <https://www.youtube.com/watch?v=k-G5eWwdpUI>`_.

.. figure:: ../images/landing_page.png
   :align:  center

   **Landing Page**

.. figure:: ../images/signup.png
   :align:  center

   **Sign Up Page**

.. figure:: ../images/login.png
   :align:  center

   **Log In Page**

.. figure:: ../images/infrastructure_page.png
   :align:  center

   **Infrastructure Page**


