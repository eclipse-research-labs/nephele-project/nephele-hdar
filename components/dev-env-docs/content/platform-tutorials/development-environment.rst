.. _devenv:

Development Environment Sandbox
=================================

This section relates to the usage of a sandbox repository that has been created for the development of all the services that 
are required to run the HDAs. It is considered as a baseline repository to fork and develop your custom HDA in a working CICD environment which
is connected to the HDAR and thus enables you to later deploy and test your applications. See image below.

.. figure:: ../images/dev-sandbox.png
  :align:  center

  **Development Environment Sandbox**
  
The objective is to simplify how HDAs are developed and provide a common CICD environment which ensures a minimal level of 
quality and reliability to all artifacts entering the Nephele SMO Platform. The Development Environment Sandbox is complemented with a 
second pipeline of verification which is performed at the HDA Registry, which includes more advanced tests, as shown in the figure below:

.. figure:: ../images/dev-cycle.png
  :align:  center

  **Development Environment Cycle**
    
Prerequesites
--------------

A HDA developer needs to configure its own local environment with:

#. Fork the development environment sandbox
#. Create user and password to the Nephele Platform. Refer to :ref:`dashboard`. 
#. Configure them as cicd variables in gitlab (NEPHELE_USER and NEPHELE_PASSWORD). 
#. Create a Gitlab Personal Access Token to interact with gitlab 
#. Clone your forked repository with the PAT
#. Install a local docker CLI (e.g., docker desktop)
#. Load the hdarctl CLI to your PATH 
  
  .. code-block:: bash
  
    cd <forked-dev-env-sandbox>
    TOOLS_DIR=$(realpath ./tools/)
    export PATH=$TOOLS_DIR:$PATH
    hdarctl --help

Quickstart
-------------

#. Login to HDAR with the Nephele platform credentials

  .. code-block:: bash
  
    hdarctl login registry.nephele-hdar.netmode.ece.ntua.gr -u <NEPHELE_USERNAME> -p <NEPHELE_PASSWORD>

#. Create your first VO 

  .. code-block:: bash
  
    hdarctl login registry.nephele-hdar.netmode.ece.ntua.gr -u <NEPHELE_USERNAME> -p <NEPHELE_PASSWORD>


#. For each new virtual-object or hda-graph:
 

  .. code-block:: bash

    cd <corresponding directory>
    hdarctl create --help   # Make sure to use the --adapt-dev-env tag
    # Select the right artifact type and any option required
    git add .
    git commit -m "baseline artifact"
    git push
    # See the first CICD executed


#. Until the artifact is considered finished continue with the dev cycle:

  .. code-block:: bash

    # Adapt descriptors as required to represent your use case
    hdarctl lint <path to artifact>
    # See logs and adapt the descriptors accordingly

