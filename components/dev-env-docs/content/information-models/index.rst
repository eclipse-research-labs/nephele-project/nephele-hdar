.. _artifactimage:

Information Models
==================

In the context of the NEPHELE Project all software applications involved in the delivery of an HDA 
will be released as an OCI complaint artifact. Figure depicts the final collection of artifacts proposed
to describe an HDA in NEPHELE in the form of a service chain. Each one shows its data model, structure,
and a target system to execute them.

Given the distributed nature of HDAs and the cloud-native characteristic of the infrastructure, it was 
clear that Docker images and Helm charts would be part of the required artifacts. Then, there is the need 
to package the intent-based orchestration of the HDAs and all the required information for the 
NEPHELE Platform to work, which is realized as an HDA Graph (HDAG).


.. figure:: ../images/artifacts.png
   :align:  center

   **Full example of artifacts in HDA**

   
From left to right, a custom HDAG describes the instantiation of a HDA as a collection of services. 
Then, a service might be one of three types; a standard Helm chart for vertical applications (e.g., Apps 
consuming (c)VOs), a pre-defined Helm chart which represents a (c)VO, or a Network Function 
Virtualization (NFV) artifact. Lastly, the actual logic is contained in a Docker image which again, could 
be either a fully custom one or the pre-defined (c)VO Docker image.

#.  The HDAG has been designed to feed the intents and configurations to the NEPHELE Platform
    per services linked. The descriptor is provided in the form of a YAML8 document.
#.  Helm's data models are based the K8s manifest but include many advanced 
    features for managing the K8s application by using additional manifests (chart.yaml, 
    values.yaml...) and support Go's template syntax. Helm dynamically creates K8s 
    resource manifests by leveraging a series of templates and instantiation data.

        *   Resulting from WP3 and WP4 discussions, it has been seen appropriate to adopt a common technology for all 
            (c)VO implementations and, for the most part, for all UC applications. These implementations will always be 
            expressed as K8s workloads using Helm, where the executed VO container image is configured using a series 
            of descriptors and configuration files as defined by the VO implementation specification, which is prepared 
            as part of WP3. 

#.  A Network Function Virtualization (NFV) artifact can be used as a wrapper to deploy any of the 
    previous two components within an NFV operator's infrastructure. These artifacts are mostly described 
    as YAML descriptors whose data models are maintained by ETSI.
#.  Docker defines its own data model for the Dockerfile which is the file that enables the creation 
    of the filesystem bundle based on the local source code.
    
For those artifacts that are managed by a thirdparty, please refer to their own documentation:

* Docker: `Dockerfile reference <https://docs.docker.com/engine/reference/builder/>`_
* Vertical App Helm Chart: `Helm Charts <https://helm.sh/docs/chart_template_guide/getting_started/>`_
* OSM: `NS and VNF reference <https://osm.etsi.org/docs/user-guide/latest/11-osm-im.html>`_

For those artifacts that are part of the NEPHELE project, please refer to the corresponding documentation:


.. toctree::
    
    hdag
    vo-description