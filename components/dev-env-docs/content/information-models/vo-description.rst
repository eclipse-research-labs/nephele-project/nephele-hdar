.. _cvo:

(composed)Virtual Object
==============================

IoT scenarios have evolved over time providing increasingly complex and integrated services, as well as a growing development of 
heterogeneous sensors and devices with different resources capabilities. These range from single-board 
computers, such as Raspberry Pi with gigabytes of memory, to microcontrollers with only a few kilobytes 
of memory. Furthermore, the interest that has increased around the IoT has driven the research and 
development of new standards which, depending on the case, sought to respond to the needs of the 
reference application context. Over time, an increasingly heterogeneous context 
has been created, constrained by the limited availability of device resources, in stark contrast to the need 
for interoperability and scalability of new smart scenarios such as urban environments, factories, agriculture, logistics, etc


Virtual Object (VO) is a software service that extends the properties, attributes, and functionalities of 
real-world physical devices within the digital infrastructure of the network. The VO serves several 
important purposes, which help overcome limitation described above:

* **Semantic abstraction:** It provides a standard representation of physical device for easier 
  management, monitoring, and discovery of device resources.
* **Interoperability:** It can be used in heterogeneous scenario to bridge communication between 
  different standards and protocols.
* **Data modelling:** It enables the use of different data modelling for different purpose leveraging 
  physical device from complex data model transmission.
* **Simulation and testing:** In its Digital-twin extension, VO can be used to simulate the physical 
  device behaviour in a virtual environment before deployment.
* **Remote management and accessibility:** VO enables continuous data access and remote 
  management where physical device access can be limited or impractical.
* **Resource consumption:** It can leverage the physical device.
  

VO will have four main interactions within the deployment environment:

* **VO-to-IoT Device Interaction:** to link the physical world of IoT devices to the edge-cloud  continuum virtualized environment.
* **VO-to-Applications (Prosumers) Interaction:** to enable data exchange between cooperative  services and to create services’ chains.
* **VO-to-Orchestration Interaction:** to manage VO microservice lifecycle.
* **VO-to-Storage Entity Interaction:** for the allocation of data in an efficient and scalable manner.

Technologies
--------------

Although the artifact technology for the (c)VOs is already agreed to consist of a Helm Chart, each VO 
implementation may need a different set of configurations, which may or may not finalise in the 
creation of a common descriptor with its own IM.

Artifact Structure
------------------

A VO is released as a library that can be consumed directly or via a docker image. Both of these artifact types 
have their own structure and is fully managed by the official maintainers of the technology (python, docker).

However, in the case of a Kubernetes deployment, NEPHELE has developed a standardize Helm chart which simplifies
how to leverage the (c)VO in a cloud environment. 

In this case, `the artifact structure follows the rules of Helm <https://helm.sh/docs/topics/charts/>`_, but
it is managed by the **hdarctl** CLI instead of by the Helm CLI to ensure that the right metadata is added for the 
rest of the NEPHELE platform to understand that it is a (c)VO Helm chart.

Data Models
------------

The documentation for the Nephele VOs is centralized in each repository.
- `VO-WOT documentation <https://netmode.gitlab.io/vo-wot/nephele.html>`_

The Helm chart is used by customizing the **values.yaml** file.

.. code-block:: yaml

  voDescriptorOverwrite:
    catalogue: 9091
    bindingNB:
      sdn:
        enabled: true
      tsn:
        enabled: true
      ports:
        httpPort: 8081
        httpProxyPort: 8080
    databaseConfig:
      timeseriesDB:
        influxDB: disabled

  voChartOverwrite:
    OIDC4VP:
      enabled: false
      proxyEnabled: false
      xacmlPdp: ""
      blockchainRestUrl: ""
      issuerHost: ""
      proxyHttpPort: 8080
      proxyCataloguePort: 9090
    influxdb:
      resources:
        limits:
          cpu: "500m"
    tsn:
      resources:
        limits:
          cpu: "500m"
    sdn:
      resources:
        limits:
          cpu: "500m"

    serviceAccount:
      create: false
      annotations: {}
      name: "vo"
    ingress:
      enabled: false
    resources:
      limits:
        cpu: "500m"

    autoscaling:
      enabled: false
    serviceImportClusters: []
    clustersAffinity: []

    service:
      type: NodePort
      catalogue:
        nodePort: 30000
      http:
        nodePort: 30001


OCI manifests
---------------

Image manifest
#################

It follows the standard container manifest structure and adds custom annotations to characterize it as a (c)VO.

.. code-block:: json

  {
    "schemaVersion": 2,
    "mediaType": "application/vnd.oci.image.manifest.v1+json",
    "config": {
      "mediaType": "application/vnd.cncf.helm.config.v1+json",
      "digest": "sha256:5b5246c11ee8ce42e3ac84531de1b277b2979ef1c169d03b8c90a193e01cf5ff",
      "size": 209
    },
    "layers": [
      {
        "mediaType": "application/vnd.cncf.helm.chart.content.v1.tar+gzip",
        "digest": "sha256:554eea82439fdf30802d3c6f45921c5be672d74c39eafc2216dd11f1b76742d2",
        "size": 3768,
        "annotations": {
          "org.opencontainers.image.title": "unique-id-vo-0.1.0.tar.gz"
        }
      }
    ],
    "annotations": {
      "org.opencontainers.image.authors": "ATOS",
      "org.opencontainers.image.created": "2024-10-29T11:06:20Z",
      "org.opencontainers.image.description": "My demo VO autogenerated",
      "org.opencontainers.image.title": "unique-id-vo",
      "org.opencontainers.image.version": "0.1.0",
      "vnd.nephele.config.artifactType": "VO_WoT"
    }
  }

Config manifest
#################

It is managed by Helm. It provides all the information from the Chart.yaml file

.. code-block:: json

  {
    "name": "unique-id-vo",
    "version": "0.1.0",
    "description": "My demo VO autogenerated",
    "maintainers": [
      {
        "name": "ATOS"
      }
    ],
    "apiVersion": "v2",
    "appVersion": "0.1.0",
    "type": "application"
  }

How to create one
-----------------

The creation of a (c)VO can be done in two ways:

#. Manually, by following the example in the Tutorial's section.
#. Using the hdarctl CLI to create a baseline Helm chart which only needs to be adapted to the specifics of the UC.

.. figure:: ../images/hdagcreate1.png
   :align:  center

   **hdarctl create comand**